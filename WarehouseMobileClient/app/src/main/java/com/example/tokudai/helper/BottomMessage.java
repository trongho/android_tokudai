package com.example.tokudai.helper;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.tokudai.R;

public class BottomMessage {
    public static void onButtonShowPopupWindowClick(View view, Context context,String tag) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                 context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView=inflater.inflate(R.layout.bottomsheet_msg, null);
        TextView tvMsg=popupView.findViewById(R.id.tvMessage);
        switch (tag){
            case "scaned":
                tvMsg.setText("Mã hàng đã scan!!!");
                break;
            case "not exist":
                tvMsg.setText("Mã hàng không đúng!!!");
                break;
            case "wrong label":
                tvMsg.setText("Mã QR ko đúng!!!");
                break;
            case "over quantity":
                tvMsg.setText("Vượt tồn kho!!!");
                break;
        }

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }
}
