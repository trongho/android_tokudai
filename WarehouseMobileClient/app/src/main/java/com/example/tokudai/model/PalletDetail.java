package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PalletDetail {
    @SerializedName("palletID")
    @Expose
    private String palletID;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("productFamily")
    @Expose
    private String productFamily ;
    @SerializedName("lotID")
    @Expose
    private String lotID;
    @SerializedName("partName")
    @Expose
    private String partName;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @Expose
    private Double packingVolume;
    @SerializedName("mfgDate")
    @Expose
    private String mfgDate;
    @SerializedName("expDate")
    @Expose
    private String expDate;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("receiptDate")
    @Expose
    private String receiptDate;

    public PalletDetail() {
    }

    public PalletDetail(String palletID, Integer ordinal, String idCode, String partNumber, String productFamily, String lotID, String partName, Double quantity, Double packingVolume, String mfgDate, String expDate, String unit, String status, String receiptDate) {
        this.palletID = palletID;
        this.ordinal = ordinal;
        this.idCode = idCode;
        this.partNumber = partNumber;
        this.productFamily = productFamily;
        this.lotID = lotID;
        this.partName = partName;
        this.quantity = quantity;
        this.packingVolume = packingVolume;
        this.mfgDate = mfgDate;
        this.expDate = expDate;
        this.unit = unit;
        this.status = status;
        this.receiptDate = receiptDate;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    public String getLotID() {
        return lotID;
    }

    public void setLotID(String lotID) {
        this.lotID = lotID;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPackingVolume() {
        return packingVolume;
    }

    public void setPackingVolume(Double packingVolume) {
        this.packingVolume = packingVolume;
    }

    public String getMfgDate() {
        return mfgDate;
    }

    public void setMfgDate(String mfgDate) {
        this.mfgDate = mfgDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }
}
