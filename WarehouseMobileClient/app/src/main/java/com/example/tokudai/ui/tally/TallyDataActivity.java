package com.example.tokudai.ui.tally;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.tokudai.R;
import com.example.tokudai.adapter.TallyDataAdapter;
import com.example.tokudai.helper.Common;
import com.example.tokudai.helper.MessageHelper;
import com.example.tokudai.model.TSDetail;
import com.example.tokudai.model.TSHeader;
import com.example.tokudai.model.TallyData;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.tokudai.helper.MessageHelper.alertSuccess;
import static com.example.tokudai.helper.MessageHelper.playErrorSound;

public class TallyDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private RecyclerView recyclerView = null;
    private EditText edtTSNumber = null;
    private EditText edtGoodsID = null;
    private EditText edtIDCode = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalQuantityByPack = null;
    private Button btnClearTSNumber = null;
    private Button btnClearGoodsID = null;
    private Button btnClearQuantity = null;
    private TallyDataAdapter adapter;
    ArrayList<TallyData> tallyDatas;
    ArrayList<TSHeader> tsHeaders;
    ArrayList<TSDetail> tsDetails;
    ArrayList<TSDetail> tsDetailByMultiIDs;
    private Double totalQuantity = 0.0;
    private Double totalQuantityByPack = 0.0;

    private String autoTSNumber;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("ddMMyyyy");
    Calendar c = Calendar.getInstance();

    IWarehouseApi iWarehouseApi = RetrofitService.getService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tally_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Kiểm kê");


        EMDKResults results = EMDKManager.getEMDKManager(TallyDataActivity.this, this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();
        tallyDatas = new ArrayList<TallyData>();
        tsHeaders = new ArrayList<>();
        tsDetails = new ArrayList<TSDetail>();
        tsDetailByMultiIDs = new ArrayList<TSDetail>();

    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtTSNumber = findViewById(R.id.edtTSNumber);
        edtGoodsID = findViewById(R.id.edtPartNumber);
        edtQuantity = findViewById(R.id.edtQuantity);
        edtIDCode = findViewById(R.id.edtIDCode);
        btnClearTSNumber = findViewById(R.id.btn_clear_tsnumber);
        btnClearGoodsID = findViewById(R.id.btn_clear_partnumber);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalQuantityByPack = findViewById(R.id.edtTotalQuantityByPack);
    }

    private Double getTotalQuantity(ArrayList<TallyData> tallyDatas) {
        for (int i = 0; i < tallyDatas.size(); i++) {
            if (tallyDatas.size() > 0 && tallyDatas.get(i).getQuantity() > 0) {
                totalQuantity += tallyDatas.get(i).getQuantity();
            }
        }
        return totalQuantity;
    }

    private Double getTotalRow(ArrayList<TallyData> tallyDatas) {
        for (int i = 0; i < tallyDatas.size(); i++) {
            totalQuantityByPack += 1;
        }
        return totalQuantityByPack;
    }

    private void setAdapter(ArrayList<TallyData> tallyDatas) {
        adapter = new TallyDataAdapter(TallyDataActivity.this, tallyDatas, new TallyDataAdapter.ItemClickListener() {
            @Override
            public void onClick(TallyData tallyData) {

            }
        }, new TallyDataAdapter.ItemActionListener() {
            @Override
            public void onClick(TallyData tallyData, int position) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    popupMenuClick(position, tallyData);
                }
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(TallyDataActivity.this));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void popupMenuClick(int position, TallyData tallyData) {
        PopupMenu popup = new PopupMenu(TallyDataActivity.this, recyclerView.getChildAt(position));
        popup.setGravity(Gravity.END);
        popup.inflate(R.menu.action_edit_delete_menu);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menuItem_edit:

                        break;
                    case R.id.menuItem_delete:

                        break;
                }
                return false;
            }
        });
        popup.show();
    }


    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    private ArrayList<TSHeader> FetchTSHeader(String tsNumber) {
        Call<List<TSHeader>> callback = iWarehouseApi.getTSHeaderById(tsNumber);
        callback.enqueue(new Callback<List<TSHeader>>() {
            @Override
            public void onResponse(Call<List<TSHeader>> call, Response<List<TSHeader>> response) {
                if (response.isSuccessful()) {
                    tsHeaders = (ArrayList<TSHeader>) response.body();
                    if (tsHeaders.size() > 0) {
                        edtTotalQuantity.setText(String.format("%.0f", tsHeaders.get(0).getTotalQuantity()));
                        edtTotalQuantityByPack.setText(String.format("%.0f", tsHeaders.get(0).getTotalQuantityByPack()));
                        FetchTSDetail(tsNumber);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TSHeader>> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tsHeaders;
    }

    private ArrayList<TSDetail> FetchTSDetail(String tsNumber) {
        Call<List<TSDetail>> callback = iWarehouseApi.getTSDetailById(tsNumber);
        callback.enqueue(new Callback<List<TSDetail>>() {
            @Override
            public void onResponse(Call<List<TSDetail>> call, Response<List<TSDetail>> response) {
                if (response.isSuccessful()) {
                    tsDetails = (ArrayList<TSDetail>) response.body();
                } else {
                    Toast.makeText(TallyDataActivity.this, "fetch dail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TSDetail>> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tsDetails;
    }

    private void PutTSHeader(String tsNumber, TSHeader tsHeader) {
        Call<TSHeader> callback = iWarehouseApi.updateTSHeader(tsNumber, tsHeader);
        callback.enqueue(new Callback<TSHeader>() {
            @Override
            public void onResponse(Call<TSHeader> call, Response<TSHeader> response) {
                if (response.isSuccessful()) {
                    FetchTSHeader(edtTSNumber.getText().toString());
                }
            }

            @Override
            public void onFailure(Call<TSHeader> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PutTSDetail(String tsNumber, String goodsID, int ordinal, TSDetail tsDetail) {
        Call<TSDetail> callback = iWarehouseApi.updateTSDetail(tsNumber, goodsID, ordinal, tsDetail);
        callback.enqueue(new Callback<TSDetail>() {
            @Override
            public void onResponse(Call<TSDetail> call, Response<TSDetail> response) {
                if (response.isSuccessful()) {
                    FetchTSDetail(edtTSNumber.getText().toString());
                }
            }

            @Override
            public void onFailure(Call<TSDetail> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PostTallyData(TallyData tallyData) {
        Call<TallyData> callback = iWarehouseApi.creatTallyData(tallyData);
        callback.enqueue(new Callback<TallyData>() {
            @Override
            public void onResponse(Call<TallyData> call, Response<TallyData> response) {
                if (response.isSuccessful()) {
                    FetchTallyData(edtTSNumber.getText().toString());
                } else {
                    Toast.makeText(TallyDataActivity.this, "Creat fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TallyData> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PutTallyData(String tsNumber, String goodsID, int No, TallyData tallyData) {
        Call<TallyData> callback = iWarehouseApi.updateTallyData(tsNumber, goodsID, No, tallyData);
        callback.enqueue(new Callback<TallyData>() {
            @Override
            public void onResponse(Call<TallyData> call, Response<TallyData> response) {
                if (response.isSuccessful()) {
                    alertSuccess(TallyDataActivity.this, "Upadte success tallydata");
                    FetchTallyData(edtTSNumber.getText().toString());
                } else {
                    Toast.makeText(TallyDataActivity.this, "Update fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TallyData> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeleteTallyDataDetail(String tsNumber, String goodsID, int No) {
        Call<Boolean> callback = iWarehouseApi.deleteTallyDataDetail(tsNumber, goodsID, No);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(TallyDataActivity.this, "Delete success tallydata");
                } else {
                    Toast.makeText(TallyDataActivity.this, "Delete fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void DeleteTallyData(String tsNumber) {
        Call<Boolean> callback = iWarehouseApi.deleteTallyData(tsNumber);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(TallyDataActivity.this, "Delete success tallydata");
                } else {
                    Toast.makeText(TallyDataActivity.this, "Delete fail tallydata", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<TallyData> FetchTallyData(String tsNumber) {
        Call<List<TallyData>> callback = iWarehouseApi.getTallyDataById(tsNumber);
        callback.enqueue(new Callback<List<TallyData>>() {
            @Override
            public void onResponse(Call<List<TallyData>> call, Response<List<TallyData>> response) {
                if (response.isSuccessful()) {
                    tallyDatas = (ArrayList<TallyData>) response.body();
                    setAdapter(tallyDatas);
                    totalQuantity = 0.0;
                    totalQuantityByPack = 0.0;
                    edtTotalQuantity.setText(getTotalQuantity(tallyDatas) + "");
                    edtTotalQuantityByPack.setText(getTotalRow(tallyDatas) + "");
                } else {
                    Toast.makeText(TallyDataActivity.this, "fetch dail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TallyData>> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
        return tallyDatas;
    }

    private void PostTSHeader(TSHeader tsHeader) {
        Call<TSHeader> callback = iWarehouseApi.creatTSHeader(tsHeader);
        callback.enqueue(new Callback<TSHeader>() {
            @Override
            public void onResponse(Call<TSHeader> call, Response<TSHeader> response) {
                if (response.isSuccessful()) {
                    alertSuccess(TallyDataActivity.this, "Creat success");
                } else {
                    Toast.makeText(TallyDataActivity.this, "Creat fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TSHeader> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void PostTSDetail(TSDetail tsDetail) {
        Call<TSDetail> callback = iWarehouseApi.creatTSDetail(tsDetail);
        callback.enqueue(new Callback<TSDetail>() {
            @Override
            public void onResponse(Call<TSDetail> call, Response<TSDetail> response) {
                if (response.isSuccessful()) {

                } else {
                    Toast.makeText(TallyDataActivity.this, "Creat fail", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TSDetail> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Failture", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_tally_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_addnew) {
            createTSNumber();
            edtGoodsID.requestFocus();
            return true;
        }
        if (id == R.id.action_save) {
            process();
            return true;
        }
        if (id == R.id.action_delete) {

            return true;
        }
        if (id == R.id.action_approve) {
            approve1();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.action_edit_delete_menu, menu);
//    }
//
//    @Override
//    public boolean onContextItemSelected(@NonNull MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.menuItem_edit:
//                Toast.makeText(this, "Bookmark", Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.menuItem_delete:
//                Toast.makeText(this, "Upload", Toast.LENGTH_SHORT).show();
//            default:
//                Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
//                break;
//        }
//        return true;
//    }

    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    String palletID = "";
                    String lotID = "";
                    String partNumber = "";
                    String partName = "";
                    String unit = "";
                    String mfgDate = "";
                    String expDate = "";
                    String quantity = "";
                    String idCode = "";
                    int index1 = result.indexOf("|");
                    int index2 = result.indexOf("|", index1 + 1);
                    int index3 = result.indexOf("|", index2 + 1);
                    int index4 = result.indexOf("|", index3 + 1);
                    int index5 = result.indexOf("|", index4 + 1);
                    int index6 = result.indexOf("|", index5 + 1);
                    int index7 = result.indexOf("|", index6 + 1);
                    int index8 = result.indexOf("|", index7 + 1);
                    int index9 = result.indexOf("|", index8 + 1);
                    int index10 = result.indexOf("|", index9 + 1);
                    int index11 = result.indexOf("|", index10 + 1);
                    int index12 = result.indexOf("|", index11 + 1);
                    int index13 = result.lastIndexOf("|");


                    if (index13 > 0) {
                        idCode = result.substring(index2 + 1, index3);
                        lotID = result.substring(index8 + 1, index9);
                        partNumber = result.substring(index5 + 1, index6);
                        partName = result.substring(index6 + 1, index7);
                        unit = result.substring(index4 + 1, index5);
                        mfgDate = result.substring(index9 + 1, index10);
                        expDate = result.substring(index10 + 1, index11);
                        quantity = result.substring(index3 + 1, index4);
                        palletID = idCode.substring(0, 5);

                        edtIDCode.setText(idCode);
                        edtQuantity.setText(quantity);
                        edtIDCode.setText(idCode);
                        edtGoodsID.setText(partNumber);
                        process();
                    } else {
                        playErrorSound(TallyDataActivity.this);
                    }
                }
            }
        });
    }

    private void process() {
        if (TextUtils.isEmpty(edtTSNumber.getText().toString())) {
            playErrorSound(TallyDataActivity.this);
            edtTSNumber.requestFocus();
        } else if (TextUtils.isEmpty(edtGoodsID.getText().toString())) {
            playErrorSound(TallyDataActivity.this);
            edtGoodsID.requestFocus();
        } else if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
            playErrorSound(TallyDataActivity.this);
            edtQuantity.requestFocus();
        }

        TallyData tallyData = new TallyData();
        tallyData.setTsNumber(edtTSNumber.getText().toString());
        tallyData.setTsDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
        tallyData.setPartNumber(edtGoodsID.getText().toString());
        tallyData.setPartName("");
        tallyData.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
        tallyData.setCreatorID(Common.USER_ID);
        tallyData.setCreatedDateTime(simpleDateFormat.format(Calendar.getInstance().getTime()));
        tallyData.setStatus("0");
        if (tallyDatas.size() > 0) {
            tallyData.setTotalQuantity(getTotalQuantity(tallyDatas) + Double.parseDouble(edtQuantity.getText().toString()));
            tallyData.setTotalRow(getTotalRow(tallyDatas) + 1);
        } else {
            tallyData.setTotalQuantity(Double.parseDouble(edtQuantity.getText().toString()));
            tallyData.setTotalRow(1.0);
        }
        tallyData.setNo(tallyDatas.size() + 1);
        PostTallyData(tallyData);
        hideSoftKeyBoard();

    }

    private void approve1() {
        Call<List<TSHeader>> callback = iWarehouseApi.getTSHeaderById(edtTSNumber.getText().toString());
        callback.enqueue(new Callback<List<TSHeader>>() {
            @Override
            public void onResponse(Call<List<TSHeader>> call, Response<List<TSHeader>> response) {
                if (response.isSuccessful()) {
                    tsHeaders = (ArrayList<TSHeader>) response.body();
                    if (tsHeaders.size() > 0) {
                        TSHeader tsHeaderUpdate = tsHeaders.get(0);
                        tsHeaderUpdate.setBranchID("000");
                        tsHeaderUpdate.setBranchName("TOKUDAI INDUSTRY VIETNAM CO., LTD");
                        tsHeaderUpdate.setWarehouseID("1");
                        tsHeaderUpdate.setWarehouseName("KHO CÔNG TY");
                        tsHeaderUpdate.setStatus("1");
                        tsHeaderUpdate.setTsDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                        tsHeaderUpdate.setUpdatedUserID(Common.USER_ID);
                        tsHeaderUpdate.setUpdatedDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                        tsHeaderUpdate.setTotalQuantity(Double.valueOf(edtTotalQuantity.getText().toString()));
                        PutTSHeader(edtTSNumber.getText().toString(), tsHeaderUpdate);

                        for (int i = 0; i < tallyDatas.size(); i++) {
                            Call<List<TSDetail>> callback = iWarehouseApi.getTSDetailById2(tallyDatas.get(i).getTsNumber(), tallyDatas.get(i).getPartNumber());
                            callback.enqueue(new Callback<List<TSDetail>>() {
                                @Override
                                public void onResponse(Call<List<TSDetail>> call, Response<List<TSDetail>> response) {
                                    if (response.isSuccessful()) {
                                        if (response.body().size() > 0) {
                                            TSDetail tsDetailUpdate = ((ArrayList<TSDetail>) response.body()).get(0);
                                            tsDetailUpdate.setTsNumber(edtTSNumber.getText().toString());
                                            tsDetailUpdate.setPartNumber(tsDetailUpdate.getPartNumber());
                                            tsDetailUpdate.setQuantity(tsDetailUpdate.getQuantity());
                                            tsDetailUpdate.setQuantityByPack(1.0);
                                            tsDetailUpdate.setOrdinal(tsDetailUpdate.getOrdinal());
                                            tsDetailUpdate.setStatus("1");
                                            PutTSDetail(tsDetailUpdate.getTsNumber(), tsDetailUpdate.getPartNumber(), tsDetailUpdate.getOrdinal(), tsDetailUpdate);
                                        } else {
                                            TSDetail tsDetailCreate = new TSDetail();
                                            tsDetailCreate.setTsNumber(edtTSNumber.getText().toString());
                                            tsDetailCreate.setPartNumber(edtGoodsID.getText().toString());
                                            tsDetailCreate.setQuantity(Double.valueOf(edtQuantity.getText().toString()));
                                            tsDetailCreate.setQuantityByPack(1.0);
                                            tsDetailCreate.setOrdinal(tallyDatas.size() + 1);
                                            tsDetailCreate.setStatus("0");
                                            PostTSDetail(tsDetailCreate);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<List<TSDetail>> call, Throwable t) {
                                    Toast.makeText(TallyDataActivity.this, "Fail", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    } else {
                        TSHeader tsHeader = new TSHeader();
                        tsHeader.setBranchID("000");
                        tsHeader.setBranchName("TOKUDAI INDUSTRY VIETNAM CO., LTD");
                        tsHeader.setWarehouseID("1");
                        tsHeader.setWarehouseName("KHO CÔNG TY");
                        tsHeader.setTsNumber(edtTSNumber.getText().toString());
                        tsHeader.setTsDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                        tsHeader.setHandlingStatusID("2");
                        tsHeader.setHandlingStatusName("Duyệt mức 1");
                        tsHeader.setTotalQuantity(Double.valueOf(edtTotalQuantity.getText().toString()));
                        tsHeader.setStatus("1");
                        tsHeader.setCreatedUserID(Common.USER_ID);
                        tsHeader.setCreatedDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                        PostTSHeader(tsHeader);

                        for (int i = 0; i < tallyDatas.size(); i++) {
                            TSDetail tsDetail = new TSDetail();
                            tsDetail.setTsNumber(edtTSNumber.getText().toString());
                            tsDetail.setPartNumber(tallyDatas.get(i).getPartNumber());
                            tsDetail.setQuantity(tallyDatas.get(i).getQuantity());
                            tsDetail.setQuantityByPack(1.0);
                            tsDetail.setOrdinal(tallyDatas.get(i).getNo());
                            tsDetail.setStatus("0");
                            PostTSDetail(tsDetail);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TSHeader>> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createTSNumber() {
        String currentDate = simpleDateFormat2.format(Calendar.getInstance().getTime());
        Call<String> callback = iWarehouseApi.getLastTSNumber();
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {

                    if (response.body().length() > 0) {
                        String lastTsNumber = response.body().toString();
                        String lastNum = lastTsNumber.substring(lastTsNumber.indexOf("S") + 1);
                        edtTSNumber.setText("KK" + currentDate + "_S" + (Integer.parseInt(lastNum) + 1));
                    } else {
                        edtTSNumber.setText("KK" + currentDate + "_S1");
                    }
                } else {
                    edtTSNumber.setText("KK" + currentDate + "_S1");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(TallyDataActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }
}