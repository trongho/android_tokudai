package com.example.tokudai.ui.pallet_manager;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.adapter.PalletHeaderAdapter;
import com.example.tokudai.model.PalletHeader;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PalletListActivity2 extends AppCompatActivity {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private EditText edtFromDate = null;
    private EditText edtToDate = null;
    private RecyclerView recyclerView = null;
    private TextView tvTotalPallet=null;
    private PalletHeaderAdapter adapter;
    AppCompatButton btnClear=null;
    ArrayList<PalletHeader> palletHeaderArrayList;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pallet_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Danh sách pallet");
        initComponent();

        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int dayOfMonth = mcurrentTime.get(Calendar.DAY_OF_MONTH);

        if (dayOfMonth < 10) {
            edtFromDate.setText(year + "-" + (month + 1) + "-" + "0" + dayOfMonth);
            edtToDate.setText(year + "-" + (month + 1) + "-" + "0" + dayOfMonth);
        } else {
            edtFromDate.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
            edtToDate.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
        }

        fetchPalletHeader(edtFromDate.getText().toString(),edtToDate.getText().toString());

        edtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseFromDate();
            }
        });

        edtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseToDate();
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchAllPalletHeader();
            }
        });
    }

    private void initComponent(){
        btnClear=findViewById(R.id.btnClear);
        edtFromDate=findViewById(R.id.edtFromDate);
        edtToDate=findViewById(R.id.edtToDate);
        recyclerView=findViewById(R.id.recycler_view);
        tvTotalPallet=findViewById(R.id.tvTotalPallet);
        palletHeaderArrayList=new ArrayList<PalletHeader>();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setAdapter(ArrayList<PalletHeader> palletHeaders) {
        adapter = new PalletHeaderAdapter(PalletListActivity2.this, palletHeaders, new PalletHeaderAdapter.ItemClickListener() {
            @Override
            public void onClick(PalletHeader palletHeader) {
                Intent intent = new Intent(PalletListActivity2.this,CreatePalletActivity.class);
                intent.putExtra( CreatePalletActivity.PALLET_ID,palletHeader.getPalletID());
                startActivity(intent);
            }
        });

        recyclerView.setLayoutManager(new GridLayoutManager(PalletListActivity2.this, 2));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    public void chooseFromDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(PalletListActivity2.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                if (dayOfMonth < 10) {
                    edtFromDate.setText(year + "-" + (month + 1) + "-" + "0" + dayOfMonth);
                } else {
                    edtFromDate.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
                }
                fetchPalletHeader(edtFromDate.getText().toString(),edtToDate.getText().toString());
            }
        }, year, month, day);
        datePickerDialog.setTitle("Từ ngày");
        datePickerDialog.show();
    }
    public void chooseToDate() {
        Calendar mcurrentTime = Calendar.getInstance();
        int year = mcurrentTime.get(Calendar.YEAR);
        int month = mcurrentTime.get(Calendar.MONTH);
        int day = mcurrentTime.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog;
        datePickerDialog = new DatePickerDialog(PalletListActivity2.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                if (dayOfMonth < 10) {
                    edtToDate.setText(year + "-" + (month + 1) + "-" + "0" + dayOfMonth);
                } else {
                    edtToDate.setText(year + "-" + (month + 1) + "-" + dayOfMonth);
                }
                fetchPalletHeader(edtFromDate.getText().toString(),edtToDate.getText().toString());
            }
        }, year, month, day);
        datePickerDialog.setTitle("Đến ngày");
        datePickerDialog.show();
    }

    private void fetchAllPalletHeader() {
        Call<List<PalletHeader>> callback = iWarehouseApi.getAllPalletHeader();
        callback.enqueue(new Callback<List<PalletHeader>>() {
            @Override
            public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                if (response.isSuccessful()) {
                    palletHeaderArrayList = (ArrayList<PalletHeader>) response.body();
                    if (palletHeaderArrayList.size() > 0) {
                        setAdapter(palletHeaderArrayList);
                        tvTotalPallet.setText("Σ Pallet: "+palletHeaderArrayList.size());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void fetchPalletHeader(String fromDate,String toDate) {
        Call<List<PalletHeader>> callback = iWarehouseApi.getPalletHeaderByCreatedDate(fromDate,toDate);
        callback.enqueue(new Callback<List<PalletHeader>>() {
            @Override
            public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                if (response.isSuccessful()) {
                    palletHeaderArrayList = (ArrayList<PalletHeader>) response.body();
                    if (palletHeaderArrayList.size() > 0) {
                        setAdapter(palletHeaderArrayList);
                        tvTotalPallet.setText("Σ Pallet: "+palletHeaderArrayList.size());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
    }


}