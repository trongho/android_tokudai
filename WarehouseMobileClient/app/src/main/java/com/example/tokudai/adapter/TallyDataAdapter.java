package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.TallyData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TallyDataAdapter extends RecyclerView.Adapter<TallyDataAdapter.ViewHolder> {
    Context context;
    ArrayList<TallyData> list;
    TallyData entry;
    TallyDataAdapter.ItemClickListener listener;
    TallyDataAdapter.ItemActionListener actionListener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public TallyDataAdapter(Context context, ArrayList<TallyData> list, TallyDataAdapter.ItemClickListener listener,ItemActionListener actionListener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.actionListener=actionListener;
    }

    @NonNull
    @Override
    public TallyDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.tally_data_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new TallyDataAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull TallyDataAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        entry = list.get(i);
        viewHolder.tvGoodsID.setText(entry.getPartNumber());
        viewHolder.tvQuantity.setText(entry.getQuantity() + "");
        viewHolder.tvNo.setText(entry.getNo()+".");
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });
        viewHolder.ivAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (actionListener != null) {
                    actionListener.onClick(list.get(i),i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvGoodsID, tvQuantity,tvNo;
        public ImageView ivAction;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvGoodsID = itemView.findViewById(R.id.tvPartNumber);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvNo=itemView.findViewById(R.id.tvNo);
            ivAction=itemView.findViewById(R.id.action);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<TallyData> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(TallyData tallyData);
    }
    public interface ItemActionListener {
        void onClick(TallyData tallyData,int position);
    }


}
