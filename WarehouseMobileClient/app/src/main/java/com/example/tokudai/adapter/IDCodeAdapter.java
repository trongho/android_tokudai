package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;

import java.text.SimpleDateFormat;
import java.util.List;

public class IDCodeAdapter extends RecyclerView.Adapter<IDCodeAdapter.ViewHolder> {
    Context context;
    List<String> list;
    ItemDeleteListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public IDCodeAdapter(Context context, List<String> list, ItemDeleteListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public IDCodeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.item_idcode, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new IDCodeAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull IDCodeAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        String s = this.list.get(i);
        viewHolder.tvIDCode.setText(s);
        viewHolder.tvLineNo.setText(i+1+"");
        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(s,i);
                    list.remove(i);
                    notifyItemRemoved(i);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIDCode,tvLineNo;
        public ImageView btnDelete;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvIDCode=itemView.findViewById(R.id.tvIDCode);
            tvLineNo=itemView.findViewById(R.id.tvNo);
            btnDelete=itemView.findViewById(R.id.btnDelete);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public interface ItemDeleteListener {
        void onClick(String s,int position);
    }

}
