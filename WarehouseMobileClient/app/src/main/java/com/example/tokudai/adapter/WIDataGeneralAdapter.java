package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.WIDataGeneral;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WIDataGeneralAdapter extends RecyclerView.Adapter<WIDataGeneralAdapter.ViewHolder> {
    Context context;
    ArrayList<WIDataGeneral> list;
    WIDataGeneral WIDataGeneral;
    WIDataGeneralAdapter.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WIDataGeneralAdapter(Context context, ArrayList<WIDataGeneral> list, WIDataGeneralAdapter.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WIDataGeneralAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WIDataGeneralAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WIDataGeneralAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        WIDataGeneral = list.get(i);
        viewHolder.tvPalletID.setText(WIDataGeneral.getPalletID());
        viewHolder.tvPartNumber.setText(WIDataGeneral.getPartNumber());
        viewHolder.tvQuantity.setText(String.format("%.0f",WIDataGeneral.getQuantity()));
        viewHolder.tvQuantityOrg.setText(String.format("%.0f",WIDataGeneral.getQuantityOrg()));
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });


        if(WIDataGeneral.getQuantity().doubleValue()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(WIDataGeneral.getQuantity().doubleValue()>0&&WIDataGeneral.getQuantity().doubleValue()<WIDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(WIDataGeneral.getQuantity().doubleValue()==WIDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(WIDataGeneral.getQuantity().doubleValue()>WIDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPalletID,tvPartNumber,tvQuantity,tvQuantityOrg,tvLineNo;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPalletID = itemView.findViewById(R.id.tvPalletID);
            tvPartNumber = itemView.findViewById(R.id.tvPartNumber);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WIDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WIDataGeneral WIDataGeneral);
    }

}
