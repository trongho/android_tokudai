package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TSHeader {
    @SerializedName("tsNumber")
    @Expose
    private String tsNumber;
    @SerializedName("tsDate")
    @Expose
    private String tsDate;
    @SerializedName("warehouseID")
    @Expose
    private String warehouseID;
    @SerializedName("warehouseName")
    @Expose
    private String warehouseName;
    @SerializedName("handlingStatusID")
    @Expose
    private String handlingStatusID;
    @SerializedName("handlingStatusName")
    @Expose
    private String handlingStatusName;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("branchID")
    @Expose
    private String branchID;
    @SerializedName("branchName")
    @Expose
    private String branchName;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("totalQuantityByPack")
    @Expose
    private Double totalQuantityByPack;
    @SerializedName("checkerID1")
    @Expose
    private String checkerID1;
    @SerializedName("checkerName1")
    @Expose
    private String checkerName1;
    @SerializedName("checkerID2")
    @Expose
    private String checkerID2;
    @SerializedName("checkerName2")
    @Expose
    private String checkerName2;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdUserID")
    @Expose
    private String createdUserID;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("updatedUserID")
    @Expose
    private String updatedUserID;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    public TSHeader() {
    }

    public TSHeader(String tsNumber, String tsDate, String warehouseID, String warehouseName, String handlingStatusID, String handlingStatusName, String note, String branchID, String branchName, Double totalQuantity, Double totalQuantityByPack, String checkerID1, String checkerName1, String checkerID2, String checkerName2, String status, String createdUserID, String createdDate, String updatedUserID, String updatedDate) {
        this.tsNumber = tsNumber;
        this.tsDate = tsDate;
        this.warehouseID = warehouseID;
        this.warehouseName = warehouseName;
        this.handlingStatusID = handlingStatusID;
        this.handlingStatusName = handlingStatusName;
        this.note = note;
        this.branchID = branchID;
        this.branchName = branchName;
        this.totalQuantity = totalQuantity;
        this.totalQuantityByPack = totalQuantityByPack;
        this.checkerID1 = checkerID1;
        this.checkerName1 = checkerName1;
        this.checkerID2 = checkerID2;
        this.checkerName2 = checkerName2;
        this.status = status;
        this.createdUserID = createdUserID;
        this.createdDate = createdDate;
        this.updatedUserID = updatedUserID;
        this.updatedDate = updatedDate;
    }

    public String getTsNumber() {
        return tsNumber;
    }

    public void setTsNumber(String tsNumber) {
        this.tsNumber = tsNumber;
    }



    public String getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getHandlingStatusID() {
        return handlingStatusID;
    }

    public void setHandlingStatusID(String handlingStatusID) {
        this.handlingStatusID = handlingStatusID;
    }

    public String getHandlingStatusName() {
        return handlingStatusName;
    }

    public void setHandlingStatusName(String handlingStatusName) {
        this.handlingStatusName = handlingStatusName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalQuantityByPack() {
        return totalQuantityByPack;
    }

    public void setTotalQuantityByPack(Double totalQuantityByPack) {
        this.totalQuantityByPack = totalQuantityByPack;
    }

    public String getCheckerID1() {
        return checkerID1;
    }

    public void setCheckerID1(String checkerID1) {
        this.checkerID1 = checkerID1;
    }

    public String getCheckerName1() {
        return checkerName1;
    }

    public void setCheckerName1(String checkerName1) {
        this.checkerName1 = checkerName1;
    }

    public String getCheckerID2() {
        return checkerID2;
    }

    public void setCheckerID2(String checkerID2) {
        this.checkerID2 = checkerID2;
    }

    public String getCheckerName2() {
        return checkerName2;
    }

    public void setCheckerName2(String checkerName2) {
        this.checkerName2 = checkerName2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedUserID() {
        return createdUserID;
    }

    public void setCreatedUserID(String createdUserID) {
        this.createdUserID = createdUserID;
    }


    public String getUpdatedUserID() {
        return updatedUserID;
    }

    public void setUpdatedUserID(String updatedUserID) {
        this.updatedUserID = updatedUserID;
    }

    public String getTsDate() {
        return tsDate;
    }

    public void setTsDate(String tsDate) {
        this.tsDate = tsDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
