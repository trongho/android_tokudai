package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryDetail {
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("month")
    @Expose
    private Integer month;
    @SerializedName("warehouseID")
    @Expose
    private String warehouseID;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("palletID")
    @Expose
    private String palletID;
    @SerializedName("lotID")
    @Expose
    private String lotID;
    @SerializedName("mfgDate")
    @Expose
    private String mfgDate;
    @SerializedName("expDate")
    @Expose
    private String expDate;
    @SerializedName("inputDate")
    @Expose
    private String inputDate;

    public InventoryDetail() {
    }

    public InventoryDetail(Integer year, Integer month, String warehouseID, String partNumber, Double quantity, String status, String idCode, String palletID, String lotID, String mfgDate, String expDate, String inputDate) {
        this.year = year;
        this.month = month;
        this.warehouseID = warehouseID;
        this.partNumber = partNumber;
        this.quantity = quantity;
        this.status = status;
        this.idCode = idCode;
        this.palletID = palletID;
        this.lotID = lotID;
        this.mfgDate = mfgDate;
        this.expDate = expDate;
        this.inputDate = inputDate;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    public String getLotID() {
        return lotID;
    }

    public void setLotID(String lotID) {
        this.lotID = lotID;
    }

    public String getMfgDate() {
        return mfgDate;
    }

    public void setMfgDate(String mfgDate) {
        this.mfgDate = mfgDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }
}
