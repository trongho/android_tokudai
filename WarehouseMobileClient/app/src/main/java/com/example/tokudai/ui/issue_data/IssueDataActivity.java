package com.example.tokudai.ui.issue_data;

import static com.example.tokudai.helper.MessageHelper.errorMessage;
import static com.example.tokudai.helper.MessageHelper.successMessage;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokudai.R;
import com.example.tokudai.adapter.WIDataDetailAdapter;
import com.example.tokudai.adapter.WIDataGeneralAdapter;
import com.example.tokudai.helper.ApiCallback;
import com.example.tokudai.helper.BottomMessage;
import com.example.tokudai.helper.Common;
import com.example.tokudai.model.Inventory;
import com.example.tokudai.model.InventoryDetail;
import com.example.tokudai.model.PalletHeader;
import com.example.tokudai.model.WIDataDetail;
import com.example.tokudai.model.WIDataGeneral;
import com.example.tokudai.model.WIDataHeader;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;
import com.example.tokudai.ui.pallet_manager.CreatePalletActivity;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IssueDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private TextView tvMsg = null;
    private RecyclerView recyclerView = null;
    private AutoCompleteTextView edtWIDNumber = null;
    private EditText edtPalletID = null;
    private EditText edtLotID = null;
    private EditText edtIDCode = null;
    private EditText edtQuantity = null;
    private EditText edtPartNumber = null;
    private EditText edtPartName = null;
    private EditText edtUnit = null;
    private EditText edtMFGDate = null;
    private EditText edtEXPDate = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnLockWIDNumber = null;
    private WIDataGeneralAdapter adapter;
    ArrayList<WIDataGeneral> WIDataGenerals;
    ArrayList<WIDataGeneral> arrayListByGoodsID;
    ArrayList<WIDataHeader> WIDataHeaders;
    WIDataHeader WIDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Boolean isGoodsIDExist = false;
    Boolean isGoodsIdDuplicate = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;
    Double totalGoodsOrg = 0.0;
    Double totalQuantityOrg = 0.0;

    ArrayList<Inventory> inventoryList = null;
    ArrayList<InventoryDetail> inventoryDetailArrayList = null;

    private View vBottom = null;

    boolean isWIDNumberLock = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Xuất hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();

        edtWIDNumber.requestFocus();
        autocompleteWIDNumber();

        WIDataHeaders = new ArrayList<>();
        WIDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
        inventoryList = new ArrayList<>();
        inventoryDetailArrayList = new ArrayList<>();


        edtWIDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWIDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 11) {
                    Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(edtWIDNumber.getText().toString().trim());
                    callback.enqueue(new Callback<List<WIDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                            if (response.isSuccessful()) {
                                WIDataHeaders = (ArrayList<WIDataHeader>) response.body();
                                if (WIDataHeaders.size() > 0) {
                                    playSuccesSound();
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    WIDataHeader = WIDataHeaders.get(0);
                                    fetchWIDataGeneralData(edtWIDNumber.getText().toString());
                                    edtTotalQuantityOrg.setText(String.format("%.0f", WIDataHeader.getTotalQuantityOrg()));
                                    edtTotalQuantity.setText(String.format("%.0f", WIDataHeader.getTotalQuantity()));
                                    edtWIDNumber.setFocusable(false);
                                    edtWIDNumber.setClickable(false);
                                    edtWIDNumber.setCursorVisible(false);
                                    btnLockWIDNumber.setBackgroundResource(R.drawable.ic_baseline_lock_24);
                                    isWIDNumberLock = true;
                                    clear();
                                } else {
//                                    playErrorSound();
//                                    alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu");
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch WIDataHeader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    fetchWIDataDetailData(edtWIDNumber.getText().toString());
                }
            }
        });

        edtPalletID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWIDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWIDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWIDNumber.requestFocus();
                    return;
                }

            }
        });

        btnLockWIDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isWIDNumberLock == true) {
                    edtWIDNumber.setFocusableInTouchMode(true);
                    edtWIDNumber.setFocusable(true);
                    edtWIDNumber.setClickable(true);
                    edtWIDNumber.setCursorVisible(true);
                    btnLockWIDNumber.setBackgroundResource(R.drawable.ic_baseline_lock_open_24);
                    isWIDNumberLock = false;
                } else {
                    edtWIDNumber.setFocusable(false);
                    edtWIDNumber.setClickable(false);
                    edtWIDNumber.setCursorVisible(false);
                    btnLockWIDNumber.setBackgroundResource(R.drawable.ic_baseline_lock_24);
                    isWIDNumberLock = true;
                }
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWIDNumber = findViewById(R.id.edtWIRNumber);
        edtPalletID = findViewById(R.id.edtPalletID);
        edtLotID = findViewById(R.id.edtLotID);
        edtIDCode = findViewById(R.id.edtIDCode);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
        vBottom = findViewById(R.id.vBottom);
        tvMsg = findViewById(R.id.tv_msg);
        edtPartNumber = findViewById(R.id.edtPartNumber);
        edtPartName = findViewById(R.id.edtPartName);
        edtUnit = findViewById(R.id.edtUnit);
        edtMFGDate = findViewById(R.id.edtMFGDate);
        edtEXPDate = findViewById(R.id.edtEXPDate);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnLockWIDNumber = findViewById(R.id.btn_lock_widNumber);
    }

    private void autocompleteWIDNumber() {
        Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeaders();
        callback.enqueue(new Callback<List<WIDataHeader>>() {
            @Override
            public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                if (response.isSuccessful()) {
                    ArrayList<String> stringArrayList = new ArrayList<>();
                    for (WIDataHeader wiDataHeader : (ArrayList<WIDataHeader>) response.body()) {
                        stringArrayList.add(wiDataHeader.getWidNumber());
                    }
                    ArrayAdapter adapterCountries
                            = new ArrayAdapter(IssueDataActivity.this, android.R.layout.simple_list_item_1, stringArrayList);
                    edtWIDNumber.setAdapter(adapterCountries);
                    edtWIDNumber.setThreshold(1);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clear() {
        edtIDCode.setText("");
        edtLotID.setText("");
        edtPartNumber.setText("");
        edtPalletID.setText("");
        edtPartName.setText("");
        edtMFGDate.setText("");
        edtEXPDate.setText("");
        edtQuantity.setText("");
        edtUnit.setText("");
    }

    private WIDataHeader fetchWIDataHeaderData(String WIDNumber) {
        Call<List<WIDataHeader>> callback = iWarehouseApi.getWIDataHeader(WIDNumber);
        callback.enqueue(new Callback<List<WIDataHeader>>() {
            @Override
            public void onResponse(Call<List<WIDataHeader>> call, Response<List<WIDataHeader>> response) {
                if (response.isSuccessful()) {
                    WIDataHeaders = (ArrayList<WIDataHeader>) response.body();
                    if (WIDataHeaders.size() > 0) {
                        WIDataHeader = WIDataHeaders.get(0);
                        hideSoftKeyBoard();
                        edtTotalQuantityOrg.setText(String.format("%.0f", WIDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", WIDataHeader.getTotalQuantity()));
                        edtPalletID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WIDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch WIDataHeader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return WIDataHeader;
    }

    private ArrayList<WIDataGeneral> fetchWIDataGeneralData(String WIDNumber) {
        Call<List<WIDataGeneral>> callback = iWarehouseApi.getWIDataGeneralById(WIDNumber);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                if (response.isSuccessful()) {
                    WIDataGenerals = (ArrayList<WIDataGeneral>) response.body();
                    if (WIDataGenerals.size() > 0) {
                        edtTotalGoodsOrg.setText(String.format("%.0f", WIDataGenerals.get(0).getTotalGoodsOrg()));
                        totalGoods = WIDataGenerals.get(0).getTotalGoodsOrg();
                        getTotalGoods(WIDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(WIDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return WIDataGenerals;
    }

    private void fetchWIDataDetail(String widNumber,String palletID,String partNumber,RecyclerView recyclerView){
        Call<List<WIDataDetail>> callback=iWarehouseApi.getWIDataDetailByParams3(widNumber,palletID,partNumber);
        callback.enqueue(new Callback<List<WIDataDetail>>() {
            @Override
            public void onResponse(Call<List<WIDataDetail>> call, Response<List<WIDataDetail>> response) {
                ArrayList<WIDataDetail> wiDataDetails=(ArrayList<WIDataDetail>)response.body();
                if(wiDataDetails.size()>0){
                    WIDataDetailAdapter wiDataDetailAdapter=new WIDataDetailAdapter(getApplicationContext(), wiDataDetails, new WIDataDetailAdapter.ItemDeleteListener() {
                        @Override
                        public void onClick(WIDataDetail wiDataDetail, int position) {
                            deleteWIDataDetail(widNumber);
                            fetchWIDataDetail(widNumber,palletID,partNumber, recyclerView);
                        }
                    });
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerView.setAdapter(wiDataDetailAdapter);
                    wiDataDetailAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<WIDataDetail>> call, Throwable t) {

            }
        });
    }

    private void putWIDataHeader(String WIDNumber, WIDataHeader WIDataHeader) {
        Call<WIDataHeader> callback = iWarehouseApi.updateWIDataHeader(WIDNumber, WIDataHeader);
        callback.enqueue(new Callback<WIDataHeader>() {
            @Override
            public void onResponse(Call<WIDataHeader> call, Response<WIDataHeader> response) {
                if (response.isSuccessful()) {
                    fetchWIDataHeaderData(WIDNumber);
                }
            }

            @Override
            public void onFailure(Call<WIDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put WIDataHeader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWIDataGeneral(String WIDNumber, String palletID, int ordinal,String partNumber, WIDataGeneral WIDataGeneral) {
        Call<WIDataGeneral> callback = iWarehouseApi.updateWIDataGeneral(WIDNumber, palletID,partNumber,ordinal, WIDataGeneral);
        callback.enqueue(new Callback<WIDataGeneral>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<WIDataGeneral> call, Response<WIDataGeneral> response) {
                if (response.isSuccessful()) {
                    successMessage(tvMsg, "Success");
                    fetchWIDataGeneralData(WIDNumber);
                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WIDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put WIDatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteWIDataDetail(String widNumber){
        Call<Boolean> callback=iWarehouseApi.deleteWIDataDetail(widNumber);
        callback.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                Log.d("delete widatadetail","success");
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.d("delete widatadetail",t.getMessage().toString());
            }
        });
    }

    private ArrayList<InventoryDetail> fetchInventoryDetail(int year, int month, String warhouseID, String partNumber, String palletID) {

        Call<List<InventoryDetail>> callback = iWarehouseApi.GetByParams5(year, month, warhouseID, partNumber, palletID);
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                if (response.isSuccessful()) {
                    inventoryDetailArrayList = (ArrayList<InventoryDetail>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return inventoryDetailArrayList;
    }

    private ArrayList<InventoryDetail> fetchInventoryDetail(int year, int month, String warhouseID, String partNumber, String palletID, String lotID) {

        Call<List<InventoryDetail>> callback = iWarehouseApi.GetByParams62(year, month, warhouseID, partNumber, palletID, lotID);
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                if (response.isSuccessful()) {
                    inventoryDetailArrayList = (ArrayList<InventoryDetail>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return inventoryDetailArrayList;
    }


    private ArrayList<InventoryDetail> fetchInventoryDetail(int year, int month, String warhouseID, final ApiCallback apiCallback) {

        Call<List<InventoryDetail>> callback = iWarehouseApi.GetByParams3(year, month, warhouseID);
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                if (response.isSuccessful()) {
                    inventoryDetailArrayList = (ArrayList<InventoryDetail>) response.body();
                    apiCallback.onSuccess(inventoryDetailArrayList);
                }

            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return inventoryDetailArrayList;
    }


    private void getTotalGoods(ArrayList<WIDataGeneral> wiDataGenerals) {
        for (int i = 0; i < wiDataGenerals.size(); i++) {
            if (wiDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                totalGoods -= 1;
            }
        }
    }

    private Boolean checkDuplicate(ArrayList<WIDataGeneral> WIDataGenerals, String partNumber, String lotID) {
        for (int i = 0; i < WIDataGenerals.size(); i++) {
            if (WIDataGenerals.get(i).getQuantity() > 0 && WIDataGenerals.get(i).getPartNumber().equalsIgnoreCase(partNumber)
                    && WIDataGenerals.get(i).getLotID().equalsIgnoreCase(lotID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistLabel(String partNumber, String lotID, ArrayList<WIDataGeneral> WIDataGenerals) {
        Boolean flag = false;
        for (int i = 0; i < WIDataGenerals.size(); i++) {
            if (partNumber.equalsIgnoreCase(WIDataGenerals.get(i).getPartNumber()) && lotID.equalsIgnoreCase(WIDataGenerals.get(i).getLotID())) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private Boolean checkPartNumberAndIDCodeScaned(String partNumber, String idCode, WIDataGeneral WIDataGeneral) {
        Boolean flag = false;
        if (!WIDataGeneral.getIdCode().equalsIgnoreCase("") || WIDataGeneral.getIdCode() != null) {
            ArrayList<String> listIDCode = new ArrayList<String>(Arrays.asList(WIDataGeneral.getIdCode().split(",")));
            for (int j = 0; j < listIDCode.size(); j++) {
                if (partNumber.equalsIgnoreCase(WIDataGeneral.getPartNumber()) && idCode.equalsIgnoreCase(listIDCode.get(j).toString().toUpperCase())
                        && WIDataGeneral.getQuantity().doubleValue() > 0) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    private ArrayList<WIDataGeneral> getNotYetList(ArrayList<WIDataGeneral> WIDataGenerals) {
        ArrayList<WIDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < WIDataGenerals.size(); i++) {
            if (WIDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(WIDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WIDataGeneral> getEnoughtYetList(ArrayList<WIDataGeneral> WIDataGenerals) {
        ArrayList<WIDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < WIDataGenerals.size(); i++) {
            if (WIDataGenerals.get(i).getQuantity() > 0 && WIDataGenerals.get(i).getQuantity() < WIDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(WIDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WIDataGeneral> getEnoughtList(ArrayList<WIDataGeneral> WIDataGenerals) {
        ArrayList<WIDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < WIDataGenerals.size(); i++) {
            if (WIDataGenerals.get(i).getQuantity() > 0 && WIDataGenerals.get(i).getQuantity().doubleValue() == WIDataGenerals.get(i).getQuantityOrg().doubleValue()) {
                enoughtList.add(WIDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WIDataGeneral> getOvertList(ArrayList<WIDataGeneral> WIDataGenerals) {
        ArrayList<WIDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < WIDataGenerals.size(); i++) {
            if (WIDataGenerals.get(i).getQuantity() > WIDataGenerals.get(i).getQuantityOrg()) {
                overList.add(WIDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void playErrorSound() {
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("error.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void playSuccesSound() {
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("ok.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(ArrayList<WIDataGeneral> WIDataGenerals) {
        adapter = new WIDataGeneralAdapter(getApplicationContext(), WIDataGenerals, new WIDataGeneralAdapter.ItemClickListener() {
            @Override
            public void onClick(WIDataGeneral WIDataGeneral) {
                AlertDialog.Builder builder=new AlertDialog.Builder(IssueDataActivity.this);
                ViewGroup viewGroup=findViewById(android.R.id.content);
                View dialogView=LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_widata_detail,viewGroup,false);
                builder.setView(dialogView);
                AlertDialog alertDialog=builder.create();
                alertDialog.show();

                RecyclerView mRecyclerView=alertDialog.findViewById(R.id.recycler_view_idcode);
                TextView mWIDNumber=alertDialog.findViewById(R.id.tvWIDNumer);
                TextView mPalletNumber=alertDialog.findViewById(R.id.tvPalletNumber);
                TextView mPartNumber=alertDialog.findViewById(R.id.tvPartNumber);
                AppCompatButton mXoaHet=alertDialog.findViewById(R.id.btnDeleteAll);
                AppCompatButton mCancel=alertDialog.findViewById(R.id.btnCancel);

                mWIDNumber.setText(WIDataGeneral.getWidNumber());
                mPalletNumber.setText(WIDataGeneral.getPalletID());
                mPartNumber.setText(WIDataGeneral.getPartNumber());

                String widNumber=mWIDNumber.getText().toString();
                String palletID=mPalletNumber.getText().toString();
                String partNumber=mPartNumber.getText().toString();
                fetchWIDataDetail(widNumber,palletID,partNumber,mRecyclerView);

                mXoaHet.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new android.app.AlertDialog.Builder(IssueDataActivity.this)
                                .setTitle("Cảnh báo!!!")
                                .setMessage("Bạn có muốn xóa toàn bộ mã hàng?")
                                .setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Call<Boolean> callback=iWarehouseApi.deleteWIDataDetail(widNumber);
                                        callback.enqueue(new Callback<Boolean>() {
                                            @Override
                                            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                                fetchWIDataDetail(widNumber,palletID,partNumber,recyclerView);
                                            }

                                            @Override
                                            public void onFailure(Call<Boolean> call, Throwable t) {
                                            }
                                        });
                                    }
                                })
                                .setPositiveButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                }).show();
                    }
                });

                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WIDataGeneral> allList = WIDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WIDataGeneral> notYetList = getNotYetList(WIDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WIDataGeneral> enoughtYetLish = getEnoughtYetList(WIDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WIDataGeneral> enoughtList = getEnoughtList(WIDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WIDataGeneral> overList = getOvertList(WIDataGenerals);
            setAdapter(overList);
            return true;
        }
        if (id == R.id.action_save) {
            xuatTungThung(edtPartNumber.getText().toString(),edtIDCode.getText().toString());
            return true;
        }
        if (id == R.id.action_approve) {
            if (WIDataHeader != null) {
                WIDataHeader WIDataHeaderUpdate = WIDataHeader;
                WIDataHeaderUpdate.setHandlingStatusID("1");
                WIDataHeaderUpdate.setHandlingStatusName("Duyệt mức 1");
                putWIDataHeader(edtWIDNumber.getText().toString(), WIDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 1 xuất kho kho thành công");
            }
            return true;
        }
        if (id == R.id.action_clear) {
            if (WIDataGenerals.size() > 0) {
                clearList();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtWIDNumber.hasFocus()) {
                        edtWIDNumber.setText(result);
                    } else {

                        String palletID = "";
                        String lotID = "";
                        String quantity = "";
                        String idCode = "";
                        String partNumber = "";
                        String partName = "";
                        String unit = "";
                        String mfgDate = "";
                        String expDate = "";
                        int index1 = result.indexOf("|");
                        int index2 = result.indexOf("|", index1 + 1);
                        int index3 = result.indexOf("|", index2 + 1);
                        int index4 = result.indexOf("|", index3 + 1);
                        int index5 = result.indexOf("|", index4 + 1);
                        int index6 = result.indexOf("|", index5 + 1);
                        int index7 = result.indexOf("|", index6 + 1);
                        int index8 = result.indexOf("|", index7 + 1);
                        int index9 = result.indexOf("|", index8 + 1);
                        int index10 = result.indexOf("|", index9 + 1);
                        int index11 = result.indexOf("|", index10 + 1);
                        int index12 = result.indexOf("|", index11 + 1);
                        int index13 = result.lastIndexOf("|");

                        if (result.length() == 5) {
//                            edtPalletID.setText(result);
//                            processAll();

                        } else {
                            if (index13 > 0) {
                                idCode = result.substring(index2 + 1, index3);
                                palletID = idCode.substring(0, 5);
                                lotID = result.substring(index8 + 1, index9);
                                quantity = result.substring(index3 + 1, index4);
                                partNumber = result.substring(index5 + 1, index6);
                                partName = result.substring(index6 + 1, index7);
                                unit = result.substring(index4 + 1, index5);
                                mfgDate = result.substring(index9 + 1, index10);
                                expDate = result.substring(index10 + 1, index11);
                                if (palletID.length() == 5) {

                                    edtPalletID.setText(palletID);
                                    edtLotID.setText(lotID);
                                    edtQuantity.setText(quantity);
                                    edtIDCode.setText(idCode);
                                    edtPartNumber.setText(partNumber);
                                    edtPartName.setText(partName);
                                    edtUnit.setText(unit);
                                    edtMFGDate.setText(mfgDate);
                                    edtEXPDate.setText(expDate);

                                    xuatTungThung(partNumber,idCode);
                                }
                            } else {
                                new android.app.AlertDialog.Builder(IssueDataActivity.this)
                                        .setTitle("Cảnh báo?")
                                        .setMessage("Xuất hết mã hàng ở pallet " + result.toString())
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                xuatTatCaHangTrongPalletNumber(result.toString());
                                            }
                                        })
                                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                            }
                                        }).show();

                                playErrorSound();
                                errorMessage(tvMsg, "Mã QR ko đúng");
                            }
                        }
                    }
                }
            }
        });
    }

    private void xuatTungThung(String partNumber,String idCode){
        String widNumber=edtWIDNumber.getText().toString();
        String dateTimeString = simpleDateFormat.format(Calendar.getInstance().getTime());
        int mYear = Integer.parseInt(dateTimeString.substring(0, 4));
        int mMonth = Integer.parseInt(dateTimeString.substring(5, 7));
        //
        boolean exist=true;
        if(WIDataGenerals.size()>0){
            for(WIDataGeneral wiDataGeneral:WIDataGenerals){
                if(!wiDataGeneral.getPartNumber().equalsIgnoreCase(partNumber)){
                    exist=false;
                    break;
                }
            }
            if(exist==false){
                alertError(IssueDataActivity.this,"Part Number không có trong phiếu xuất");
                return;
            }
        }

        Call<List<InventoryDetail>> callback=iWarehouseApi.GetInventoryDetailByIDCode(idCode);
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                ArrayList<InventoryDetail> inventoryDetails=(ArrayList<InventoryDetail>) response.body();
                if(inventoryDetails.size()==0){
                    alertError(IssueDataActivity.this,"Mã hàng "+idCode+" không có trong tồn kho");
                    return;
                }
                String palletID=inventoryDetails.get(0).getPalletID();
                String lotID=inventoryDetails.get(0).getLotID();
                double quantity=inventoryDetails.get(0).getQuantity();
                String mfgDate=inventoryDetails.get(0).getMfgDate();
                String expDate=inventoryDetails.get(0).getExpDate();

                Call<List<WIDataGeneral>> callback1=iWarehouseApi.GetWIDataGeneralByParams3(widNumber,inventoryDetails.get(0).getPalletID(),partNumber);
                callback1.enqueue(new Callback<List<WIDataGeneral>>() {
                    @Override
                    public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                        ArrayList<WIDataGeneral> wiDataGenerals=(ArrayList<WIDataGeneral>) response.body();
                        if(wiDataGenerals.size()>0){
                            WIDataGeneral wiDataGeneral=wiDataGenerals.get(0);


                            Call<List<WIDataDetail>> callbackWIDataDetail=iWarehouseApi.getWIDataDetailByParams5(widNumber,palletID,partNumber,lotID,idCode);
                            callbackWIDataDetail.enqueue(new Callback<List<WIDataDetail>>() {
                                @Override
                                public void onResponse(Call<List<WIDataDetail>> call, Response<List<WIDataDetail>> response) {
                                    ArrayList<WIDataDetail> wiDataDetails=(ArrayList<WIDataDetail>) response.body();
                                    if(wiDataDetails.size()>0){
                                        alertError(IssueDataActivity.this,"Đã scan");
                                        return;
                                    }

                                    updateWIDataGeneral(widNumber,palletID,partNumber,quantity);
                                    createWIDataDetail(widNumber,palletID,partNumber,lotID,quantity,mfgDate,expDate);
                                    fetchWIDataGeneralData(widNumber);
                                }

                                @Override
                                public void onFailure(Call<List<WIDataDetail>> call, Throwable t) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {

            }
        });
    }

    private void xuatTatCaHangTrongPalletNumber(String palletNumber){
        String widNumber=edtWIDNumber.getText().toString();
        String dateTimeString = simpleDateFormat.format(Calendar.getInstance().getTime());
        int mYear = Integer.parseInt(dateTimeString.substring(0, 4));
        int mMonth = Integer.parseInt(dateTimeString.substring(5, 7));
        Call<List<InventoryDetail>> inventoryDetailCallBack=iWarehouseApi.GetInventoryDetailByWarehouseAndPallet(mYear,mMonth,"1",palletNumber);
        inventoryDetailCallBack.enqueue(new Callback<List<InventoryDetail>>() {
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                ArrayList<InventoryDetail> inventoryDetailsByPalletNumber=(ArrayList<InventoryDetail>)response.body();
                if(inventoryDetailsByPalletNumber.size()>0){
                    for(InventoryDetail inventoryDetail:inventoryDetailsByPalletNumber){
                        updateWIDataGeneral(widNumber,inventoryDetail.getPalletID(),inventoryDetail.getPartNumber(),inventoryDetail.getQuantity());
                        createWIDataDetail(widNumber,inventoryDetail.getPalletID(),inventoryDetail.getPartNumber(),inventoryDetail.getLotID(),inventoryDetail.getQuantity(),
                                inventoryDetail.getMfgDate(),inventoryDetail.getExpDate());
                        fetchWIDataGeneralData(widNumber);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {

            }
        });
    }

    private void updateWIDataGeneral(String widNumber,String palletID,String partNumber,double quantiy){
        Call<List<WIDataGeneral>> callback=iWarehouseApi.GetWIDataGeneralByParams3(widNumber,palletID,partNumber);
        callback.enqueue(new Callback<List<WIDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
                ArrayList<WIDataGeneral> wiDataGenerals=(ArrayList<WIDataGeneral>) response.body();
                if(wiDataGenerals.size()>0){
                    WIDataGeneral wiDataGeneral=wiDataGenerals.get(0);
                    wiDataGeneral.setQuantity(wiDataGeneral.getQuantity()+quantiy);

                    Call<WIDataGeneral> callbackUpdate=iWarehouseApi.updateWIDataGeneral(wiDataGeneral.getWidNumber(),wiDataGeneral.getPalletID(),wiDataGeneral.getPartNumber(),wiDataGeneral.getOrdinal(),wiDataGeneral);
                    callbackUpdate.enqueue(new Callback<WIDataGeneral>() {
                        @Override
                        public void onResponse(Call<WIDataGeneral> call, Response<WIDataGeneral> response) {

                        }

                        @Override
                        public void onFailure(Call<WIDataGeneral> call, Throwable t) {
                            Log.d("update widatageneral",t.getMessage().toString());
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
                Log.d("get widatageneral",t.getMessage().toString());
            }
        });

    }

    private void createWIDataDetail(String widNumber,String palletID,String partNumber,String lotID,double quantity,String mfgDate,String expDate){
        Call<List<WIDataDetail>> callback=iWarehouseApi.getWIDataDetailById(widNumber);
        callback.enqueue(new Callback<List<WIDataDetail>>() {
            @Override
            public void onResponse(Call<List<WIDataDetail>> call, Response<List<WIDataDetail>> response) {
                ArrayList<WIDataDetail> wiDataDetails=(ArrayList<WIDataDetail>) response.body();
                WIDataDetail wiDataDetail=new WIDataDetail();
                wiDataDetail.setWidNumber(widNumber);
                wiDataDetail.setOrdinal(wiDataDetails.size()+1);
                wiDataDetail.setPalletID(palletID);
                wiDataDetail.setPartNumber(partNumber);
                wiDataDetail.setLotID(lotID);
                wiDataDetail.setQuantity(quantity);
                try {
                    wiDataDetail.setMfgDate(simpleDateFormat.format(originalFormat.parse(mfgDate)));
                    wiDataDetail.setExpDate(simpleDateFormat.format(originalFormat.parse(expDate)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                wiDataDetail.setCreatorID(Common.USER_ID);
                wiDataDetail.setCreatedDateTime(simpleDateFormat.format(Calendar.getInstance().getTime()));
                wiDataDetail.setStatus("New");

                Call<WIDataDetail> callbackCreate=iWarehouseApi.createWIDataDetail(wiDataDetail);
                callbackCreate.enqueue(new Callback<WIDataDetail>() {
                    @Override
                    public void onResponse(Call<WIDataDetail> call, Response<WIDataDetail> response) {
                        Log.d("create widatadetail","success");
                    }

                    @Override
                    public void onFailure(Call<WIDataDetail> call, Throwable t) {
                        Log.d("create widatadetail",t.getMessage().toString());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<WIDataDetail>> call, Throwable t) {
                Log.d("get widatadetail",t.getMessage().toString());
            }
        });
    }


//    private void processAll(String palletNumber) {
//        Call<List<WIDataGeneral>> callback = iWarehouseApi.GetWIDataGeneralByParams3(edtWIDNumber.getText().toString(),palletNumber,);
//        callback.enqueue(new Callback<List<WIDataGeneral>>() {
//            @Override
//            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByGoodsID = (ArrayList<WIDataGeneral>) response.body();
//                    for (int i = 0; i < arrayListByGoodsID.size(); i++) {
//                        if (arrayListByGoodsID.get(i).getQuantity() == 0) {
//                            WIDataGeneral WIDataGeneralUpdate = arrayListByGoodsID.get(i);
//                            WIDataGeneralUpdate.setQuantity(WIDataGeneralUpdate.getQuantityOrg());
//                            WIDataGeneralUpdate.setTotalGoods(totalGoods - 1);
//                            WIDataGeneralUpdate.setTotalQuantity(WIDataHeader.getTotalQuantity() - WIDataGeneralUpdate.getQuantityOrg());
//                            WIDataGeneralUpdate.setOutputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
//
//                            String dateTimeString = simpleDateFormat.format(Calendar.getInstance().getTime());
//                            int mYear = Integer.parseInt(dateTimeString.substring(0, 4));
//                            int mMonth = Integer.parseInt(dateTimeString.substring(5, 7));
//
//                            Call<List<InventoryDetail>> inventoryDetailCallBack=iWarehouseApi.GetByParams5(mYear,mMonth,"1",WIDataGeneralUpdate.getPartNumber(),WIDataGeneralUpdate.getPalletID());
//                            inventoryDetailCallBack.enqueue(new Callback<List<InventoryDetail>>() {
//                                @Override
//                                public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
//                                    ArrayList<InventoryDetail> inventoryDetails=(ArrayList<InventoryDetail>)response.body();
//                                    for(InventoryDetail inventoryDetail:inventoryDetails){
//                                        WIDataGeneralUpdate.setIdCode(WIDataGeneralUpdate.getIdCode() +inventoryDetail.getIdCode()+ ",");
//                                    }
//
//                                    WIDataGeneralUpdate.setOutputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
//                                    WIDataGeneralUpdate.setIdCode(WIDataGeneralUpdate.getIdCode() + edtIDCode.getText().toString().toUpperCase() + ",");
//                                    WIDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
//                                    WIDataGeneralUpdate.setTotalQuantity(WIDataHeader.getTotalQuantity() - Double.parseDouble(edtQuantity.getText().toString()));
//                                    putWIDataGeneral(WIDataGeneralUpdate.getWidNumber(), WIDataGeneralUpdate.getPalletID(), WIDataGeneralUpdate.getOrdinal(), WIDataGeneralUpdate);
//                                }
//
//                                @Override
//                                public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
//
//                                }
//                            });
//                        }
//                    }
//
//                    WIDataHeader WIDataHeaderUpdate = WIDataHeader;
//                    WIDataHeaderUpdate.setTotalQuantity(WIDataHeader.getTotalQuantity() - arrayListByGoodsID.get(0).getTotalQuantityOrg());
//                    putWIDataHeader(edtWIDNumber.getText().toString(), WIDataHeaderUpdate);
//                    playSuccesSound();
//                    successMessage(tvMsg, "Xuất thành công hàng hóa của pallet " + arrayListByGoodsID.get(0).getPalletID());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get WIDatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//    }

//    private void process() {
//        if (TextUtils.isEmpty(edtPalletID.getText().toString())) {
//            alertError(getApplicationContext(), "Nhập Pallet ID");
//            edtPalletID.requestFocus();
//            return;
//        }
//        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
//            alertError(getApplicationContext(), "Nhập số lượng");
//            edtQuantity.requestFocus();
//            return;
//        }
//
//        if (checkExistLabel(edtPartNumber.getText().toString(), edtLotID.getText().toString(), WIDataGenerals) == false) {
//            playErrorSound();
//            errorMessage(tvMsg, "Dữ liệu tem không có trong yêu cầu xuất");
//            return;
//        }
//
//        Call<List<WIDataGeneral>> callback = iWarehouseApi.GetWIDataGeneralByParams2(edtWIDNumber.getText().toString(), edtPartNumber.getText().toString());
//        callback.enqueue(new Callback<List<WIDataGeneral>>() {
//            @Override
//            public void onResponse(Call<List<WIDataGeneral>> call, Response<List<WIDataGeneral>> response) {
//                if (response.isSuccessful()) {
//                    arrayListByGoodsID = (ArrayList<WIDataGeneral>) response.body();
//                    if (arrayListByGoodsID.size() > 0) {
//                        WIDataGeneral WIDataGeneralUpdate = arrayListByGoodsID.get(0);
//
//                        if (checkPartNumberAndIDCodeScaned(edtPartNumber.getText().toString(), edtIDCode.getText().toString(), WIDataGeneralUpdate) == true) {
//                            playErrorSound();
//                            errorMessage(tvMsg, "Mã hàng đã scan");
//                            return;
//                        }
//
//                        if (Math.round(WIDataGeneralUpdate.getQuantity()) + Long.parseLong(edtQuantity.getText().toString()) > Math.round(WIDataGeneralUpdate.getQuantityOrg())) {
//                            playErrorSound();
//                            errorMessage(tvMsg, "Vượt số lượt cần xuất !!!");
//                            return;
//                        }
//
////                        Call<List<InventoryDetail>> inventoryDetailCallBack=iWarehouseApi.GetByParams5(mYear,mMonth,"1",WIDataGeneralUpdate.getPartNumber(),WIDataGeneralUpdate.getPalletID());
////                        inventoryDetailCallBack.enqueue(new Callback<List<InventoryDetail>>() {
////                            @Override
////                            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
////
////                            }
////
////                            @Override
////                            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
////
////                            }
////                        });
//
//                        playSuccesSound();
//                        checkDuplicate(arrayListByGoodsID, edtPartNumber.getText().toString(), edtLotID.getText().toString());
//                        if (isGoodsIdDuplicate == false) {
//                            WIDataGeneralUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
//                            WIDataGeneralUpdate.setTotalGoods(totalGoods - 1);
//                        } else {
//                            WIDataGeneralUpdate.setQuantity(WIDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
//                            WIDataGeneralUpdate.setTotalGoods(totalGoods);
//                            isGoodsIdDuplicate = false;
//                        }
//                        WIDataGeneralUpdate.setWidNumber(edtWIDNumber.getText().toString());
//                        WIDataGeneralUpdate.setOutputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
//                        WIDataGeneralUpdate.setIdCode(WIDataGeneralUpdate.getIdCode() + edtIDCode.getText().toString().toUpperCase() + ",");
//                        WIDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
//                        WIDataGeneralUpdate.setTotalQuantity(WIDataHeader.getTotalQuantity() - Double.parseDouble(edtQuantity.getText().toString()));
//                        putWIDataGeneral(WIDataGeneralUpdate.getWidNumber(), WIDataGeneralUpdate.getPalletID(), WIDataGeneralUpdate.getOrdinal(),WIDataGeneralUpdate.getPartNumber() ,WIDataGeneralUpdate);
//
//                        WIDataHeader WIDataHeaderUpdate = WIDataHeader;
//                        WIDataHeaderUpdate.setTotalQuantity(WIDataHeader.getTotalQuantity() - Double.parseDouble(edtQuantity.getText().toString()));
//                        putWIDataHeader(edtWIDNumber.getText().toString(), WIDataHeaderUpdate);
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<WIDataGeneral>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "get WIDatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void clearList() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(IssueDataActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_clear_list, viewGroup, false);
        builder.setView(dialogView);
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteWIDataDetail(edtWIDNumber.getText().toString());

                for (int i = 0; i < WIDataGenerals.size(); i++) {
                    WIDataGeneral WIDataGeneralUpdate = WIDataGenerals.get(i);
                    WIDataGeneralUpdate.setQuantity(0.0);
                    WIDataGeneralUpdate.setTotalGoods(WIDataGenerals.get(i).getTotalGoodsOrg());
                    WIDataGeneralUpdate.setTotalQuantity(WIDataGenerals.get(i).getTotalQuantityOrg());
                    WIDataGeneralUpdate.setOutputDate(null);
                    WIDataGeneralUpdate.setIdCode("");
                    putWIDataGeneral(WIDataGeneralUpdate.getWidNumber(), WIDataGeneralUpdate.getPalletID(), WIDataGeneralUpdate.getOrdinal(),WIDataGeneralUpdate.getPartNumber(), WIDataGeneralUpdate);
                }
                WIDataHeader WIDataHeaderUpdate = WIDataHeader;
                WIDataHeaderUpdate.setTotalQuantity(WIDataHeader.getTotalQuantityOrg());
                putWIDataHeader(edtWIDNumber.getText().toString().toUpperCase(), WIDataHeaderUpdate);
                alertDialog.cancel();
                successMessage(tvMsg, "Đã xóa dữ liệu quét" + edtWIDNumber.getText().toString().toUpperCase());
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}