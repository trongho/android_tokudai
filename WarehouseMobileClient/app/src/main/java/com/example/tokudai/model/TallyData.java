package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TallyData {
    @SerializedName("tsNumber")
    @Expose
    private String tsNumber;
    @SerializedName("tsDate")
    @Expose
    private String tsDate;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("partName")
    @Expose
    private String partName;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("totalRow")
    @Expose
    private Double totalRow;
    @SerializedName("creatorID")
    @Expose
    private String creatorID;
    @SerializedName("createdDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("editerID")
    @Expose
    private String editerID;
    @SerializedName("editedDateTime")
    @Expose
    private String editedDateTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("no")
    @Expose
    private Integer no;

    public TallyData() {
    }

    public TallyData(String tsNumber, String tsDate, String partNumber, String partName, String idCode, Double quantity, Double totalQuantity, Double totalRow, String creatorID, String createdDateTime, String editerID, String editedDateTime, String status, Integer no) {
        this.tsNumber = tsNumber;
        this.tsDate = tsDate;
        this.partNumber = partNumber;
        this.partName = partName;
        this.idCode = idCode;
        this.quantity = quantity;
        this.totalQuantity = totalQuantity;
        this.totalRow = totalRow;
        this.creatorID = creatorID;
        this.createdDateTime = createdDateTime;
        this.editerID = editerID;
        this.editedDateTime = editedDateTime;
        this.status = status;
        this.no = no;
    }

    public String getTsNumber() {
        return tsNumber;
    }

    public void setTsNumber(String tsNumber) {
        this.tsNumber = tsNumber;
    }

    public String getTsDate() {
        return tsDate;
    }

    public void setTsDate(String tsDate) {
        this.tsDate = tsDate;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(Double totalRow) {
        this.totalRow = totalRow;
    }

    public String getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(String creatorID) {
        this.creatorID = creatorID;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getEditerID() {
        return editerID;
    }

    public void setEditerID(String editerID) {
        this.editerID = editerID;
    }

    public String getEditedDateTime() {
        return editedDateTime;
    }

    public void setEditedDateTime(String editedDateTime) {
        this.editedDateTime = editedDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNo() {
        return no;
    }

    public void setNo(Integer no) {
        this.no = no;
    }
}
