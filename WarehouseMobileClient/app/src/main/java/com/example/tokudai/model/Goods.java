package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Goods {
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("palletID")
    @Expose
    private String palletID;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("partName")
    @Expose
    private String partName;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("productFamily")
    @Expose
    private String productFamily;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("mold")
    @Expose
    private String mold;
    @SerializedName("cartonName")
    @Expose
    private String cartonName;
    @SerializedName("cartonSize")
    @Expose
    private String cartonSize;
    @SerializedName("pads")
    @Expose
    private String pads;
    @SerializedName("nilon")
    @Expose
    private String nilon;
    @SerializedName("labelSize")
    @Expose
    private String labelSize;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("createdUserID")
    @Expose
    private String createdUserID;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("updatedUserID")
    @Expose
    private String updatedUserID;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    public Goods() {
    }

    public Goods(String partNumber, String palletID, Integer ordinal, String partName, String unit, String productFamily, Double quantity, String mold, String cartonName, String cartonSize, String pads, String nilon, String labelSize, String material, String status, String createdUserID, String createdDate, String updatedUserID, String updatedDate) {
        this.partNumber = partNumber;
        this.palletID = palletID;
        this.ordinal = ordinal;
        this.partName = partName;
        this.unit = unit;
        this.productFamily = productFamily;
        this.quantity = quantity;
        this.mold = mold;
        this.cartonName = cartonName;
        this.cartonSize = cartonSize;
        this.pads = pads;
        this.nilon = nilon;
        this.labelSize = labelSize;
        this.material = material;
        this.status = status;
        this.createdUserID = createdUserID;
        this.createdDate = createdDate;
        this.updatedUserID = updatedUserID;
        this.updatedDate = updatedDate;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getMold() {
        return mold;
    }

    public void setMold(String mold) {
        this.mold = mold;
    }

    public String getCartonName() {
        return cartonName;
    }

    public void setCartonName(String cartonName) {
        this.cartonName = cartonName;
    }

    public String getCartonSize() {
        return cartonSize;
    }

    public void setCartonSize(String cartonSize) {
        this.cartonSize = cartonSize;
    }

    public String getPads() {
        return pads;
    }

    public void setPads(String pads) {
        this.pads = pads;
    }

    public String getNilon() {
        return nilon;
    }

    public void setNilon(String nilon) {
        this.nilon = nilon;
    }

    public String getLabelSize() {
        return labelSize;
    }

    public void setLabelSize(String labelSize) {
        this.labelSize = labelSize;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedUserID() {
        return createdUserID;
    }

    public void setCreatedUserID(String createdUserID) {
        this.createdUserID = createdUserID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUserID() {
        return updatedUserID;
    }

    public void setUpdatedUserID(String updatedUserID) {
        this.updatedUserID = updatedUserID;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
