package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.PalletHeader;
import com.example.tokudai.model.WRDataHeader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PalletHeaderAdapter extends RecyclerView.Adapter<PalletHeaderAdapter.ViewHolder> {
    Context context;
    ArrayList<PalletHeader> list;
    PalletHeader palletHeader;
    ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public PalletHeaderAdapter(Context context, ArrayList<PalletHeader> list, ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.pallet_header_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        palletHeader = list.get(i);
        try {
            viewHolder.tvPalletID.setText(palletHeader.getPalletID());
            Date createdDate = originalFormat.parse(palletHeader.getCreatedDate());
            viewHolder.tvCreateDate.setText(simpleDateFormat.format(createdDate));
            viewHolder.tvTotalIDCode.setText(String.format("%.0f",palletHeader.getTotalIDCode()));
            for(int j=0;j<=i;j++){
                viewHolder.tvLineNo.setText(j+1+"");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        if(palletHeader.getHandlingStatusID().equalsIgnoreCase("1")){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(palletHeader.getHandlingStatusID().equalsIgnoreCase("2")){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#ccffcc"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLineNo,tvPalletID, tvCreateDate,tvTotalIDCode;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvLineNo= itemView.findViewById(R.id.tvLineNo);
            tvPalletID = itemView.findViewById(R.id.tvPalletID);
            tvCreateDate=itemView.findViewById(R.id.tvCreatedDate);
            tvTotalIDCode=itemView.findViewById(R.id.tvTotalIDCode);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public interface ItemClickListener {
        void onClick(PalletHeader palletHeader);
    }

}
