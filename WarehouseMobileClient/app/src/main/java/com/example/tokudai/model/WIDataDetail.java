package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WIDataDetail {
    @SerializedName("widNumber")
    @Expose
    private String widNumber;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("palletID")
    @Expose
    private String palletID;
    @SerializedName("lotID")
    @Expose
    private String lotID;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("partName")
    @Expose
    private String partName;
    @SerializedName("productFamily")
    @Expose
    private String productFamily;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("mfgDate")
    @Expose
    private String mfgDate;
    @SerializedName("expDate")
    @Expose
    private String expDate;
    @SerializedName("creatorID")
    @Expose
    private String creatorID;
    @SerializedName("createdDateTime")
    @Expose
    private String createdDateTime;
    @SerializedName("editerID")
    @Expose
    private String editerID;
    @SerializedName("editedDateTime")
    @Expose
    private String editedDateTime;
    @SerializedName("status")
    @Expose
    private String status;

    public WIDataDetail() {
    }

    public WIDataDetail(String widNumber, Integer ordinal, String partNumber, String palletID, String lotID, String idCode, String partName, String productFamily, Double quantity, String mfgDate, String expDate, String creatorID, String createdDateTime, String editerID, String editedDateTime, String status) {
        this.widNumber = widNumber;
        this.ordinal = ordinal;
        this.partNumber = partNumber;
        this.palletID = palletID;
        this.lotID = lotID;
        this.idCode = idCode;
        this.partName = partName;
        this.productFamily = productFamily;
        this.quantity = quantity;
        this.mfgDate = mfgDate;
        this.expDate = expDate;
        this.creatorID = creatorID;
        this.createdDateTime = createdDateTime;
        this.editerID = editerID;
        this.editedDateTime = editedDateTime;
        this.status = status;
    }

    public String getWidNumber() {
        return widNumber;
    }

    public void setWidNumber(String widNumber) {
        this.widNumber = widNumber;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    public String getLotID() {
        return lotID;
    }

    public void setLotID(String lotID) {
        this.lotID = lotID;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getMfgDate() {
        return mfgDate;
    }

    public void setMfgDate(String mfgDate) {
        this.mfgDate = mfgDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getCreatorID() {
        return creatorID;
    }

    public void setCreatorID(String creatorID) {
        this.creatorID = creatorID;
    }

    public String getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(String createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getEditerID() {
        return editerID;
    }

    public void setEditerID(String editerID) {
        this.editerID = editerID;
    }

    public String getEditedDateTime() {
        return editedDateTime;
    }

    public void setEditedDateTime(String editedDateTime) {
        this.editedDateTime = editedDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
