package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.PalletDetail;
import com.example.tokudai.model.WRDataGeneral;
import com.example.tokudai.ui.receipt_data_scan.ReceiptDataScanActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CreatePalletIDAdapter extends RecyclerView.Adapter<CreatePalletIDAdapter.ViewHolder> {
    private ReceiptDataScanActivity receiptDataScanActivity;
    Context context;
    ArrayList<PalletDetail> list;
    PalletDetail palletDetail;
    ItemClickListener listener;
    ItemDeleteListener itemDeleteListener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    boolean isEnable=false;
    boolean isSelectAll=false;

    public CreatePalletIDAdapter(Context context, ArrayList<PalletDetail> list, CreatePalletIDAdapter.ItemClickListener listener, ItemDeleteListener itemDeleteListener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.itemDeleteListener=itemDeleteListener;
    }

    @NonNull
    @Override
    public CreatePalletIDAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.create_pallet_id_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new CreatePalletIDAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull CreatePalletIDAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        palletDetail = list.get(i);
        viewHolder.tvIDCode.setText(palletDetail.getIdCode());
        viewHolder.tvPartNumber.setText(palletDetail.getPartNumber());
        viewHolder.tvLotID.setText(palletDetail.getLotID());
        viewHolder.tvQuantity.setText(String.format("%.0f",palletDetail.getQuantity()));

        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemDeleteListener != null) {
                    itemDeleteListener.onClick(list.get(i));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIDCode,tvPartNumber,tvLotID, tvQuantity,tvLineNo;
        public ImageView chkBox;
        public CardView cardView;
        public Button btnDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvIDCode = itemView.findViewById(R.id.tvIDCode);
            tvPartNumber = itemView.findViewById(R.id.tvPartNumber);
            tvLotID=itemView.findViewById(R.id.tvLotID);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
            btnDelete=itemView.findViewById(R.id.btnDelete);
        }
    }

    public interface ItemClickListener {
        void onClick(PalletDetail palletDetail);
    }

    public interface ItemDeleteListener {
        void onClick(PalletDetail palletDetail);
    }
}
