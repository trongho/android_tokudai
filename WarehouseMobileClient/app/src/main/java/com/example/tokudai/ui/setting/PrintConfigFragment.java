package com.example.tokudai.ui.setting;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tokudai.R;
import com.example.tokudai.helper.MessageHelper;
import com.example.tokudai.helper.PrintSettingHelper;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveryException;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;
import com.zebra.sdk.printer.discovery.NetworkDiscoverer;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PrintConfigFragment extends Fragment {
    public static final String SUBNET_SEARCH_RANGE = "SUBNET_SEARCH_RANGE";
    public static PrintConfigFragment newInstance(String text) {

        PrintConfigFragment  f = new PrintConfigFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_print_config, container, false);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button discoverButton = (Button) view.findViewById(R.id.do_subnet_search);
        EditText subnetSearchRange = (EditText) view.findViewById(R.id.subnet_search_range);

        subnetSearchRange.setText(PrintSettingHelper.getIp(getContext()));

        discoverButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                PrintSettingHelper.saveIp(getContext(),subnetSearchRange.getText().toString());
            }
        });

    }
}