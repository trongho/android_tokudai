package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HandlingStatus {
    @SerializedName("HandlingStatusID")
    @Expose
    private String HandlingStatusID;
    @SerializedName("HandlingStatusName")
    @Expose
    private String HandlingStatusName;
    @SerializedName("HandlingStatusNameEN")
    @Expose
    private String HandlingStatusNameEN;

    public HandlingStatus(String handlingStatusID, String handlingStatusName, String handlingStatusNameEN) {
        HandlingStatusID = handlingStatusID;
        HandlingStatusName = handlingStatusName;
        HandlingStatusNameEN = handlingStatusNameEN;
    }

    public HandlingStatus() {
    }
}
