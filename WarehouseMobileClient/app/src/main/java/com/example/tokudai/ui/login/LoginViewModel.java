package com.example.tokudai.ui.login;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.tokudai.model.User;

public class LoginViewModel extends ViewModel {

    public LoginViewModel() {
        mUsername = new MutableLiveData<>();
        mPassword = new MutableLiveData<>();
        mUsername.setValue("admin");
        mPassword.setValue("123456");
    }

    private MutableLiveData<String> mUsername;
    private MutableLiveData<String> mPassword;
    private MutableLiveData<User> user;

    public MutableLiveData<String> getmUsername() {
        return mUsername;
    }
    public MutableLiveData<String> getmPassword() {
        return mPassword;
    }

//    public MutableLiveData<User> login() {
//        user=warehouseRepository.login(mUsername.getValue(),mPassword.getValue());
//        return user;
//    }
}
