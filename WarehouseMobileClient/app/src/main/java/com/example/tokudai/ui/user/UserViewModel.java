package com.example.tokudai.ui.user;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class UserViewModel extends ViewModel {

    private MutableLiveData<String> mUsername;
    private MutableLiveData<String> mFullname;
    private MutableLiveData<String> mPassword;

    public UserViewModel() {
        mUsername = new MutableLiveData<>();
        mFullname = new MutableLiveData<>();
        mPassword = new MutableLiveData<>();
        mUsername.setValue("admin");
        mFullname.setValue("Admin");
        mPassword.setValue("123456");
    }

    public MutableLiveData<String> getmUsername() {
        return mUsername;
    }

    public MutableLiveData<String> getmFullname() {
        return mFullname;
    }

    public MutableLiveData<String> getmPassword() {
        return mPassword;
    }
}