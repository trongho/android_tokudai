/***********************************************
 CONFIDENTIAL AND PROPRIETARY
 The source code and other information contained herein is the confidential and the exclusive property of
 ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 Copyright ZIH Corp. 2015
 ALL RIGHTS RESERVED
 ***********************************************/

package com.example.tokudai.helper;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.ResultReceiver;
import android.util.Log;

import com.example.tokudai.R;
import com.example.tokudai.event.StatusIntentCompleteEvent;
import com.example.tokudai.ui.setting.ConnectedPrinterFragment;

import de.greenrobot.event.EventBus;


public class PrinterStatusHelper {

    private static final String LOG_TAG = PrinterStatusHelper.class.getName();
    public static final String RECEIVER_KEY = "com.zebra.printconnect.PrintService.RESULT_RECEIVER";

    public static ResultReceiver buildIPCSafeReceiver(ResultReceiver actualReceiver) {
        Parcel parcel = Parcel.obtain();
        actualReceiver.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        ResultReceiver receiverForSending = ResultReceiver.CREATOR.createFromParcel(parcel);
        parcel.recycle();
        return receiverForSending;
    }


    public static void updatePrinterSelectedFragment(Activity activity) {
        Intent printerStatusIntent = new Intent();
        printerStatusIntent.setComponent(new ComponentName("com.zebra.printconnect", "com.zebra.printconnect.print.GetPrinterStatusService"));
        printerStatusIntent.putExtra(RECEIVER_KEY, buildIPCSafeReceiver(new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, final Bundle resultData) {
                Log.i(LOG_TAG, "Result Code: " + resultCode);
                EventBus.getDefault().post(new StatusIntentCompleteEvent(resultCode, resultData));
            }
        }));
        activity.startService(printerStatusIntent);
    }

    public static void removeFragment(Activity activity, String fragmentTag) {
        FragmentManager fm = activity.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Fragment fragment = fm.findFragmentByTag(fragmentTag);
        if (fragment != null) {
            ft.remove(fragment);
            ft.commit();
        }
    }

    public static Fragment findFragmentByTag(Activity activity, String tagName) {
        FragmentManager fm = activity.getFragmentManager();
        if (fm != null) {
            return fm.findFragmentByTag(tagName);
        }
        return null;
    }

}
