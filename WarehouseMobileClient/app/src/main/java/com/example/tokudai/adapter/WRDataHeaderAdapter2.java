package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.WRDataHeader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WRDataHeaderAdapter2 extends RecyclerView.Adapter<WRDataHeaderAdapter2.ViewHolder> {
    Context context;
    ArrayList<WRDataHeader> list;
    WRDataHeader wrDataHeader;
    ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WRDataHeaderAdapter2(Context context, ArrayList<WRDataHeader> list, ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdataheader_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataHeader = list.get(i);
        try {
            viewHolder.tvWRDNumber.setText(wrDataHeader.getWrdNumber());
            Date wRDDate = originalFormat.parse(wrDataHeader.getWrdDate());
            viewHolder.tvWRDate.setText(simpleDateFormat.format(wRDDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvWRDNumber, tvWRDate;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvWRDNumber = itemView.findViewById(R.id.textWRDNumber);
            tvWRDate = itemView.findViewById(R.id.textWRDDate);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WRDataHeader> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataHeader wrDataHeader);
    }

}
