package com.example.tokudai.ui.user;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.tokudai.R;
import com.example.tokudai.databinding.FragmentUserBinding;
import com.example.tokudai.helper.Common;
import com.example.tokudai.model.User;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;
import com.example.tokudai.ui.setting.ServerConfigFragment;
import com.example.tokudai.ui.setting.SettingFragment;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserFragment extends Fragment {

    private UserViewModel userViewModel;
    private FragmentUserBinding binding;
    IWarehouseApi iWarehouseApi =null;
    SharedPreferences storage;
    public final static String ISLOGIN="islogin";
    public final static String USERID="user_id";
    private Button btnLogin=null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        userViewModel =
                new ViewModelProvider(this).get(UserViewModel.class);

        binding = FragmentUserBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        storage = getActivity().getSharedPreferences(ServerConfigFragment.PREFERENCES, Context.MODE_PRIVATE);

        final EditText edtUsername = binding.edtUsername;
        final EditText edtFullname = binding.edtFullname;
        final EditText edtPassword = binding.edtPassword;
        btnLogin=binding.btnSave;
        userViewModel.getmUsername().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtUsername.setText(s);
            }
        });
        userViewModel.getmFullname().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtFullname.setText(s);
            }
        });
        userViewModel.getmPassword().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtPassword.setText(s);
            }
        });

        String base = "http://%1$s:6381/api/";
        if (storage.getString(ServerConfigFragment.IPADRESS, null) != null) {
            Common.IP_SERVER =String.format(base,storage.getString(ServerConfigFragment.IPADRESS, null));
        }
        iWarehouseApi = RetrofitService.getService();

        if (storage.getString(ISLOGIN, null)!=null&&storage.getString(ISLOGIN, null).equalsIgnoreCase("logined")) {
            btnLogin.setText("Logout");
//            WarehouseFragment.isLogin=true;
        }
        else {
            btnLogin.setText("Login");
//            WarehouseFragment.isLogin=false;
        }


        return root;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user=new User();
                user.setUsername("admin");
                user.setPassword("123456");
                if(btnLogin.getText().toString().equalsIgnoreCase("Login")) {
                    Call<User> callback = iWarehouseApi.login(user);
                    callback.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            if (response.isSuccessful()) {
                                alertSuccess(getContext(), "Token: " + response.body().getToken());
                                Common.USER_ID = response.body().getUserID();
                                btnLogin.setText("Logout");
                                SharedPreferences.Editor editor = storage.edit();
                                editor.putString(ISLOGIN,"logined");
                                editor.putString(USERID,response.body().getUserID());
                                editor.commit();
                            } else {
                                Toast.makeText(getContext(), "Login fail", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Toast.makeText(getContext(), "Failure", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else {
                    SharedPreferences.Editor editor = storage.edit();
                    editor.putString(ISLOGIN,"notlogin");
                    editor.commit();
                    btnLogin.setText("Login");
                    Toast.makeText(getContext(),"Bạn đã đăng xuất",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}