package com.example.tokudai.ui.receipt_data;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.tokudai.R;
import com.example.tokudai.adapter.WRDataDetailAdapter2;
import com.example.tokudai.model.Inventory;
import com.example.tokudai.model.WRDataGeneral;
import com.example.tokudai.model.WRDataHeader;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptDataActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    private RecyclerView recyclerView = null;
    private EditText edtWRDNumber = null;
    private EditText edtPalletID = null;
    private EditText edtLotID = null;
    private EditText edtIDCode = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantityOrg = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoodsOrg = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearPalletID = null;
    private Button btnClearQuantity = null;
    private WRDataDetailAdapter2 adapter;
    ArrayList<WRDataGeneral> wrDataGenerals;
    ArrayList<WRDataGeneral> arrayListByGoodsID;
    ArrayList<WRDataHeader> wrDataHeaders;
    WRDataHeader wrDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Boolean isGoodsIDExist = false;
    Boolean isGoodsIdDuplicate = false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;

    ArrayList<Inventory> inventoryList=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Nhập hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();

        edtWRDNumber.requestFocus();

        wrDataHeaders = new ArrayList<>();
        wrDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
        inventoryList=new ArrayList<Inventory>();


        edtWRDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 11) {
                    Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString().trim());
                    callback.enqueue(new Callback<List<WRDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                            if (response.isSuccessful()) {
                                wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                                if (wrDataHeaders.size() > 0) {
                                    playSuccesSound();
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    wrDataHeader = wrDataHeaders.get(0);
                                    fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                                    hideSoftKeyBoard();
//                                    edtReferenceNumber.setText(wrDataHeader.getReferenceNumber());
                                    edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                                    edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                                    edtPalletID.requestFocus();
                                } else {
                                    playErrorSound();
                                    alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu");
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    fetchWRDataDetailData(edtWRDNumber.getText().toString());
                }
            }
        });

        edtPalletID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }

            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWRDNumber.setText("");
            }
        });

        btnClearPalletID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtPalletID.setText("");
            }
        });

        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void initComponent() {
        recyclerView = findViewById(R.id.recycler_view);
        edtWRDNumber = findViewById(R.id.edtWRDNumber);
        edtPalletID = findViewById(R.id.edtPalletID);
        edtLotID=findViewById(R.id.edtLotID);
        edtIDCode = findViewById(R.id.edtIDCode);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearPalletID = findViewById(R.id.btn_clear_palletid);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantityOrg = findViewById(R.id.edtTotalQuantityOrg);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoodsOrg = findViewById(R.id.edtTotalGoodsOrg);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
    }

    private void clear() {
        edtPalletID.setText("");
        edtQuantity.setText("");
    }

    private WRDataHeader fetchWRDataHeaderData(String wrdNumber) {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(wrdNumber);
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        wrDataHeader = wrDataHeaders.get(0);
                        hideSoftKeyBoard();
                        edtTotalQuantityOrg.setText(String.format("%.0f", wrDataHeader.getTotalQuantityOrg()));
                        edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                        edtPalletID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataHeader;
    }

    private ArrayList<WRDataGeneral> fetchWRDataGeneralData(String wrdNumber) {
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralById(wrdNumber);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wrDataGenerals = (ArrayList<WRDataGeneral>) response.body();
                    if (wrDataGenerals.size() > 0) {
                        edtTotalGoodsOrg.setText(String.format("%.0f", wrDataGenerals.get(0).getTotalGoodsOrg()));
                        totalGoods = 0.0;
                        getTotalGoods(wrDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataGenerals;
    }

    private void putWRDataHeader(String wRDNumber, WRDataHeader wrDataHeader) {
        Call<WRDataHeader> callback = iWarehouseApi.updateWRDataHeader(wRDNumber, wrDataHeader);
        callback.enqueue(new Callback<WRDataHeader>() {
            @Override
            public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataHeaderData(wRDNumber);
                }
            }

            @Override
            public void onFailure(Call<WRDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral(String wRDNumber, String palletID, int ordinal,String lotID, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, palletID, ordinal,lotID, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataGeneralData(wRDNumber);
                    getTotalQuantity(wrDataGenerals);
                    getTotalGoods(wrDataGenerals);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));

                    //Cập nhật tồn kho
//                    int year=Calendar.getInstance().get(Calendar.YEAR);
//                    int month= YearMonth.now().getMonthValue();
//                    fetchInventory(year,month,"1",wrDataGeneral.getPartNumber());

                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral2(String wRDNumber, String palletID, int ordinal,String lotID, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, palletID, ordinal,lotID, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataGeneralData(wRDNumber);
                    getTotalQuantity(wrDataGenerals);
                    getTotalGoods(wrDataGenerals);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));

                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Inventory> fetchInventory(int year, int month, String warhouseID, String partNumber) {

        Call<List<Inventory>> callback = iWarehouseApi.GetInventoryByPartNumber(year,month,warhouseID,partNumber);
        callback.enqueue(new Callback<List<Inventory>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<Inventory>> call, Response<List<Inventory>> response) {
                if (response.isSuccessful()) {
                    inventoryList=(ArrayList<Inventory>)response.body();
                    //Cập nhật tồn kho
                    int year= Calendar.getInstance().get(Calendar.YEAR);
                    int month= YearMonth.now().getMonthValue();
                    if(inventoryList.size()>0) {
                        Inventory inventory = inventoryList.get(0);
                        String idCodeString = inventory.getIdCode();
                        inventory.setIdCode(idCodeString.concat(edtIDCode.getText().toString()+","));

                        putInventory(year,month,"1",partNumber,inventory);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Inventory>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return inventoryList;
    }

    private void putInventory(int year, int month, String warhouseID, String partNumber, Inventory inventory) {
        Call<Inventory> callback = iWarehouseApi.updateInventory(year,month,warhouseID,partNumber,inventory);
        callback.enqueue(new Callback<Inventory>() {
            @Override
            public void onResponse(Call<Inventory> call, Response<Inventory> response) {
                if (response.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();

                } else {
                    alertError(getApplicationContext(), "Cập nhật tồn kho thất bại");
                }

            }

            @Override
            public void onFailure(Call<Inventory> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTotalQuantity(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.size() > 0 && wrDataGenerals.get(i).getQuantity() > 0) {
                totalQuantity += wrDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                totalGoods += 1;
            }
        }
    }

    private Boolean checkDuplicate(ArrayList<WRDataGeneral> wrDataGenerals, String palletID) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getPalletID().equalsIgnoreCase(palletID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistLabel(String palletID, ArrayList<WRDataGeneral> wrDataGenerals) {
        Boolean flag = false;
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (palletID.equalsIgnoreCase(wrDataGenerals.get(i).getPalletID())) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private Boolean checkPalletIDAndIDCodeScaned(String palletID, String idCode, WRDataGeneral wrDataGeneral) {
        Boolean flag = false;
        if(!wrDataGeneral.getIdCode().equalsIgnoreCase("")||wrDataGeneral.getIdCode()!=null) {
            ArrayList<String> listIDCode = new ArrayList<String>(Arrays.asList(wrDataGeneral.getIdCode().split(",")));
            for (int j = 0; j < listIDCode.size(); j++) {
                if (palletID.equalsIgnoreCase(wrDataGeneral.getPalletID()) && idCode.equalsIgnoreCase(listIDCode.get(j).toString().toUpperCase())
                        && wrDataGeneral.getQuantity().doubleValue() > 0) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    private ArrayList<WRDataGeneral> getNotYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wrDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WRDataGeneral> getEnoughtYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() < wrDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wrDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WRDataGeneral> getEnoughtList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity().doubleValue() == wrDataGenerals.get(i).getQuantityOrg().doubleValue()) {
                enoughtList.add(wrDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WRDataGeneral> getOvertList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > wrDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wrDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void playErrorSound(){
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("error.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void playSuccesSound(){
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("ok.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(ArrayList<WRDataGeneral> wrDataGenerals) {
        adapter = new WRDataDetailAdapter2(getApplicationContext(), wrDataGenerals, new WRDataDetailAdapter2.ItemClickListener() {
            @Override
            public void onClick(WRDataGeneral wrDataGeneral) {
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_wrdatadetail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_all) {
            ArrayList<WRDataGeneral> allList = wrDataGenerals;
            setAdapter(allList);
            return true;
        }
        if (id == R.id.action_not_yet) {
            ArrayList<WRDataGeneral> notYetList = getNotYetList(wrDataGenerals);
            setAdapter(notYetList);
            return true;
        }
        if (id == R.id.action_enought_yet) {
            ArrayList<WRDataGeneral> enoughtYetLish = getEnoughtYetList(wrDataGenerals);
            setAdapter(enoughtYetLish);
            return true;
        }
        if (id == R.id.action_enought) {
            ArrayList<WRDataGeneral> enoughtList = getEnoughtList(wrDataGenerals);
            setAdapter(enoughtList);
            return true;
        }
        if (id == R.id.action_over) {
            ArrayList<WRDataGeneral> overList = getOvertList(wrDataGenerals);
            setAdapter(overList);
            return true;
        }
        if (id == R.id.action_save) {
            process();
            return true;
        }
        if (id == R.id.action_approve) {
            if (wrDataHeader != null) {
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setHandlingStatusID("1");
                wrDataHeaderUpdate.setHandlingStatusName("Duyệt mức 1");
                putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 1 nhập kho thành công");
            }
            return true;
        }
        if (id == R.id.action_clear) {
            if (wrDataGenerals.size() > 0) {
                clearList();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled =true;
//                config.decoderParams.dataMatrix.enabled=true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtWRDNumber.hasFocus()) {
                        edtWRDNumber.setText(result);
                    } else {
                        String palletID = "";
                        String lotID="";
                        String quantity = "";
                        String idCode = "";
                        int index1 = result.indexOf("|");
                        int index2 = result.indexOf("|", index1 + 1);
                        int index3 = result.indexOf("|", index2 + 1);
                        int index4 = result.indexOf("|", index3 + 1);
                        int index5 = result.indexOf("|", index4 + 1);
                        int index6 = result.indexOf("|", index5 + 1);
                        int index7 = result.indexOf("|", index6 + 1);
                        int index8 = result.indexOf("|", index7 + 1);
                        int index9 = result.indexOf("|", index8 + 1);
                        int index10 = result.indexOf("|", index9 + 1);
                        int index11 = result.indexOf("|", index10 + 1);
                        int index12 = result.indexOf("|", index11 + 1);
                        int index13 = result.lastIndexOf("|");

                        if(result.length()==5){
//                            processAll();
//                            alertSuccess(ReceiptDataActivity.this,"Hoàn thành nhập đơn hàng có mã Pallet ID là: "+result);
                        }
                        else {
                            if (index13 > 0) {
                                idCode = result.substring(index2 + 1, index3);
                                palletID = idCode.substring(0, 5);
                                lotID=result.substring(index8+1,index9);
                                quantity = result.substring(index3 + 1, index4);
                                if (palletID.length() ==5) {
                                    edtPalletID.setText(palletID);
                                    edtLotID.setText(lotID);
                                    edtQuantity.setText(quantity);
                                    edtIDCode.setText(idCode);

                                    process();
                                }
                            } else {
                                playErrorSound();
                                Toast.makeText(ReceiptDataActivity.this, "Định dạng tem nhãn không đúng", Toast.LENGTH_SHORT);
                            }
                        }
                    }
                }
            }
        });
    }

//    private void processAll(){
//        for (int i = 0; i < wrDataGenerals.size(); i++) {
//            if (wrDataGenerals.get(i).getQuantity() == 0) {
//                WRDataGeneral wrDataGeneralUpdate = wrDataGenerals.get(i);
//                wrDataGeneralUpdate.setQuantity(wrDataGeneralUpdate.getQuantityOrg());
//                wrDataGeneralUpdate.setTotalGoods(wrDataGeneralUpdate.getTotalGoods()+totalGoods-wrDataGenerals.size());
//                wrDataGeneralUpdate.setTotalQuantity(wrDataGeneralUpdate.getTotalQuantity() -wrDataGeneralUpdate.getTotalQuantityOrg()+ totalQuantity);
//                putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getPalletID(), wrDataGeneralUpdate.getOrdinal(),wrDataGeneralUpdate);
//            }
//        }
//        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
//        wrDataHeaderUpdate.setTotalQuantity(wrDataHeaderUpdate.getTotalQuantityOrg());
//        putWRDataHeader(edtWRDNumber.getText().toString(),wrDataHeaderUpdate);
//    }

    private void process() {
        if (TextUtils.isEmpty(edtPalletID.getText().toString())) {
            alertError(getApplicationContext(), "Nhập Pallet ID");
            edtPalletID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
            alertError(getApplicationContext(), "Nhập số lượng");
            edtQuantity.requestFocus();
            return;
        }

        if (checkExistLabel(edtPalletID.getText().toString(), wrDataGenerals) == false) {
            playErrorSound();
            alertError(getApplicationContext(), "Thông tin tem nhãn không tồn tại");
            return;
        }

        totalQuantity = 0.0;
        totalGoods = 0.0;
        getTotalGoods(wrDataGenerals);
        getTotalQuantity(wrDataGenerals);
        Call<List<WRDataGeneral>> callback = iWarehouseApi.GetByParams3(edtWRDNumber.getText().toString(), edtPalletID.getText().toString(),edtLotID.getText().toString());
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    arrayListByGoodsID = (ArrayList<WRDataGeneral>) response.body();
                    if (arrayListByGoodsID.size() > 0) {
                        WRDataGeneral wrDataGeneralUpdate = arrayListByGoodsID.get(0);

                        if (checkPalletIDAndIDCodeScaned(edtPalletID.getText().toString(), edtIDCode.getText().toString(), wrDataGeneralUpdate) == true) {
                            playErrorSound();
                            alertError(ReceiptDataActivity.this, "Tem nhãn đã scan!!!");
                            return;
                        }

                        playSuccesSound();
                        checkDuplicate(arrayListByGoodsID, edtPalletID.getText().toString());
                        if (isGoodsIdDuplicate == false) {
                            wrDataGeneralUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                        } else {
                            wrDataGeneralUpdate.setQuantity(wrDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                            isGoodsIdDuplicate = false;
                        }
                        wrDataGeneralUpdate.setWrdNumber(edtWRDNumber.getText().toString());
                        wrDataGeneralUpdate.setIdCode(wrDataGeneralUpdate.getIdCode() + edtIDCode.getText().toString().toUpperCase() + ",");
                        wrDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());
                        wrDataGeneralUpdate.setTotalGoods(totalGoods + 1);
                        wrDataGeneralUpdate.setInputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                        wrDataGeneralUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getPalletID(), wrDataGeneralUpdate.getOrdinal(),wrDataGeneralUpdate.getLotID(), wrDataGeneralUpdate);

                        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                        wrDataHeaderUpdate.setTotalQuantity(totalQuantity + Double.parseDouble(edtQuantity.getText().toString()));
                        putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearList() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ReceiptDataActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_clear_list, viewGroup, false);
        builder.setView(dialogView);
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < wrDataGenerals.size(); i++) {
                    WRDataGeneral wrDataGeneralUpdate = wrDataGenerals.get(i);
                    wrDataGeneralUpdate.setQuantity(0.0);
                    wrDataGeneralUpdate.setTotalGoods(0.0);
                    wrDataGeneralUpdate.setTotalQuantity(0.0);
                    wrDataGeneralUpdate.setIdCode("");
                    wrDataGeneralUpdate.setInputDate(null);
                    putWRDataGeneral2(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getPalletID(), wrDataGeneralUpdate.getOrdinal(),wrDataGeneralUpdate.getLotID(), wrDataGeneralUpdate);
                }
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setTotalQuantity(0.0);
                putWRDataHeader(edtWRDNumber.getText().toString().toUpperCase(), wrDataHeaderUpdate);
                alertDialog.cancel();
                Toast.makeText(ReceiptDataActivity.this, "Đã xóa dữ liệu quét" + edtWRDNumber.getText().toString().toUpperCase(), Toast.LENGTH_SHORT).show();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}