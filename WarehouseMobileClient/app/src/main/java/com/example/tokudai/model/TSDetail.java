package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TSDetail {
    @SerializedName("tsNumber")
    @Expose
    private String tsNumber;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("partName")
    @Expose
    private String partName;
    @SerializedName("tallyUnitID")
    @Expose
    private String tallyUnitID;
    @SerializedName("unitRate")
    @Expose
    private Double unitRate;
    @SerializedName("stockUnitID")
    @Expose
    private String stockUnitID;
    @SerializedName("packingVolume")
    @Expose
    private Double packingVolume;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("quantityByPack")
    @Expose
    private Double quantityByPack;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("status")
    @Expose
    private String status;

    public TSDetail() {
    }

    public TSDetail(String tsNumber, Integer ordinal, String partNumber, String partName, String tallyUnitID, Double unitRate, String stockUnitID, Double packingVolume, Double quantity, Double quantityByPack, String note, String status) {
        this.tsNumber = tsNumber;
        this.ordinal = ordinal;
        this.partNumber = partNumber;
        this.partName = partName;
        this.tallyUnitID = tallyUnitID;
        this.unitRate = unitRate;
        this.stockUnitID = stockUnitID;
        this.packingVolume = packingVolume;
        this.quantity = quantity;
        this.quantityByPack = quantityByPack;
        this.note = note;
        this.status = status;
    }

    public String getTsNumber() {
        return tsNumber;
    }

    public void setTsNumber(String tsNumber) {
        this.tsNumber = tsNumber;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getTallyUnitID() {
        return tallyUnitID;
    }

    public void setTallyUnitID(String tallyUnitID) {
        this.tallyUnitID = tallyUnitID;
    }

    public Double getUnitRate() {
        return unitRate;
    }

    public void setUnitRate(Double unitRate) {
        this.unitRate = unitRate;
    }

    public String getStockUnitID() {
        return stockUnitID;
    }

    public void setStockUnitID(String stockUnitID) {
        this.stockUnitID = stockUnitID;
    }

    public Double getPackingVolume() {
        return packingVolume;
    }

    public void setPackingVolume(Double packingVolume) {
        this.packingVolume = packingVolume;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getQuantityByPack() {
        return quantityByPack;
    }

    public void setQuantityByPack(Double quantityByPack) {
        this.quantityByPack = quantityByPack;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
