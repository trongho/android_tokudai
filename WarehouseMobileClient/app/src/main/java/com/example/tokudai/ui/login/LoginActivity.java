package com.example.tokudai.ui.login;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.widget.Button;

import com.example.tokudai.databinding.ActivityLoginBinding;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;
    private LoginViewModel loginViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);

        final TextInputEditText edtUsername = binding.edtUsername;
        final TextInputEditText edtPassword = binding.edtPassword;
        final Button btnLogin = binding.btnLogin;

        loginViewModel.getmUsername().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtUsername.setText(s);
            }
        });
        loginViewModel.getmPassword().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                edtPassword.setText(s);
            }
        });
//        loginViewModel.login().observe(this,user -> {
//            if(user==null){
//                Toast.makeText(this,"Login fail",Toast.LENGTH_SHORT).show();
//            }
//            else {
//                Toast.makeText(this,"Login sucess",Toast.LENGTH_SHORT).show();
//            }
//        });


    }
}