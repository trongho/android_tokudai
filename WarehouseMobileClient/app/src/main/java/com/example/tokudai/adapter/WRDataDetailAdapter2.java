package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.WRDataGeneral;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WRDataDetailAdapter2 extends RecyclerView.Adapter<WRDataDetailAdapter2.ViewHolder> {
    Context context;
    ArrayList<WRDataGeneral> list;
    WRDataGeneral wrDataGeneral;
    WRDataDetailAdapter2.ItemClickListener listener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public WRDataDetailAdapter2(Context context, ArrayList<WRDataGeneral> list, WRDataDetailAdapter2.ItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WRDataDetailAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WRDataDetailAdapter2.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WRDataDetailAdapter2.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataGeneral = list.get(i);
        viewHolder.tvPalletID.setText(wrDataGeneral.getPalletID());
        viewHolder.tvPartNumber.setText(wrDataGeneral.getPartNumber());
        viewHolder.tvLotID.setText(wrDataGeneral.getLotID());
        viewHolder.tvQuantity.setText(String.format("%.0f",wrDataGeneral.getQuantity()));
        viewHolder.tvQuantityOrg.setText(String.format("%.0f",wrDataGeneral.getQuantityOrg()));
        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });


        if(wrDataGeneral.getQuantity().doubleValue()==0){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        else if(wrDataGeneral.getQuantity().doubleValue()>0&&wrDataGeneral.getQuantity().doubleValue()<wrDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f75c78"));
        }
        else if(wrDataGeneral.getQuantity().doubleValue()==wrDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#1dde3d"));
        } else if(wrDataGeneral.getQuantity().doubleValue()>wrDataGeneral.getQuantityOrg().doubleValue()){
            viewHolder.cardView.setBackgroundColor(Color.parseColor("#f7fc60"));
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPalletID,tvPartNumber,tvLotID, tvQuantity,tvQuantityOrg,tvLineNo;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPalletID = itemView.findViewById(R.id.tvPalletID);
            tvPartNumber = itemView.findViewById(R.id.tvPartNumber);
            tvLotID=itemView.findViewById(R.id.tvLotID);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvQuantityOrg = itemView.findViewById(R.id.tvQuantityOrg);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
        }
    }

    public void updateList(List<WRDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataGeneral wrDataGeneral);
    }

}
