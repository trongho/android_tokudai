package com.example.tokudai.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.tokudai.ui.setting.PrintConfigFragment;
import com.example.tokudai.ui.setting.ServerConfigFragment;
import com.example.tokudai.ui.setting.SettingFragment;


public class SettingViewPager2Adapter extends FragmentStateAdapter {
    public SettingViewPager2Adapter(Fragment fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int pos) {

        switch (pos) {
            case 0: {
                return ServerConfigFragment.newInstance("Server Config");
            }
            case 1: {

                return PrintConfigFragment.newInstance("Print Config");
            }
            default:
                return ServerConfigFragment.newInstance("Server Config, Default");
        }
    }

    @Override
    public int getItemCount() {
        return SettingFragment.NUM_PAGES;
    }
}
