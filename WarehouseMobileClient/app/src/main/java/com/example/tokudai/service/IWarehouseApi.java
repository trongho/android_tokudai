package com.example.tokudai.service;

import com.example.tokudai.model.Goods;
import com.example.tokudai.model.Inventory;
import com.example.tokudai.model.InventoryDetail;
import com.example.tokudai.model.PalletDetail;
import com.example.tokudai.model.PalletHeader;
import com.example.tokudai.model.TSDetail;
import com.example.tokudai.model.TSHeader;
import com.example.tokudai.model.TallyData;
import com.example.tokudai.model.User;
import com.example.tokudai.model.WIDataDetail;
import com.example.tokudai.model.WIDataGeneral;
import com.example.tokudai.model.WIDataHeader;
import com.example.tokudai.model.WRDataGeneral;
import com.example.tokudai.model.WRDataHeader;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IWarehouseApi {
    @GET("wrdataheader")
    Call<List<WRDataHeader>> getWRDataHeaders();
    @GET("wrdataheader/GetByID/{id}")
    Call<List<WRDataHeader>> getWRDataHeader(@Path("id") String id);
    @PUT("wrdataheader/Put/{wRDNumber}")
    Call<WRDataHeader> updateWRDataHeader(@Path("wRDNumber") String wRDNumber,@Body WRDataHeader body);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("wrdataheader/Post")
    Call<WRDataHeader> creatWRDataHeader(@Body WRDataHeader body);

    @GET("wrdataheader/GetLastID/{str}")
    Call<String> GetLastID(@Path("str") String str);
    @DELETE("wrdataheader/Delete/{wrdNumber}")
    Call<Boolean> DeleteWRDataHeader(@Path("wrdNumber") String wrdNumber);

    @GET("wrdatageneral/getByID/{id}")
    Call<List<WRDataGeneral>> getWRDataGeneralById(@Path("id") String id);
    @PUT("wrdatageneral/Put/{wRDNumber}/{palletID}/{ordinal}/{lotID}")
    Call<WRDataGeneral> updateWRDataGeneral(@Path("wRDNumber") String wRDNumber, @Path("palletID") String palletID,@Path("ordinal") int ordinal, @Path("lotID") String lotID, @Body WRDataGeneral body);
    @GET("wrdatageneral/GetByWRDNumberAndPalletID/{WRDNumber}/{palletID}")
    Call<List<WRDataGeneral>> getWRDataGeneral(@Path("WRDNumber") String wRDNumber,@Path("palletID") String palletID);
    @GET("wrdatageneral/GetByParams3/{WRDNumber}/{palletID}/{lotID}")
    Call<List<WRDataGeneral>> GetByParams3(@Path("WRDNumber") String wRDNumber,@Path("palletID") String palletID,@Path("lotID") String lotID);
    @GET("wrdatageneral/GetByPartNumberAndLotID/{WRDNumber}/{partNumber}/{lotID}")
    @POST("wrdatageneral/Post")
    Call<WRDataGeneral> creatWRDataGeneral(@Body WRDataGeneral body);
    @DELETE("wrdatageneral/Delete3/{wrdNumber}/{palletID}/{lotID}")
    Call<Boolean> DeleteWRDataGeneral(@Path("wrdNumber") String wrdNumber,@Path("palletID") String palletID,@Path("lotID") String lotID);
    @DELETE("wrdatageneral/Delete2/{wrdNumber}/{palletID}")
    Call<Boolean> DeleteWRDataGeneral(@Path("wrdNumber") String wrdNumber,@Path("palletID") String palletID);
    @DELETE("wrdatageneral/Delete/{wrdNumber}")
    Call<Boolean> DeleteWRDataGeneral(@Path("wrdNumber") String wrdNumber);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("palletheader/Post")
    Call<PalletHeader> createPalletHeader(@Body PalletHeader body);
    @GET("palletheader/GetLastID/{str}")
    Call<String> GetLastIDPalletHeader(@Path("str") String str);
    @PUT("palletheader/Put/{palletID}/{ordinal}")
    Call<PalletHeader> updatePalletHeader(@Path("palletID") String palletID,@Path("ordinal") int ordinal,@Body PalletHeader body);
    @GET("palletheader/GetByID/{palletID}")
    Call<List<PalletHeader>> getPalletHeaderByID(@Path("palletID") String palletID);
    @GET("palletheader")
    Call<List<PalletHeader>> getAllPalletHeader();
    @GET("palletheader/GetByCreatedDate/{fromDate}/{toDate}")
    Call<List<PalletHeader>> getPalletHeaderByCreatedDate(@Path("fromDate") String fromDate,@Path("toDate") String toDate);
    @DELETE("palletheader/Delete/{palletID}")
    Call<Boolean> DeletePalletHeader(@Path("palletID") String palletID);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("palletdetail/Post")
    Call<PalletDetail> createPalletDetail(@Body PalletDetail body);
    @DELETE("palletdetail/DeleteByParams/{palletID}/{idCode}")
    Call<Boolean> DeletePalletDetail(@Path("palletID") String palletID,@Path("idCode") String idCode);
    @DELETE("palletdetail/Delete/{palletID}")
    Call<Boolean> DeletePalletDetail(@Path("palletID") String palletID);
    @GET("palletdetail/GetByParams/{palletID}/{idCode}")
    Call<List<PalletDetail>> getPalletDetailByParams(@Path("palletID") String palletID,@Path("idCode") String idCode);
    @PUT("palletdetail/Put/{palletID}/{ordinal}/{idCode}")
    Call<PalletDetail> updatePalletDetail(@Path("palletID") String palletID,@Path("ordinal") int ordinal, @Path("idCode") String idCode, @Body PalletDetail body);
    @GET("palletdetail/getByID/{palletID}")
    Call<List<PalletDetail>> getPalletDetailById(@Path("palletID") String palletID);
    @GET("palletdetail/getByIDCode/{idCode}")
    Call<List<PalletDetail>> getPalletDetailByIDCode(@Path("idCode") String idCode);


    @GET("goods/GetByParams/{partNumber}/{palletID}")
    Call<List<Goods>> getGoodsByParams2(@Path("partNumber") String partNumber, @Path("palletID") String palletID);

    @GET("widataheader")
    Call<List<WIDataHeader>> getWIDataHeaders();
    @GET("widataheader/GetByID/{id}")
    Call<List<WIDataHeader>> getWIDataHeader(@Path("id") String id);
    @PUT("widataheader/Put/{wIDNumber}")
    Call<WIDataHeader> updateWIDataHeader(@Path("wIDNumber") String wIDNumber,@Body WIDataHeader body);


    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("widatadetail/Post")
    Call<WIDataDetail> createWIDataDetail(@Body WIDataDetail body);
    @GET("widatadetail/GetByID/{id}")
    Call<List<WIDataDetail>> getWIDataDetailById(@Path("id") String id);
    @PUT("widatadetail/Put/{wIDNumber}/{palletID}/{partNumber}/{ordinal}/{lotID}/{idCode}")
    Call<WIDataDetail> updateWIDataDetail(@Path("wIDNumber") String wIDNumber,@Path("palletID") String palletID ,@Path("partNumber") String partNumber, @Path("ordinal") int ordinal,@Path("lotID") String lotID,@Path("idCode") String idCode,@Body WIDataDetail body);
    @GET("widatadetail/GetByParams3/{WIDNumber}/{palletID}/{partNumber}")
    Call<List<WIDataDetail>> getWIDataDetailByParams3(@Path("WIDNumber") String wIDNumber,@Path("palletID") String palletID,@Path("partNumber") String partNumber);
    @GET("widatadetail/GetByParams5/{WIDNumber}/{palletID}/{partNumber}/{lotID}/{idCode}")
    Call<List<WIDataDetail>> getWIDataDetailByParams5(@Path("WIDNumber") String wIDNumber,@Path("palletID") String palletID,@Path("partNumber") String partNumber,@Path("lotID") String lotID,@Path("idCode") String idCode);
    @DELETE("widatadetail/Delete/{widNumber}")
    Call<Boolean> deleteWIDataDetail(@Path("widNumber") String widNumber);
    @DELETE("widatadetail/DeleteByParams6/{widNumber}/{palletID}/{partNumber}/{ordinal}/{lotID}/{idCode}")
    Call<Boolean> deleteWIDataDetailByParams4(@Path("widNumber") String widNumber,@Path("palletId") String palletID,@Path("partNumber") String partNumber,@Path("ordinal") int ordinal,@Path("lotID") String lotId,@Path("idCode") String idCode);


    @GET("widatageneral/getByID/{id}")
    Call<List<WIDataGeneral>> getWIDataGeneralById(@Path("id") String id);
//    @PUT("widatageneral/Put/{wIDNumber}/{palletID}/{ordinal}/{lotID}")
//    Call<WIDataGeneral> updateWIDataGeneral(@Path("wIDNumber") String wIDNumber, @Path("palletID") String palletID, @Path("ordinal") int ordinal,@Path("lotID") String lotID, @Body WIDataGeneral body);
    @PUT("widatageneral/Put/{wIDNumber}/{palletID}/{partNumber}/{ordinal}")
    Call<WIDataGeneral> updateWIDataGeneral(@Path("wIDNumber") String wIDNumber, @Path("palletID") String palletID,@Path("partNumber") String partNumber, @Path("ordinal") int ordinal, @Body WIDataGeneral body);
    @GET("widatageneral/GetByParams3/{WIDNumber}/{palletID}/{partNumber}")
    Call<List<WIDataGeneral>> GetWIDataGeneralByParams3(@Path("WIDNumber") String wIDNumber,@Path("palletID") String palletID,@Path("partNumber") String partNumber);
//    @GET("widatageneral/GetByPartNumberAndLotID/{WIDNumber}/{partNumber}/{lotID}")
//    Call<List<WIDataGeneral>> GetByPartNumberAndLotID(@Path("WIDNumber") String wIDNumber,@Path("partNumber") String partNumber,@Path("lotID") String lotID);

    @GET("inventory/GetByMultiPartNumber/{year}/{month}/{warhouseID}/{partNumber}")
    Call<List<Inventory>> GetInventoryByPartNumber(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID,@Path("partNumber") String partNumber);
    @PUT("inventory/Put/{year}/{month}/{warhouseID}/{partNumber}")
    Call<Inventory> updateInventory(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID,@Path("partNumber") String partNumber, @Body Inventory body);

    @GET("inventorydetail/GetByWarehouseAndPallet/{year}/{month}/{warehouseID}/{palletID}")
    Call<List<InventoryDetail>> GetInventoryDetailByWarehouseAndPallet(@Path("year") int year,@Path("month") int month,@Path("warehouseID") String warehouseID,@Path("palletID") String palletID);
    @GET("inventorydetail/CheckExist/{warhouseID}/{partNumber}/{palletID}/{idCode}")
    Call<List<InventoryDetail>> CheckExist(@Path("warhouseID") String warhouseID, @Path("partNumber") String partNumber,@Path("palletID") String palletID,@Path("idCode") String idCode);
    @GET("inventorydetail/GetByParams3/{Year}/{Month}/{WarehouseID}")
    Call<List<InventoryDetail>> GetByParams3(@Path("Year") int year, @Path("Month") int month, @Path("WarehouseID") String warehouseID);
    @GET("inventorydetail/GetByParams4/{year}/{month}/{warhouseID}/{partNumber}")
    Call<List<InventoryDetail>> GetByParams4(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID, @Path("partNumber") String partNumber);
    @GET("inventorydetail/GetByParams5/{year}/{month}/{warhouseID}/{partNumber}/{palletID}")
    Call<List<InventoryDetail>> GetByParams5(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID, @Path("partNumber") String partNumber,@Path("palletID") String palletID);
    @GET("inventorydetail/GetByParams6/{year}/{month}/{warhouseID}/{partNumber}/{palletID}/{idCode}")
    Call<List<InventoryDetail>> GetByParams6(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID, @Path("partNumber") String partNumber,@Path("palletID") String palletID,@Path("idCode") String idCode);
    @GET("inventorydetail/GetByParams62/{year}/{month}/{warhouseID}/{partNumber}/{palletID}/{lotID}")
    Call<List<InventoryDetail>> GetByParams62(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID, @Path("partNumber") String partNumber,@Path("palletID") String palletID,@Path("lotID") String lotID);
    @PUT("inventorydetail/Put/{year}/{month}/{warhouseID}/{partNumber}/{palletID}")
    Call<InventoryDetail> updateInventoryDetail(@Path("year") int year, @Path("month") int month, @Path("warhouseID") String warhouseID,@Path("partNumber") String partNumber,@Path("palletID") String palletID, @Body InventoryDetail body);

    @GET("inventorydetail/GetByIDCode/{idCode}")
    Call<List<InventoryDetail>> GetInventoryDetailByIDCode(@Path("IDCode") String idCode);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("user/login")
    Call<User> login(@Body User body);


    @GET("tallydata/GetByID/{id}")
    Call<List<TallyData>> getTallyDataById(@Path("id") String id);
    @GET("tallydata/lastID")
    Call<String> getLastTSNumber();
    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tallydata/Post")
    Call<TallyData> creatTallyData(@Body TallyData body);
    @PUT("tallydata/Put/{tsNumber}/{GoodsID}/{No}")
    Call<TallyData> updateTallyData(@Path("tsNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("No") int no,@Body TallyData body);
    @DELETE("tallydata/Delete/{tsNumber}")
    Call<Boolean> deleteTallyData(@Path("tsNumber") String tsNumber);
    @DELETE("tallydata/DeleteDetail/{tsNumber}/{goodsID}/{No}")
    Call<Boolean> deleteTallyDataDetail(@Path("tsNumber") String tsNumber,@Path("goodsID") String goodsID,@Path("No") int No);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tsheader/Post")
    Call<TSHeader> creatTSHeader(@Body TSHeader body);
    @PUT("tsheader/Put/{tsNumber}")
    Call<TSHeader> updateTSHeader(@Path("tsNumber") String tsNumber,@Body TSHeader body);
    @GET("tsheader/GetByID/{id}")
    Call<List<TSHeader>> getTSHeaderById(@Path("id") String id);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("tsdetail/Post")
    Call<TSDetail> creatTSDetail(@Body TSDetail body);
    @PUT("tsdetail/Put/{tsNumber}/{GoodsID}/{Ordinal}")
    Call<TSDetail> updateTSDetail(@Path("tsNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("Ordinal") int ordinal,@Body TSDetail body);
    @GET("tsdetail/GetByMultiID/{TSNumber}/{GoodsID}/{Ordinal}")
    Call<List<TSDetail>> getTSDetailByMultiId(@Path("TSNumber") String tsNumber,@Path("GoodsID") String goodsID,@Path("Ordinal") int ordinal);
    @GET("tsdetail/GetByID/{id}")
    Call<List<TSDetail>> getTSDetailById(@Path("id") String id);
    @GET("tsdetail/GetByID2/{tsNumber}/{partNumber}")
    Call<List<TSDetail>> getTSDetailById2(@Path("tsNumber") String tsNumber,@Path("partNumber") String partNumber);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @POST("printer/Post")
    Call<String> postZPLString(@Body String zplString);
}

