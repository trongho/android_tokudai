package com.example.tokudai.helper;

import com.example.tokudai.model.InventoryDetail;

import java.util.ArrayList;

public interface ApiCallback {
    void onSuccess(ArrayList<InventoryDetail> result);
}
