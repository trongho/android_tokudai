package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PalletHeader {
    @SerializedName("palletID")
    @Expose
    private String palletID;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("totalIDCode")
    @Expose
    private Double totalIDCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("handlingStatusID")
    @Expose
    private String handlingStatusID;
    @SerializedName("handlingStatusName")
    @Expose
    private String handlingStatusName;
    @SerializedName("createdUserID")
    @Expose
    private String createdUserID;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("updatedUserID")
    @Expose
    private String updatedUserID;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    public PalletHeader() {
    }

    public PalletHeader(String palletID, Integer ordinal, Double totalIDCode, String status, String handlingStatusID, String handlingStatusName, String createdUserID, String createdDate, String updatedUserID, String updatedDate) {
        this.palletID = palletID;
        this.ordinal = ordinal;
        this.totalIDCode = totalIDCode;
        this.status = status;
        this.handlingStatusID = handlingStatusID;
        this.handlingStatusName = handlingStatusName;
        this.createdUserID = createdUserID;
        this.createdDate = createdDate;
        this.updatedUserID = updatedUserID;
        this.updatedDate = updatedDate;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public Double getTotalIDCode() {
        return totalIDCode;
    }

    public void setTotalIDCode(Double totalIDCode) {
        this.totalIDCode = totalIDCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHandlingStatusID() {
        return handlingStatusID;
    }

    public void setHandlingStatusID(String handlingStatusID) {
        this.handlingStatusID = handlingStatusID;
    }

    public String getHandlingStatusName() {
        return handlingStatusName;
    }

    public void setHandlingStatusName(String handlingStatusName) {
        this.handlingStatusName = handlingStatusName;
    }

    public String getCreatedUserID() {
        return createdUserID;
    }

    public void setCreatedUserID(String createdUserID) {
        this.createdUserID = createdUserID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedUserID() {
        return updatedUserID;
    }

    public void setUpdatedUserID(String updatedUserID) {
        this.updatedUserID = updatedUserID;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
