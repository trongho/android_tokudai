package com.example.tokudai.ui.pallet_manager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokudai.R;
import com.example.tokudai.adapter.CreatePalletIDAdapter;
import com.example.tokudai.helper.Common;
import com.example.tokudai.helper.DemoSleeper;
import com.example.tokudai.helper.MessageHelper;
import com.example.tokudai.helper.PrintHelper;
import com.example.tokudai.helper.PrintSettingHelper;
import com.example.tokudai.helper.UIHelper;
import com.example.tokudai.model.Goods;
import com.example.tokudai.model.InventoryDetail;
import com.example.tokudai.model.PalletDetail;
import com.example.tokudai.model.PalletHeader;
import com.example.tokudai.model.WIDataGeneral;
import com.example.tokudai.model.WIDataHeader;
import com.example.tokudai.model.WRDataGeneral;
import com.example.tokudai.model.WRDataHeader;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;
import com.example.tokudai.ui.issue_data.IssueDataActivity;
import com.example.tokudai.ui.receipt_data_scan.ReceiptDataScanActivity;
import com.example.tokudai.ui.setting.ServerConfigFragment;
import com.example.tokudai.ui.setting.SettingFragment;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreatePalletActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    public TextView tvMsg = null;
    private RecyclerView recyclerView = null;
    private AutoCompleteTextView edtPalletID = null;
    private EditText edtLotID = null;
    private EditText edtIDCode = null;
    private EditText edtPartNumber = null;
    private EditText edtPartName = null;
    private EditText edtUnit = null;
    private EditText edtMFGDate = null;
    private EditText edtEXPDate = null;
    private EditText edtQuantity = null;
    private EditText edtTotalIDCode = null;
    private Button btnLockPalletID=null;
    private TextView tvStatus=null;

    private Boolean isPalletIDLock=false;

    private CreatePalletIDAdapter adapter = null;
    ArrayList<PalletDetail> palletDetails = null;
    ArrayList<PalletHeader> palletHeaders = null;
    PalletHeader palletHeader = null;
    ArrayList<Goods> goodsList = null;

    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    SharedPreferences storage = null;
    String lastID = "";

    //print
    Connection printerConnection = null;
    private UIHelper helper = new UIHelper(this);

    TextToSpeech textToSpeech;
    public static final String PALLET_ID = "pallet id";
    public static final int PALLET_ID_RESULT = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pallet);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Quét tạo mã pallet");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();

        edtPalletID.requestFocus();
        autocompletePalletID();

        if(getIntent().getStringExtra(PALLET_ID)!=""){
            edtPalletID.setText(getDataFromIntent());
            Call<List<PalletHeader>> callback = iWarehouseApi.getPalletHeaderByID(edtPalletID.getText().toString().trim());
            callback.enqueue(new Callback<List<PalletHeader>>() {
                @Override
                public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                    if (response.isSuccessful()) {
                        palletHeaders = (ArrayList<PalletHeader>) response.body();
                        if (palletHeaders.size() > 0) {
                            playSuccesSound();
//                                    textToSpeech.speak("", TextToSpeech.QUEUE_FLUSH, null);
//                                    alertSuccess(getApplicationContext(), "Lấy thành công dữ liệu của pallet " + edtPalletID.getText().toString());
                            tvMsg.setText("Lấy thành công dữ liệu của pallet " + edtPalletID.getText().toString());
                            palletHeader = palletHeaders.get(0);
                            fetchPalletDetail(edtPalletID.getText().toString());
//                                    hideSoftKeyBoard();
                            clear();
                            edtTotalIDCode.setText(String.format("%.0f", palletHeader.getTotalIDCode()));
                            edtPalletID.setFocusable(false);
                            edtPalletID.setClickable(false);
                            edtPalletID.setCursorVisible(false);
                            btnLockPalletID.setBackgroundResource(R.drawable.ic_baseline_lock_24);
                            isPalletIDLock=true;
                        } else {

                        }

                    }
                }

                @Override
                public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                }
            });
        }

        edtPalletID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    edtPalletID.requestFocus();
                    return;
                }
                if (editable.length() >= 5) {
                    Call<List<PalletHeader>> callback = iWarehouseApi.getPalletHeaderByID(edtPalletID.getText().toString().trim());
                    callback.enqueue(new Callback<List<PalletHeader>>() {
                        @Override
                        public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                            if (response.isSuccessful()) {
                                palletHeaders = (ArrayList<PalletHeader>) response.body();
                                if (palletHeaders.size() > 0) {
                                    playSuccesSound();
//                                    textToSpeech.speak("", TextToSpeech.QUEUE_FLUSH, null);
//                                    alertSuccess(getApplicationContext(), "Lấy thành công dữ liệu của pallet " + edtPalletID.getText().toString());
                                    tvMsg.setText("Lấy thành công dữ liệu của pallet " + edtPalletID.getText().toString());
                                    palletHeader = palletHeaders.get(0);
                                    fetchPalletDetail(edtPalletID.getText().toString());
//                                    hideSoftKeyBoard();
                                    clear();
                                    edtTotalIDCode.setText(String.format("%.0f", palletHeader.getTotalIDCode()));
                                    edtPalletID.setFocusable(false);
                                    edtPalletID.setClickable(false);
                                    edtPalletID.setCursorVisible(false);
                                    btnLockPalletID.setBackgroundResource(R.drawable.ic_baseline_lock_24);
                                    isPalletIDLock=true;
                                } else {

                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        btnLockPalletID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isPalletIDLock==true) {
                    edtPalletID.setFocusableInTouchMode(true);
                    edtPalletID.setFocusable(true);
                    edtPalletID.setClickable(true);
                    edtPalletID.setCursorVisible(true);
                    btnLockPalletID.setBackgroundResource(R.drawable.ic_baseline_lock_open_24);
                    isPalletIDLock=false;
                }
                else {
                    edtPalletID.setFocusable(false);
                    edtPalletID.setClickable(false);
                    edtPalletID.setCursorVisible(false);
                    btnLockPalletID.setBackgroundResource(R.drawable.ic_baseline_lock_24);
                    isPalletIDLock=true;
                }
            }
        });
    }

    private void initComponent() {
        tvStatus=findViewById(R.id.tvStatus);
        btnLockPalletID=findViewById(R.id.btn_lock_palletid);
        tvMsg = findViewById(R.id.tv_msg);
        recyclerView = findViewById(R.id.recycler_view);
        edtPalletID = findViewById(R.id.edtPalletID);
        edtLotID = findViewById(R.id.edtLotID);
        edtIDCode = findViewById(R.id.edtIDCode);
        edtPartNumber = findViewById(R.id.edtPartNumber);
        edtPartName = findViewById(R.id.edtPartName);
        edtUnit = findViewById(R.id.edtUnit);
        edtMFGDate = findViewById(R.id.edtMFGDate);
        edtEXPDate = findViewById(R.id.edtEXPDate);
        edtQuantity = findViewById(R.id.edtQuantity);
        edtTotalIDCode = findViewById(R.id.edtTotalIDCode);
        storage = getSharedPreferences(ServerConfigFragment.PREFERENCES, Context.MODE_PRIVATE);
        palletDetails = new ArrayList<PalletDetail>();
        goodsList = new ArrayList<Goods>();
        palletHeaders = new ArrayList<PalletHeader>();
        palletHeader = new PalletHeader();

        textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });
    }

    private String getDataFromIntent(){
        Intent intent = getIntent();
        String id = intent.getStringExtra(PALLET_ID);
        return  id;
    }

    private void clear(){
        edtIDCode.setText("");
        edtLotID.setText("");
        edtPartNumber.setText("");
        edtPartName.setText("");
        edtMFGDate.setText("");
        edtEXPDate.setText("");
        edtQuantity.setText("");
        edtUnit.setText("");
        edtTotalIDCode.setText(0+"");
    }


    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();

    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void playErrorSound() {
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(200);
        }
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("error.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void playSuccesSound() {
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("ok.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void playDropSound() {
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("drop.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(ArrayList<PalletDetail> palletDetailArrayList) {
        adapter = new CreatePalletIDAdapter(getApplicationContext(), palletDetailArrayList, new CreatePalletIDAdapter.ItemClickListener() {
            @Override
            public void onClick(PalletDetail palletDetail) {

            }
        }, new CreatePalletIDAdapter.ItemDeleteListener() {
            @Override
            public void onClick(PalletDetail palletDetail) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CreatePalletActivity.this);
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete, viewGroup, false);
                builder.setView(dialogView);
                final android.app.AlertDialog alertDialog = builder.create();
                alertDialog.show();

                Button btnYes, btnNo;
                btnYes = alertDialog.findViewById(R.id.btnYes);
                btnNo = alertDialog.findViewById(R.id.btnNo);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(palletHeader.getHandlingStatusName().equals("Duyệt mức 2")){
                            alertError(CreatePalletActivity.this,"Không thể xóa dữ liệu đã duyệt mức 2");
                            alertDialog.cancel();
                            return;
                        }
                        deletePalletDetail(palletDetail.getPalletID(),palletDetail.getIdCode());
                    }
                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                    }
                });

            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pallet_manager_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_addnew) {
            Call<List<PalletHeader>> callback = iWarehouseApi.getAllPalletHeader();
            callback.enqueue(new Callback<List<PalletHeader>>() {
                @Override
                public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                    if (response.isSuccessful()) {
                        Call<List<PalletHeader>> callback = iWarehouseApi.getPalletHeaderByID(edtPalletID.getText().toString());
                        callback.enqueue(new Callback<List<PalletHeader>>() {
                            @Override
                            public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response2) {
                                if (response2.isSuccessful()) {
                                    if(response2.body().size()==0) {
                                        PalletHeader palletHeader = new PalletHeader();
                                        palletHeader.setPalletID(edtPalletID.getText().toString());
                                        palletHeader.setStatus("0");
                                        palletHeader.setTotalIDCode(0.0);
                                        palletHeader.setOrdinal(response.body().size() + 1);
                                        palletHeader.setHandlingStatusID("0");
                                        palletHeader.setHandlingStatusName("Chưa duyệt");
                                        palletHeader.setCreatedDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                                        palletHeader.setCreatedUserID(Common.USER_ID);
                                        createPalletHeader(palletHeader);

                                        edtPalletID.setFocusable(false);
                                        edtPalletID.setClickable(false);
                                        edtPalletID.setCursorVisible(false);
                                        btnLockPalletID.setBackgroundResource(R.drawable.ic_baseline_lock_24);
                                        isPalletIDLock = true;
                                        tvMsg.setText("Đã tạo thành công mã pallet "+edtPalletID.getText().toString());
//                                        hideSoftKeyBoard();
                                        autocompletePalletID();
                                    }
                                    else {
                                        playErrorSound();
                                        tvMsg.setText("Mã pallet "+edtPalletID.getText().toString()+" đã tồn tại trong hệ thống");
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                }
            });
            return true;
        }
        if (id == R.id.action_save) {
            if (palletHeader != null) {
                PalletHeader palletHeaderUpdate = palletHeader;
                palletHeaderUpdate.setStatus("1");
                palletHeader.setHandlingStatusID("1");
                palletHeader.setHandlingStatusName("Duyệt mức 1");
                palletHeader.setUpdatedDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                palletHeader.setUpdatedUserID(Common.USER_ID);
                putPalletHeader(edtPalletID.getText().toString(), palletHeaderUpdate.getOrdinal(), palletHeaderUpdate);
                alertSuccess(getApplicationContext(), "Lưu thành công dữ liệu mã hàng của pallet " + edtPalletID.getText().toString());
                MessageHelper.successMessage(tvMsg,"Lưu thành công dữ liệu mã hàng của pallet " + edtPalletID.getText().toString());
                MessageHelper.playDropSound(CreatePalletActivity.this);
            }
            return true;
        }
        if (id == R.id.action_clear) {
            if(palletHeaders.size()>0) {
                if (palletHeaders.get(0).getHandlingStatusID().equalsIgnoreCase("3")){
                    MessageHelper.errorMessage(tvMsg,"Duyệt mức 2 không thể xóa dữ liệu");
                }
                else {
                    if (palletDetails.size() > 0) {
                        deletePalletDetailByPalletID(edtPalletID.getText().toString());
                        alertSuccess(getApplicationContext(), "Xóa thành công tất cả mã hàng của pallet " + edtPalletID.getText().toString());
                    }
                }
            }
            return true;
        }
        if (id == R.id.action_list) {
            startActivity(new Intent(CreatePalletActivity.this,PalletListActivity.class));
            return true;
        }
        if (id == R.id.action_print) {
            sendZPL();
//            new InitTask().execute();
            return true;
        }
        if (id == R.id.action_delete) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(CreatePalletActivity.this);
            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete_pallet, viewGroup, false);
            builder.setView(dialogView);
            final android.app.AlertDialog alertDialog = builder.create();
            alertDialog.show();

            Button btnYes, btnNo;
            btnYes = alertDialog.findViewById(R.id.btnYes);
            btnNo = alertDialog.findViewById(R.id.btnNo);
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deletePalletHeaderlByPalletID(edtPalletID.getText().toString());
                    deletePalletDetailByPalletID2(edtPalletID.getText().toString());
                    alertDialog.cancel();
                    MessageHelper.successMessage(tvMsg,"Xóa thành công Pallet "+edtPalletID.getText().toString());
                    MessageHelper.playSuccesSound(CreatePalletActivity.this);
                    recyclerView.setAdapter(null);
                    clear();
                    autocompletePalletID();
                }
            });
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.cancel();
                }
            });

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class InitTask extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... params) {
            // TODO Auto-generated method stub
            try {
                printerConnection = new TcpConnection(PrintSettingHelper.getIp(CreatePalletActivity.this), 9100);
            } catch (NumberFormatException e) {
                helper.showErrorDialogOnGuiThread("Port number is invalid");
            }
            try {
                helper.showLoadingDialog("Connecting...");
                printerConnection.open();

                ZebraPrinter printer = null;

                if (printerConnection.isConnected()) {
                    printer = ZebraPrinterFactory.getInstance(printerConnection);

                    if (printer != null) {
                        PrinterLanguage pl = printer.getPrinterControlLanguage();
                        if (pl == PrinterLanguage.CPCL) {
                            helper.showErrorDialogOnGuiThread("This demo will not work for CPCL printers!");
                        } else {
                            // [self.connectivityViewController setStatus:@"Building receipt in ZPL..." withColor:[UIColor
                            // cyanColor]];
                            sendTestLabel();
                            PalletHeader palletHeaderUpdate=palletHeader;
                            palletHeader.setStatus("Đã in");
                            putPalletHeader(palletHeaderUpdate.getPalletID(),palletHeaderUpdate.getOrdinal(),palletHeaderUpdate);
                        }
                        printerConnection.close();

                    }
                }
            } catch (ConnectionException e) {
                helper.showErrorDialogOnGuiThread(e.getMessage());
            } catch (ZebraPrinterLanguageUnknownException e) {
                helper.showErrorDialogOnGuiThread("Could not detect printer language");
            } finally {
                helper.dismissLoadingDialog();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }
    }

    private void sendTestLabel() {
        try {
            byte[] configLabel = createZplReceipt().getBytes();
            printerConnection.write(configLabel);
            DemoSleeper.sleep(1500);
            if (printerConnection instanceof BluetoothConnection) {
                DemoSleeper.sleep(500);
            }
        } catch (ConnectionException e) {
        }
    }


    private String createZplReceipt() {
        String footer = String.format(
                "^XA" +
                        " ^FO200,20" + "\r\n" + "^BY3" + "\r\n" + "^B3N,N,100,Y,N" + "\r\n" + "^FD" + edtPalletID.getText().toString() + "^FS" + "\r\n" + "^XZ");


        String wholeZplLabel = String.format("%s", footer);

        return wholeZplLabel;
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
                    // DO WHATEVER YOU WANT.
                    edtPalletID.setText(intent.getStringExtra(PALLET_ID));
                    unregisterReceiver(this);
                }
            }
        };
        registerReceiver(broadcastReceiver, new IntentFilter("finish_activity"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, BarcodeManager.ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanDataCollection.ScanData> scanData = scanDataCollection.getScanData();
            for (ScanDataCollection.ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        StatusData.ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = Scanner.TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = Scanner.TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = Scanner.TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(EMDKManager.FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    String palletID = "";
                    String lotID = "";
                    String partNumber = "";
                    String partName = "";
                    String unit = "";
                    String mfgDate = "";
                    String expDate = "";
                    String quantity = "";
                    String idCode = "";
                    int index1 = result.indexOf("|");
                    int index2 = result.indexOf("|", index1 + 1);
                    int index3 = result.indexOf("|", index2 + 1);
                    int index4 = result.indexOf("|", index3 + 1);
                    int index5 = result.indexOf("|", index4 + 1);
                    int index6 = result.indexOf("|", index5 + 1);
                    int index7 = result.indexOf("|", index6 + 1);
                    int index8 = result.indexOf("|", index7 + 1);
                    int index9 = result.indexOf("|", index8 + 1);
                    int index10 = result.indexOf("|", index9 + 1);
                    int index11 = result.indexOf("|", index10 + 1);
                    int index12 = result.indexOf("|", index11 + 1);
                    int index13 = result.lastIndexOf("|");


                    if (index13 > 0) {
                        idCode = result.substring(index2 + 1, index3);
                        lotID = result.substring(index8 + 1, index9);
                        partNumber = result.substring(index5 + 1, index6);
                        partName = result.substring(index6 + 1, index7);
                        unit = result.substring(index4 + 1, index5);
                        mfgDate = result.substring(index9 + 1, index10);
                        expDate = result.substring(index10 + 1, index11);
                        quantity = result.substring(index3 + 1, index4);
                        palletID = idCode.substring(0, 5);

                        edtIDCode.setText(idCode);
                        edtLotID.setText(lotID);
                        edtQuantity.setText(quantity);
                        edtIDCode.setText(idCode);
                        edtPartNumber.setText(partNumber);
                        edtPartName.setText(partName);
                        edtUnit.setText(unit);
                        edtMFGDate.setText(mfgDate);
                        edtEXPDate.setText(expDate);

                        fetchPalletHeader(edtPalletID.getText().toString());
                        fetchGoods(partNumber, palletID);
                        process();
                    } else {
                        tvMsg.setText("Quét sai mã code");
                        playErrorSound();
                    }
                }
            }
        });
    }

    private ArrayList<Goods> fetchGoods(String partNumber, String palletID) {

        Call<List<Goods>> callback = iWarehouseApi.getGoodsByParams2(partNumber, palletID);
        callback.enqueue(new Callback<List<Goods>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<Goods>> call, Response<List<Goods>> response) {
                if (response.isSuccessful()) {
                    goodsList = (ArrayList<Goods>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<Goods>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return goodsList;
    }

    private void process() {
        Call<List<InventoryDetail>> callback = iWarehouseApi.CheckExist("1",edtPartNumber.getText().toString(),edtPalletID.getText().toString(),edtIDCode.getText().toString());
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size()>0) {
                        playErrorSound();
                        tvMsg.setText("Mã hàng: "+edtIDCode.getText().toString()+" đã có tồn kho!!!");
                    }
                    else {
                        createPallet();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createPallet(){
        Call<List<PalletDetail>> callback = iWarehouseApi.getPalletDetailByParams(edtPalletID.getText().toString(), edtIDCode.getText().toString());
        callback.enqueue(new Callback<List<PalletDetail>>() {
            @Override
            public void onResponse(Call<List<PalletDetail>> call, Response<List<PalletDetail>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        playErrorSound();
                        tvMsg.setText("Trùng mã hàng!!!");
                    } else {
                        Call<List<PalletDetail>> callback = iWarehouseApi.getPalletDetailByIDCode(edtIDCode.getText().toString());
                        callback.enqueue(new Callback<List<PalletDetail>>() {
                            @Override
                            public void onResponse(Call<List<PalletDetail>> call, Response<List<PalletDetail>> response2) {
                                if (response2.isSuccessful()) {
                                    if (response2.body().size()>0) {
                                        String palletID=((ArrayList<PalletDetail>)response2.body()).get(0).getPalletID();
                                        playErrorSound();
                                        tvMsg.setText("Mã hàng "+edtIDCode.getText().toString()+" đã tồn tại trong pallet"+palletID);
                                    }
                                    else {
                                        playSuccesSound();
                                        try {
                                            if(TextUtils.isEmpty(edtPalletID.getText().toString())||palletHeader.getTotalIDCode()==null){
                                                playErrorSound();
                                                return;
                                            }
                                            PalletDetail palletDetailCreate = new PalletDetail();
                                            palletDetailCreate.setPalletID(edtPalletID.getText().toString());
                                            palletDetailCreate.setOrdinal((int) (palletHeader.getTotalIDCode() + 1));
                                            palletDetailCreate.setIdCode(edtIDCode.getText().toString());
                                            palletDetailCreate.setPartNumber(edtPartNumber.getText().toString());
                                            palletDetailCreate.setPartName(edtPartName.getText().toString());
                                            palletDetailCreate.setLotID(edtLotID.getText().toString());
                                            if (goodsList.size() > 0) {
                                                palletDetailCreate.setProductFamily(goodsList.get(0).getProductFamily());
                                            }
                                            palletDetailCreate.setUnit(edtUnit.getText().toString());
                                            palletDetailCreate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                                            palletDetailCreate.setMfgDate(simpleDateFormat.format(originalFormat.parse(edtMFGDate.getText().toString())));
                                            palletDetailCreate.setExpDate(simpleDateFormat.format(originalFormat.parse(edtEXPDate.getText().toString())));
                                            palletDetailCreate.setReceiptDate(simpleDateFormat.format(Calendar.getInstance().getTime()));

                                            createPalletDetail(palletDetailCreate);

                                            PalletHeader palletHeaderUpdate = palletHeader;
                                            palletHeaderUpdate.setTotalIDCode(palletHeader.getTotalIDCode() + 1);
                                            putPalletHeader(edtPalletID.getText().toString(), palletHeaderUpdate.getOrdinal(), palletHeaderUpdate);
                                            tvMsg.setText("success");
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<List<PalletDetail>> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PalletDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createPalletHeader(PalletHeader palletHeader) {
        Call<PalletHeader> callback = iWarehouseApi.createPalletHeader(palletHeader);
        callback.enqueue(new Callback<PalletHeader>() {
            @Override
            public void onResponse(Call<PalletHeader> call, Response<PalletHeader> response) {
                fetchPalletHeader(edtPalletID.getText().toString());
            }

            @Override
            public void onFailure(Call<PalletHeader> call, Throwable t) {

            }
        });
    }

    private void createPalletDetail(PalletDetail palletDetail) {
        Call<PalletDetail> callback = iWarehouseApi.createPalletDetail(palletDetail);
        callback.enqueue(new Callback<PalletDetail>() {
            @Override
            public void onResponse(Call<PalletDetail> call, Response<PalletDetail> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<PalletDetail> call, Throwable t) {

            }
        });
    }

    private void putPalletHeader(String palletID, int ordinal, PalletHeader palletHeader) {
        Call<PalletHeader> callback = iWarehouseApi.updatePalletHeader(palletID, ordinal, palletHeader);
        callback.enqueue(new Callback<PalletHeader>() {
            @Override
            public void onResponse(Call<PalletHeader> call, Response<PalletHeader> response) {
                if (response.isSuccessful()) {
                    fetchPalletHeader(palletID);
                }
            }

            @Override
            public void onFailure(Call<PalletHeader> call, Throwable t) {

            }
        });
    }

    private void putPalletDetail(String palletID, int ordinal, String idCode, PalletDetail palletDetail) {
        Call<PalletDetail> callback = iWarehouseApi.updatePalletDetail(palletID, ordinal, idCode, palletDetail);
        callback.enqueue(new Callback<PalletDetail>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<PalletDetail> call, Response<PalletDetail> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchPalletDetail(palletID);

                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<PalletDetail> call, Throwable t) {

            }
        });
    }

    private PalletHeader fetchPalletHeader(String palletID) {
        Call<List<PalletHeader>> callback = iWarehouseApi.getPalletHeaderByID(palletID);
        callback.enqueue(new Callback<List<PalletHeader>>() {
            @Override
            public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                if (response.isSuccessful()) {
                    palletHeaders = (ArrayList<PalletHeader>) response.body();
                    if (palletHeaders.size() > 0) {
                        palletHeader = palletHeaders.get(0);
//                        hideSoftKeyBoard();
                        fetchPalletDetail(edtPalletID.getText().toString());
                        edtTotalIDCode.setText(String.format("%.0f", palletHeader.getTotalIDCode()));
//                        edtIDCode.requestFocus();
                        tvStatus.setText(palletHeader.getHandlingStatusName());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return palletHeader;
    }

    private ArrayList<PalletDetail> fetchPalletDetail(String palletID) {
        Call<List<PalletDetail>> callback = iWarehouseApi.getPalletDetailById(palletID);
        callback.enqueue(new Callback<List<PalletDetail>>() {
            @Override
            public void onResponse(Call<List<PalletDetail>> call, Response<List<PalletDetail>> response) {
                if (response.isSuccessful()) {
                    palletDetails = (ArrayList<PalletDetail>) response.body();
                    if (palletDetails.size() > 0) {

                    }
                    setAdapter(palletDetails);
                }
            }

            @Override
            public void onFailure(Call<List<PalletDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return palletDetails;
    }

    private void deletePalletHeaderlByPalletID(String palletID) {

        Call<Boolean> callback = iWarehouseApi.DeletePalletHeader(palletID);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deletePalletDetailByPalletID2(String palletID) {

        Call<Boolean> callback = iWarehouseApi.DeletePalletHeader(palletID);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deletePalletDetailByPalletID(String palletID) {

        Call<Boolean> callback = iWarehouseApi.DeletePalletDetail(palletID);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    fetchPalletDetail(edtPalletID.getText().toString());

                    PalletHeader palletHeaderUpdate = palletHeader;
                    palletHeaderUpdate.setTotalIDCode(0.0);
                    putPalletHeader(edtPalletID.getText().toString(), palletHeaderUpdate.getOrdinal(), palletHeaderUpdate);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deletePalletDetail(String palletID,String idCode) {

        Call<Boolean> callback = iWarehouseApi.DeletePalletDetail(palletID,idCode);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    PalletHeader palletHeaderUpdate = palletHeader;
                    palletHeaderUpdate.setTotalIDCode(palletHeaderUpdate.getTotalIDCode()-1);
                    putPalletHeader(edtPalletID.getText().toString(), palletHeaderUpdate.getOrdinal(), palletHeaderUpdate);
                    tvMsg.setText("Xóa thành công mã hàng "+idCode);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void autocompletePalletID() {
        Call<List<PalletHeader>> callback = iWarehouseApi.getAllPalletHeader();
        callback.enqueue(new Callback<List<PalletHeader>>() {
            @Override
            public void onResponse(Call<List<PalletHeader>> call, Response<List<PalletHeader>> response) {
                if (response.isSuccessful()) {
                    ArrayList<String> stringArrayList = new ArrayList<>();
                    for (PalletHeader palletHeader : (ArrayList<PalletHeader>) response.body()) {
                        stringArrayList.add(palletHeader.getPalletID());
                    }
                    ArrayAdapter adapterCountries
                            = new ArrayAdapter(CreatePalletActivity.this, android.R.layout.simple_list_item_1, stringArrayList);
                    edtPalletID.setAdapter(adapterCountries);
                    edtPalletID.setThreshold(1);
                }
            }

            @Override
            public void onFailure(Call<List<PalletHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendZPL(){

        Call<String> callback = iWarehouseApi.postZPLString(createZplReceipt());
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                alertSuccess(CreatePalletActivity.this,"In thành công pallet label "+edtPalletID.getText().toString());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }
}