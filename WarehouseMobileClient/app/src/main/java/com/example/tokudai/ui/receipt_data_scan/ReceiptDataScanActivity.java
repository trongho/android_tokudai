package com.example.tokudai.ui.receipt_data_scan;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokudai.R;
import com.example.tokudai.adapter.IDCodeAdapter;
import com.example.tokudai.adapter.WRDataGeneralScanAdapter;
import com.example.tokudai.model.Goods;
import com.example.tokudai.model.Inventory;
import com.example.tokudai.model.InventoryDetail;
import com.example.tokudai.model.WRDataGeneral;
import com.example.tokudai.model.WRDataHeader;
import com.example.tokudai.service.IWarehouseApi;
import com.example.tokudai.service.RetrofitService;
import com.example.tokudai.ui.pallet_manager.CreatePalletActivity;
import com.example.tokudai.ui.setting.ServerConfigFragment;
import com.example.tokudai.ui.setting.SettingFragment;
import com.example.tokudai.ui.user.UserFragment;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceiptDataScanActivity extends AppCompatActivity implements EMDKManager.EMDKListener, Scanner.DataListener, Scanner.StatusListener, BarcodeManager.ScannerConnectionListener {
    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;
    private Spinner spinnerScannerDevices = null;
    private List<ScannerInfo> deviceList = null;
    private final Object lock = new Object();
    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;

    private String numberRegex = "\\d+(?:\\.\\d+)?";

    public TextView tvEmpty = null;
    private RecyclerView recyclerView = null;
    private EditText edtWRDNumber = null;
    private EditText edtPalletID = null;
    private EditText edtLotID = null;
    private EditText edtIDCode = null;
    private EditText edtPartNumber = null;
    private EditText edtPartName = null;
    private EditText edtUnit = null;
    private EditText edtMFGDate = null;
    private EditText edtEXPDate = null;
    private EditText edtQuantity = null;
    private EditText edtTotalQuantity = null;
    private EditText edtTotalGoods = null;
    private Button btnClearWRDNumber = null;
    private Button btnClearPalletID = null;
    private Button btnClearQuantity = null;
    private WRDataGeneralScanAdapter adapter;
    ArrayList<WRDataGeneral> wrDataGenerals;
    ArrayList<WRDataGeneral> arrayListByGoodsID;
    ArrayList<WRDataHeader> wrDataHeaders;
    WRDataHeader wrDataHeader;
    IWarehouseApi iWarehouseApi = RetrofitService.getService();
    private static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
    SimpleDateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Boolean isGoodsIDExist = false;
    Boolean isGoodsIdDuplicate = false;
    Boolean isInventory=false;
    Double totalGoods = 0.0;
    Double totalQuantity = 0.0;

    ArrayList<Inventory> inventoryList = null;

    String lastWRDNumber = "";
    ArrayList<Goods> goodsList = null;
    SharedPreferences storage = null;

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_data_scan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Scan nhập hàng");

        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }
        initComponent();

        edtWRDNumber.requestFocus();

        wrDataHeaders = new ArrayList<>();
        wrDataGenerals = new ArrayList<>();
        arrayListByGoodsID = new ArrayList<>();
        inventoryList = new ArrayList<Inventory>();
        goodsList = new ArrayList<Goods>();


        edtWRDNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(editable)) {
                    alertError(getApplicationContext(), "Nhập số phiếu");
                    edtWRDNumber.requestFocus();
                    return;
                }
                if (editable.length() >= 11) {
                    Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(edtWRDNumber.getText().toString().trim());
                    callback.enqueue(new Callback<List<WRDataHeader>>() {
                        @Override
                        public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                            if (response.isSuccessful()) {
                                wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                                if (wrDataHeaders.size() > 0) {
                                    playSuccesSound();
                                    alertSuccess(getApplicationContext(), "Lấy dữ liệu thành công");
                                    wrDataHeader = wrDataHeaders.get(0);
                                    fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                                    hideSoftKeyBoard();
                                    edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                                    edtPalletID.requestFocus();
                                } else {
//                                    alertError(getApplicationContext(), "Không tìm thấy chi tiết dữ liệu");
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    fetchWRDataDetailData(edtWRDNumber.getText().toString());
                }
            }
        });

        edtPalletID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }
            }
        });

        edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (TextUtils.isEmpty(edtWRDNumber.getText().toString())) {
                    alertError(getApplicationContext(), "Nhập Conveyance number");
                    edtWRDNumber.requestFocus();
                    return;
                }

            }
        });

        btnClearWRDNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtWRDNumber.setText("");
            }
        });

        btnClearPalletID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtPalletID.setText("");
            }
        });

        btnClearQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtQuantity.setText("");
            }
        });
    }

    private void initComponent() {
        tvEmpty = findViewById(R.id.tv_empty);
        recyclerView = findViewById(R.id.recycler_view);
        edtWRDNumber = findViewById(R.id.edtWRDNumber);
        edtPalletID = findViewById(R.id.edtPalletID);
        edtLotID = findViewById(R.id.edtLotID);
        edtIDCode = findViewById(R.id.edtIDCode);
        edtPartNumber = findViewById(R.id.edtPartNumber);
        edtPartName = findViewById(R.id.edtPartName);
        edtUnit = findViewById(R.id.edtUnit);
        edtMFGDate = findViewById(R.id.edtMFGDate);
        edtEXPDate = findViewById(R.id.edtEXPDate);
        edtQuantity = findViewById(R.id.edtQuantity);
        btnClearWRDNumber = findViewById(R.id.btn_clear_wrdnumber);
        btnClearPalletID = findViewById(R.id.btn_clear_palletid);
        btnClearQuantity = findViewById(R.id.btn_clear_quantity);
        edtTotalQuantity = findViewById(R.id.edtTotalQuantity);
        edtTotalGoods = findViewById(R.id.edtTotalGoods);
        storage = getSharedPreferences(ServerConfigFragment.PREFERENCES, Context.MODE_PRIVATE);
    }

    private void dialogDetail(){
        if (TextUtils.isEmpty(edtWRDNumber.getText().toString()) || TextUtils.isEmpty(edtPalletID.getText().toString())
                || TextUtils.isEmpty(edtIDCode.getText().toString()) || TextUtils.isEmpty(edtWRDNumber.getText().toString())
                || TextUtils.isEmpty(edtLotID.getText().toString())) {
            alertError(ReceiptDataScanActivity.this, "error");
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ReceiptDataScanActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_idcode_list, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnNo;
        Button btnDeleteAll;
        TextView tvWRDNumber = alertDialog.findViewById(R.id.tvWRDNumber);
        TextView tvPalletID = alertDialog.findViewById(R.id.tvPalletID);
        RecyclerView recyclerViewIDCode;
        recyclerViewIDCode = alertDialog.findViewById(R.id.recycler_view_idcode);

        tvWRDNumber.setText(edtWRDNumber.getText().toString());
        tvPalletID.setText(edtPalletID.getText().toString());

        // now setting the simpleAdapter to the ListView
        String[] str = edtIDCode.getText().toString().split(",");
        List<String> stringList =new LinkedList<String>( Arrays.asList(str.clone()));

        IDCodeAdapter idCodeAdapter=new IDCodeAdapter(ReceiptDataScanActivity.this, stringList, new IDCodeAdapter.ItemDeleteListener() {
            @Override
            public void onClick(String p,int position) {
                Call<List<WRDataGeneral>> callback = iWarehouseApi.GetByParams3(edtWRDNumber.getText().toString(), edtPalletID.getText().toString(), edtLotID.getText().toString());
                callback.enqueue(new Callback<List<WRDataGeneral>>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                        if (response.isSuccessful()) {
                            AlertDialog alertDialog = new AlertDialog.Builder(ReceiptDataScanActivity.this).create();
                            alertDialog.setTitle("Alert");
                            alertDialog.setMessage("Alert message to be shown");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (((ArrayList<WRDataGeneral>) response.body()).size() > 0) {
                                                WRDataGeneral wrDataGeneral = ((ArrayList<WRDataGeneral>) response.body()).get(0);
                                                String idCodeString = wrDataGeneral.getIdCode();
                                                String f = idCodeString.substring(0, idCodeString.indexOf(p));
                                                String s = idCodeString.substring(idCodeString.indexOf(p) + p.length() + 1);
                                                if (idCodeString.length() == 0) {
                                                    deleteWRDataGeneral(wrDataGeneral.getWrdNumber(), wrDataGeneral.getPalletID(), wrDataGeneral.getLotID());

                                                    WRDataHeader wrDataHeader = wrDataHeaders.get(0);
                                                    wrDataHeader.setTotalQuantity(wrDataHeader.getTotalQuantity() - wrDataGeneral.getQuantity());
                                                    putWRDataHeader(wrDataHeader.getWrdNumber(), wrDataHeader);

                                                    recyclerViewIDCode.setAdapter(null);
                                                    return;
                                                }

                                                wrDataGeneral.setIdCode(f.concat(s));
                                                wrDataGeneral.setTotalQuantity(wrDataGeneral.getTotalQuantity() - wrDataGeneral.getPackingVolume());
                                                wrDataGeneral.setQuantity(wrDataGeneral.getQuantity() - wrDataGeneral.getPackingVolume());
                                                putWRDataGeneral(wrDataGeneral.getWrdNumber(), wrDataGeneral.getPalletID(), wrDataGeneral.getOrdinal(), wrDataGeneral.getLotID(), wrDataGeneral);

                                                WRDataHeader wrDataHeader = wrDataHeaders.get(0);
                                                wrDataHeader.setTotalQuantity(wrDataHeader.getTotalQuantity() - wrDataGeneral.getPackingVolume());
                                                putWRDataHeader(wrDataHeader.getWrdNumber(), wrDataHeader);

                                            }
                                        }
                                    });
                            alertDialog.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        recyclerViewIDCode.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerViewIDCode.setAdapter(idCodeAdapter);
        idCodeAdapter.notifyDataSetChanged();

        btnNo = alertDialog.findViewById(R.id.btnCancel);
        btnDeleteAll = alertDialog.findViewById(R.id.btnDeleteAll);
        btnDeleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ReceiptDataScanActivity.this);
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete, viewGroup, false);
                builder.setView(dialogView);
                final AlertDialog alertDialog = builder.create();
                alertDialog.show();

                Button btnYes, btnNo;
                btnYes = alertDialog.findViewById(R.id.btnYes);
                btnNo = alertDialog.findViewById(R.id.btnNo);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteWRDataGeneral(edtWRDNumber.getText().toString(), edtPalletID.getText().toString(),edtLotID.getText().toString());
                        recyclerViewIDCode.setAdapter(null);
                        alertDialog.cancel();
                    }
                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.cancel();
                    }
                });
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void clear() {
        edtPalletID.setText("");
        edtQuantity.setText("");
    }

    private WRDataHeader fetchWRDataHeaderData(String wrdNumber) {
        Call<List<WRDataHeader>> callback = iWarehouseApi.getWRDataHeader(wrdNumber);
        callback.enqueue(new Callback<List<WRDataHeader>>() {
            @Override
            public void onResponse(Call<List<WRDataHeader>> call, Response<List<WRDataHeader>> response) {
                if (response.isSuccessful()) {
                    wrDataHeaders = (ArrayList<WRDataHeader>) response.body();
                    if (wrDataHeaders.size() > 0) {
                        wrDataHeader = wrDataHeaders.get(0);
                        hideSoftKeyBoard();
                        fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                        edtTotalQuantity.setText(String.format("%.0f", wrDataHeader.getTotalQuantity()));
                        edtPalletID.requestFocus();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<WRDataHeader>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch wrdataheader fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataHeader;
    }

    private ArrayList<WRDataGeneral> fetchWRDataGeneralData(String wrdNumber) {
        Call<List<WRDataGeneral>> callback = iWarehouseApi.getWRDataGeneralById(wrdNumber);
        callback.enqueue(new Callback<List<WRDataGeneral>>() {
            @Override
            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                if (response.isSuccessful()) {
                    wrDataGenerals = (ArrayList<WRDataGeneral>) response.body();
                    if (wrDataGenerals.size() > 0) {
                        totalGoods = 0.0;
                        getTotalGoods(wrDataGenerals);
                        edtTotalGoods.setText(String.format("%.0f", totalGoods));
                    }
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  wrdatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return wrDataGenerals;
    }


    private void putWRDataHeader(String wRDNumber, WRDataHeader wrDataHeader) {
        Call<WRDataHeader> callback = iWarehouseApi.updateWRDataHeader(wRDNumber, wrDataHeader);
        callback.enqueue(new Callback<WRDataHeader>() {
            @Override
            public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataHeaderData(wRDNumber);
                }
            }

            @Override
            public void onFailure(Call<WRDataHeader> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdataheader fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral(String wRDNumber, String palletID, int ordinal, String lotID, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, palletID, ordinal, lotID, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataGeneralData(wRDNumber);
//                    getTotalQuantity(wrDataGenerals);
//                    getTotalGoods(wrDataGenerals);
//                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
//                    edtTotalGoods.setText(String.format("%.0f", totalGoods));

                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void putWRDataGeneral2(String wRDNumber, String palletID, int ordinal, String lotID, WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.updateWRDataGeneral(wRDNumber, palletID, ordinal, lotID, wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();
                    fetchWRDataGeneralData(wRDNumber);
                    getTotalQuantity(wrDataGenerals);
                    getTotalGoods(wrDataGenerals);
                    edtTotalQuantity.setText(String.format("%.0f", totalQuantity));
                    edtTotalGoods.setText(String.format("%.0f", totalGoods));

                } else {
                    alertError(getApplicationContext(), "Cập nhật dữ liệu nhập thất bại");
                }

            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put wrdatadetail fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Inventory> fetchInventory(int year, int month, String warhouseID, String partNumber) {

        Call<List<Inventory>> callback = iWarehouseApi.GetInventoryByPartNumber(year, month, warhouseID, partNumber);
        callback.enqueue(new Callback<List<Inventory>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<Inventory>> call, Response<List<Inventory>> response) {
                if (response.isSuccessful()) {
                    inventoryList = (ArrayList<Inventory>) response.body();
                    //Cập nhật tồn kho
                    int year = Calendar.getInstance().get(Calendar.YEAR);
                    int month = YearMonth.now().getMonthValue();
                    if (inventoryList.size() > 0) {
                        Inventory inventory = inventoryList.get(0);
                        String idCodeString = inventory.getIdCode();
                        inventory.setIdCode(idCodeString.concat(edtIDCode.getText().toString() + ","));

                        putInventory(year, month, "1", partNumber, inventory);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Inventory>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return inventoryList;
    }

    private void putInventory(int year, int month, String warhouseID, String partNumber, Inventory inventory) {
        Call<Inventory> callback = iWarehouseApi.updateInventory(year, month, warhouseID, partNumber, inventory);
        callback.enqueue(new Callback<Inventory>() {
            @Override
            public void onResponse(Call<Inventory> call, Response<Inventory> response) {
                if (response.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), "Success!!!", Toast.LENGTH_SHORT).show();

                } else {
                    alertError(getApplicationContext(), "Cập nhật tồn kho thất bại");
                }

            }

            @Override
            public void onFailure(Call<Inventory> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "put fail" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getLastID(String str) {
        Call<String> callback = iWarehouseApi.GetLastID(str);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Calendar c = Calendar.getInstance();
                String curentYear = originalFormat.format(c.getTime()).substring(2, 4);
                if (response.body() != null) {
                    lastWRDNumber = response.body();
                    String[] arrListStr = lastWRDNumber.split("-");
                    String lStr = arrListStr[2];
                    int lStrInt = Integer.parseInt(lStr) + 1;
                    edtWRDNumber.setText(str + "-" + curentYear + "000" + "-" + String.format("%05d", lStrInt));

                } else {
                    edtWRDNumber.setText(str + "-" + curentYear + "000" + "-" + String.format("%05d", 1));
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch last id fail", Toast.LENGTH_SHORT).show();
            }
        });
        return lastWRDNumber;
    }

    private void createWRDataHeader(WRDataHeader wrDataHeader, String str) {
        Call<String> callback = iWarehouseApi.GetLastID(str);
        callback.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Calendar c = Calendar.getInstance();
                String curentYear = simpleDateFormat.format(c.getTime()).substring(2, 4);
                if (response.body() != null) {
                    lastWRDNumber = response.body();
                    String[] arrListStr = lastWRDNumber.split("-");
                    String lStr = arrListStr[2];
                    int lStrInt = Integer.parseInt(lStr) + 1;
                    edtWRDNumber.setText(str + "-" + curentYear + "000" + "-" + String.format("%05d", lStrInt));
                    lastWRDNumber = str + "-" + curentYear + "000" + "-" + String.format("%05d", lStrInt);

                } else {
                    edtWRDNumber.setText(str + "-" + curentYear + "000" + "-" + String.format("%05d", 1));
                    lastWRDNumber = str + "-" + curentYear + "000" + "-" + String.format("%05d", 1);
                }

                wrDataHeader.setWrdNumber(lastWRDNumber);
                Call<WRDataHeader> callback = iWarehouseApi.creatWRDataHeader(wrDataHeader);
                callback.enqueue(new Callback<WRDataHeader>() {
                    @Override
                    public void onResponse(Call<WRDataHeader> call, Response<WRDataHeader> response) {

                        alertSuccess(getApplicationContext(), "Tạo thành công lệnh nhập hàng");
                    }

                    @Override
                    public void onFailure(Call<WRDataHeader> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch last id fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createWRDataGeneral(WRDataGeneral wrDataGeneral) {
        Call<WRDataGeneral> callback = iWarehouseApi.creatWRDataGeneral(wrDataGeneral);
        callback.enqueue(new Callback<WRDataGeneral>() {
            @Override
            public void onResponse(Call<WRDataGeneral> call, Response<WRDataGeneral> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<WRDataGeneral> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failture" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Goods> fetchGoods(String partNumber, String palletID) {

        Call<List<Goods>> callback = iWarehouseApi.getGoodsByParams2(partNumber, palletID);
        callback.enqueue(new Callback<List<Goods>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<List<Goods>> call, Response<List<Goods>> response) {
                if (response.isSuccessful()) {
                    goodsList = (ArrayList<Goods>) response.body();
                }
            }

            @Override
            public void onFailure(Call<List<Goods>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return goodsList;
    }

    private void deleteWRDataHeader(String wrdNumber) {

        Call<Boolean> callback = iWarehouseApi.DeleteWRDataHeader(wrdNumber);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Xóa thành công lệnh nhập " + edtWRDNumber.getText().toString());
                    edtWRDNumber.setText("");
                    edtPalletID.setText("");
                    edtLotID.setText("");
                    edtPartNumber.setText("");
                    edtPartName.setText("");
                    edtQuantity.setText("");
                    edtUnit.setText("");
                    edtMFGDate.setText("");
                    edtEXPDate.setText("");
                    edtIDCode.setText("");
                    edtTotalQuantity.setText("");
                    edtTotalGoods.setText("");
                    wrDataGenerals.clear();
                    setAdapter(wrDataGenerals);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteWRDataGeneralByWRDNumber(String wrdNumber) {

        Call<Boolean> callback = iWarehouseApi.DeleteWRDataGeneral(wrdNumber);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    deleteWRDataHeader(wrdNumber);
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteWRDataGeneral(String wrdNumber, String palletID, String lotID) {

        Call<Boolean> callback = iWarehouseApi.DeleteWRDataGeneral(wrdNumber, palletID, lotID);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Xóa thành công lệnh nhập của Pallet ID: " + edtPalletID.getText().toString()+" Lot ID: " +edtLotID.getText().toString());
                    edtPalletID.setText("");
                    edtLotID.setText("");
                    edtPartNumber.setText("");
                    edtPartName.setText("");
                    edtQuantity.setText("");
                    edtUnit.setText("");
                    edtMFGDate.setText("");
                    edtEXPDate.setText("");
                    edtIDCode.setText("");
                    fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteWRDataGeneral(String wrdNumber, String palletID) {

        Call<Boolean> callback = iWarehouseApi.DeleteWRDataGeneral(wrdNumber, palletID);
        callback.enqueue(new Callback<Boolean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    alertSuccess(getApplicationContext(), "Xóa thành công lệnh nhập của pallet " + edtPalletID.getText().toString());
                    edtPalletID.setText("");
                    edtLotID.setText("");
                    edtPartNumber.setText("");
                    edtPartName.setText("");
                    edtQuantity.setText("");
                    edtUnit.setText("");
                    edtMFGDate.setText("");
                    edtEXPDate.setText("");
                    edtIDCode.setText("");
                    fetchWRDataGeneralData(edtWRDNumber.getText().toString());
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getTotalQuantity(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.size() > 0 && wrDataGenerals.get(i).getQuantity() > 0) {
                totalQuantity += wrDataGenerals.get(i).getQuantity();
            }
        }
    }

    private void getTotalGoods(ArrayList<WRDataGeneral> wrDataGenerals) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity().doubleValue() > 0) {
                totalGoods += 1;
            }
        }
    }

    private Boolean checkDuplicate(ArrayList<WRDataGeneral> wrDataGenerals, String palletID) {
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getPalletID().equalsIgnoreCase(palletID)) {
                isGoodsIdDuplicate = true;
                break;
            }
        }
        return isGoodsIdDuplicate;
    }

    private Boolean checkExistLabel(String palletID, ArrayList<WRDataGeneral> wrDataGenerals) {
        Boolean flag = false;
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (palletID.equalsIgnoreCase(wrDataGenerals.get(i).getPalletID())) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private Boolean checkPalletIDAndIDCodeScaned(String palletID, String idCode, WRDataGeneral wrDataGeneral) {
        Boolean flag = false;
        if (!wrDataGeneral.getIdCode().equalsIgnoreCase("") || wrDataGeneral.getIdCode() != null) {
            ArrayList<String> listIDCode = new ArrayList<String>(Arrays.asList(wrDataGeneral.getIdCode().split(",")));
            for (int j = 0; j < listIDCode.size(); j++) {
                if (palletID.equalsIgnoreCase(wrDataGeneral.getPalletID()) && idCode.equalsIgnoreCase(listIDCode.get(j).toString().toUpperCase())
                        && wrDataGeneral.getQuantity().doubleValue() > 0) {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

//    private Boolean checkInventory(int year, int month, String warhouseID, String partNumber, String palletID, String idCode) {
//        Call<List<InventoryDetail>> callback = iWarehouseApi.GetByParams6(year, month, warhouseID, partNumber, palletID, idCode);
//        callback.enqueue(new Callback<List<InventoryDetail>>() {
//            @RequiresApi(api = Build.VERSION_CODES.O)
//            @Override
//            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().size() < 1) {
//                        isInventory = false;
//                    } else {
//                        isInventory = true;
//                        alertError(ReceiptDataScanActivity.this, (" Mã hàng này đã nhập kho!!!"));
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
//                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
//            }
//        });
//        return isInventory;
//    }

    private Boolean checkExistInventory(String warhouseID, String partNumber, String palletID, String idCode) {
        Call<List<InventoryDetail>> callback = iWarehouseApi.CheckExist(warhouseID, partNumber, palletID, idCode);
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(ReceiptDataScanActivity.this,response.body().size()+"",Toast.LENGTH_SHORT).show();
                    if (response.body().size()>0) {
                        isInventory=true;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
        return isInventory;
    }

    private ArrayList<WRDataGeneral> getNotYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> notYetList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() == 0) {
                notYetList.add(wrDataGenerals.get(i));
            }
        }
        return notYetList;
    }

    private ArrayList<WRDataGeneral> getEnoughtYetList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughYettList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity() < wrDataGenerals.get(i).getQuantityOrg()) {
                enoughYettList.add(wrDataGenerals.get(i));
            }
        }
        return enoughYettList;
    }

    private ArrayList<WRDataGeneral> getEnoughtList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> enoughtList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > 0 && wrDataGenerals.get(i).getQuantity().doubleValue() == wrDataGenerals.get(i).getQuantityOrg().doubleValue()) {
                enoughtList.add(wrDataGenerals.get(i));
            }
        }
        return enoughtList;
    }

    private ArrayList<WRDataGeneral> getOvertList(ArrayList<WRDataGeneral> wrDataGenerals) {
        ArrayList<WRDataGeneral> overList = new ArrayList<>();
        for (int i = 0; i < wrDataGenerals.size(); i++) {
            if (wrDataGenerals.get(i).getQuantity() > wrDataGenerals.get(i).getQuantityOrg()) {
                overList.add(wrDataGenerals.get(i));
            }
        }
        return overList;
    }

    private void alertError(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Error")
                .setIcon(R.drawable.ic_baseline_error_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();

    }

    private void alertSuccess(Context context, String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
        dialog.setTitle("Success")
                .setIcon(R.drawable.ic_baseline_done_24)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
    }

    private void playErrorSound(){
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("error.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void playSuccesSound(){
        try {
            AssetFileDescriptor afd = null;
            afd = getAssets().openFd("ok.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();
            Thread.sleep(200);
            player.stop();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(ArrayList<WRDataGeneral> wrDataGenerals) {
        adapter = new WRDataGeneralScanAdapter(getApplicationContext(), wrDataGenerals, new WRDataGeneralScanAdapter.ItemClickListener() {
            @Override
            public void onClick(WRDataGeneral wrDataGeneral) {
                edtPalletID.setText(wrDataGeneral.getPalletID());
                edtLotID.setText(wrDataGeneral.getLotID());
                edtPartNumber.setText(wrDataGeneral.getPartNumber());
                edtPartName.setText(wrDataGeneral.getPartName());
                edtQuantity.setText(wrDataGeneral.getQuantity() + "");
                edtUnit.setText(wrDataGeneral.getUnit());
                edtMFGDate.setText(wrDataGeneral.getMfgDate());
                edtEXPDate.setText(wrDataGeneral.getExpDate());
                edtIDCode.setText(wrDataGeneral.getIdCode());
            }
        }, new WRDataGeneralScanAdapter.ItemLongClickListener() {
            @Override
            public void onLongClick(WRDataGeneral wrDataGeneral) {

            }
        }, new WRDataGeneralScanAdapter.DetailButtonClickListener() {
            @Override
            public void onClick(WRDataGeneral wrDataGeneral) {
                dialogDetail();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) { // verify if the soft keyboard is open
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_receipt_data_scan_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_addnew) {
            WRDataHeader wrDataHeader = new WRDataHeader();
            wrDataHeader.setWrrNumber("");
            wrDataHeader.setNote("");
            wrDataHeader.setUpdatedUserID("");
            wrDataHeader.setUpdatedDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
            wrDataHeader.setBranchID("1");
            wrDataHeader.setBranchName("TOKUDAI INDUSTRY VIETNAM CO., LTD");
            wrDataHeader.setHandlingStatusID("1");
            wrDataHeader.setHandlingStatusName("Chưa duyệt");
            wrDataHeader.setWrdDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
            wrDataHeader.setTotalQuantity(0.0);
            wrDataHeader.setTotalQuantityOrg(0.0);
            wrDataHeader.setStatus("0");
            wrDataHeader.setCreatedUserID(storage.getString(UserFragment.USERID, null));
            wrDataHeader.setCreatedDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
            createWRDataHeader(wrDataHeader, "R");
            alertSuccess(getApplicationContext(), "Tạo thành công lệnh quét nhập kho");
            edtPalletID.requestFocus();
            return true;
        }
        if (id == R.id.action_delete) {
            deleteWRDataGeneral(edtWRDNumber.getText().toString(), edtPalletID.getText().toString(), edtLotID.getText().toString());
            return true;
        }
        if (id == R.id.action_save) {
            process();
            return true;
        }
        if (id == R.id.action_approve) {
            if (wrDataHeader != null) {
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setHandlingStatusID("1");
                wrDataHeaderUpdate.setHandlingStatusName("Duyệt mức 1");
                putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                alertSuccess(getApplicationContext(), "Duyệt mức 1 quét nhập kho thành công");
            }
            return true;
        }
        if (id == R.id.action_clear) {
            if (wrDataGenerals.size() > 0) {
                deleteWRDataGeneralByWRDNumber(edtWRDNumber.getText().toString());
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
        } else {
            bExtScannerDisconnected = false;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
                    }
                }
                break;
            case WAITING:
                break;
            case SCANNING:
                break;
            case DISABLED:
                break;
            case ERROR:
                break;
            default:
                break;
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(1));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {

            } else {
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    if (edtWRDNumber.hasFocus()) {
                        edtWRDNumber.setText(result);
                    } else {
                        String palletID = "";
                        String lotID = "";
                        String partNumber = "";
                        String partName = "";
                        String unit = "";
                        String mfgDate = "";
                        String expDate = "";
                        String quantity = "";
                        String idCode = "";
                        int index1 = result.indexOf("|");
                        int index2 = result.indexOf("|", index1 + 1);
                        int index3 = result.indexOf("|", index2 + 1);
                        int index4 = result.indexOf("|", index3 + 1);
                        int index5 = result.indexOf("|", index4 + 1);
                        int index6 = result.indexOf("|", index5 + 1);
                        int index7 = result.indexOf("|", index6 + 1);
                        int index8 = result.indexOf("|", index7 + 1);
                        int index9 = result.indexOf("|", index8 + 1);
                        int index10 = result.indexOf("|", index9 + 1);
                        int index11 = result.indexOf("|", index10 + 1);
                        int index12 = result.indexOf("|", index11 + 1);
                        int index13 = result.lastIndexOf("|");


                        if (index13 > 0) {
                            idCode = result.substring(index2 + 1, index3);
                            lotID = result.substring(index8 + 1, index9);
                            partNumber = result.substring(index5 + 1, index6);
                            partName = result.substring(index6 + 1, index7);
                            unit = result.substring(index4 + 1, index5);
                            mfgDate = result.substring(index9 + 1, index10);
                            expDate = result.substring(index10 + 1, index11);
                            quantity = result.substring(index3 + 1, index4);
                            palletID = idCode.substring(0, 5);
                            if (palletID.length() == 12) {
                                idCode = (Integer.parseInt(palletID.substring(5, palletID.length()))) + "";
                                palletID = palletID.substring(0, 5);
                                edtPalletID.setText(palletID);
                                edtLotID.setText(lotID);
                                edtQuantity.setText(quantity);
                                edtIDCode.setText(idCode);
                                edtPartNumber.setText(partNumber);
                                edtPartName.setText(partName);
                                edtUnit.setText(unit);
                                edtMFGDate.setText(mfgDate);
                                edtEXPDate.setText(expDate);

                                fetchWRDataHeaderData(edtWRDNumber.getText().toString());
                                fetchGoods(partNumber, palletID);

                                process();

                            }
                        } else {
                            playErrorSound();
                            alertError(ReceiptDataScanActivity.this, "Định dạng tem nhãn không đúng");
                        }

                    }
                }
            }
        });
    }


    private void process() {
        if (TextUtils.isEmpty(edtPalletID.getText().toString())) {
            alertError(getApplicationContext(), "Nhập Pallet ID");
            edtPalletID.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(edtQuantity.getText().toString())) {
            alertError(getApplicationContext(), "Nhập số lượng");
            edtQuantity.requestFocus();
            return;
        }


        Call<List<InventoryDetail>> callback = iWarehouseApi.CheckExist("1",edtPartNumber.getText().toString(),edtPalletID.getText().toString(),edtIDCode.getText().toString());
        callback.enqueue(new Callback<List<InventoryDetail>>() {
            @Override
            public void onResponse(Call<List<InventoryDetail>> call, Response<List<InventoryDetail>> response) {
                if (response.isSuccessful()) {
                    if (response.body().size()>0) {
                        playErrorSound();
                        alertError(ReceiptDataScanActivity.this,"PartNumber: "+edtPartNumber.getText().toString()+", Mã hàng: "+edtPalletID.getText().toString()+edtIDCode.getText().toString()+" đã nhập kho, không thể thêm");
                    }
                    else {
                        Call<List<WRDataGeneral>> callback = iWarehouseApi.GetByParams3(edtWRDNumber.getText().toString(), edtPalletID.getText().toString(), edtLotID.getText().toString());
                        callback.enqueue(new Callback<List<WRDataGeneral>>() {
                            @Override
                            public void onResponse(Call<List<WRDataGeneral>> call, Response<List<WRDataGeneral>> response) {
                                if (response.isSuccessful()) {
                                    arrayListByGoodsID = (ArrayList<WRDataGeneral>) response.body();
                                    if (arrayListByGoodsID.size() > 0) {
                                        WRDataGeneral wrDataGeneralUpdate = arrayListByGoodsID.get(0);

                                        if (checkPalletIDAndIDCodeScaned(edtPalletID.getText().toString(), edtIDCode.getText().toString(), wrDataGeneralUpdate) == true) {
                                            playErrorSound();
                                            alertError(ReceiptDataScanActivity.this,"PartNumber: "+edtPartNumber.getText().toString()+", Mã hàng: "+edtPalletID.getText().toString()+edtIDCode.getText().toString()+" đã scan trong lệnh quét "+edtWRDNumber.getText().toString());
                                            return;
                                        }

                                        playSuccesSound();
                                        checkDuplicate(arrayListByGoodsID, edtPalletID.getText().toString());
                                        if (isGoodsIdDuplicate == false) {
                                            wrDataGeneralUpdate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                                            wrDataGeneralUpdate.setTotalGoods(totalGoods + 1);
                                        } else {
                                            wrDataGeneralUpdate.setQuantity(wrDataGeneralUpdate.getQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                                            wrDataGeneralUpdate.setTotalGoods(totalGoods);
                                            isGoodsIdDuplicate = false;
                                        }
                                        wrDataGeneralUpdate.setWrdNumber(edtWRDNumber.getText().toString());
                                        wrDataGeneralUpdate.setIdCode(wrDataGeneralUpdate.getIdCode() + edtIDCode.getText().toString().toUpperCase() + ",");
                                        wrDataGeneralUpdate.setOrdinal(arrayListByGoodsID.get(0).getOrdinal());

                                        wrDataGeneralUpdate.setInputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                                        wrDataGeneralUpdate.setTotalQuantity(wrDataHeader.getTotalQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                                        putWRDataGeneral(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getPalletID(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate.getLotID(), wrDataGeneralUpdate);

                                        WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                                        wrDataHeaderUpdate.setTotalQuantity(wrDataHeader.getTotalQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                                        putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                                    } else {
                                        try {
                                            WRDataGeneral wrDataGeneralCreate = new WRDataGeneral();

                                            wrDataGeneralCreate.setQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                                            wrDataGeneralCreate.setTotalGoods(totalGoods + 1);
                                            wrDataGeneralCreate.setWrdNumber(edtWRDNumber.getText().toString());
                                            if (goodsList.size() > 0) {
                                                wrDataGeneralCreate.setProductFamily(goodsList.get(0).getProductFamily());
                                            }
                                            wrDataGeneralCreate.setIdCode(edtIDCode.getText().toString().toUpperCase() + ",");
                                            wrDataGeneralCreate.setOrdinal(wrDataGenerals.size() + 1);
                                            wrDataGeneralCreate.setInputDate(simpleDateFormat.format(Calendar.getInstance().getTime()));
                                            wrDataGeneralCreate.setTotalQuantity(Double.parseDouble(edtQuantity.getText().toString()));
                                            wrDataGeneralCreate.setTotalQuantityOrg(0.0);
                                            wrDataGeneralCreate.setTotalGoodsOrg(0.0);
                                            wrDataGeneralCreate.setQuantityOrg(0.0);
                                            wrDataGeneralCreate.setQuantityByItem(0.0);
                                            wrDataGeneralCreate.setQuantityByPack(0.0);
                                            wrDataGeneralCreate.setPackingVolume(Double.valueOf(edtQuantity.getText().toString()));
                                            wrDataGeneralCreate.setPackingQuantity(0.0);

                                            wrDataGeneralCreate.setUnit(edtUnit.getText().toString());
                                            wrDataGeneralCreate.setPartNumber(edtPartNumber.getText().toString());
                                            wrDataGeneralCreate.setPartName(edtPartName.getText().toString());
                                            wrDataGeneralCreate.setPalletID(edtPalletID.getText().toString());
                                            wrDataGeneralCreate.setLotID(edtLotID.getText().toString());

                                            wrDataGeneralCreate.setMfgDate(simpleDateFormat.format(originalFormat.parse(edtMFGDate.getText().toString())));

                                            wrDataGeneralCreate.setExpDate(simpleDateFormat.format(originalFormat.parse(edtEXPDate.getText().toString())));


                                            createWRDataGeneral(wrDataGeneralCreate);

                                            WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                                            wrDataHeaderUpdate.setTotalQuantity(wrDataHeader.getTotalQuantity() + Double.parseDouble(edtQuantity.getText().toString()));
                                            putWRDataHeader(edtWRDNumber.getText().toString(), wrDataHeaderUpdate);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<List<WRDataGeneral>> call, Throwable t) {
                                Toast.makeText(getApplicationContext(), "get wrdatadetail by idcode" + t.getMessage() + "", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<InventoryDetail>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Fetch  WIDatadetail fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearList() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ReceiptDataScanActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_clear_list, viewGroup, false);
        builder.setView(dialogView);
        final android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < wrDataGenerals.size(); i++) {
                    WRDataGeneral wrDataGeneralUpdate = wrDataGenerals.get(i);
                    wrDataGeneralUpdate.setQuantity(0.0);
                    wrDataGeneralUpdate.setTotalGoods(0.0);
                    wrDataGeneralUpdate.setTotalQuantity(0.0);
                    wrDataGeneralUpdate.setIdCode("");
                    wrDataGeneralUpdate.setInputDate(null);
                    putWRDataGeneral2(wrDataGeneralUpdate.getWrdNumber(), wrDataGeneralUpdate.getPalletID(), wrDataGeneralUpdate.getOrdinal(), wrDataGeneralUpdate.getLotID(), wrDataGeneralUpdate);
                }
                WRDataHeader wrDataHeaderUpdate = wrDataHeader;
                wrDataHeaderUpdate.setTotalQuantity(0.0);
                putWRDataHeader(edtWRDNumber.getText().toString().toUpperCase(), wrDataHeaderUpdate);
                alertDialog.cancel();
                Toast.makeText(ReceiptDataScanActivity.this, "Đã xóa dữ liệu quét" + edtWRDNumber.getText().toString().toUpperCase(), Toast.LENGTH_SHORT).show();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}