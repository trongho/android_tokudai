package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WIDataGeneral {
    @SerializedName("widNumber")
    @Expose
    private String widNumber;
    @SerializedName("ordinal")
    @Expose
    private Integer ordinal;
    @SerializedName("palletID")
    @Expose
    private String palletID;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("productFamily")
    @Expose
    private String productFamily ;
    @SerializedName("lotID")
    @Expose
    private String lotID;
    @SerializedName("partName")
    @Expose
    private String partName;
    @SerializedName("quantity")
    @Expose
    private Double quantity;
    @SerializedName("totalQuantity")
    @Expose
    private Double totalQuantity;
    @SerializedName("totalGoods")
    @Expose
    private Double totalGoods;
    @SerializedName("quantityOrg")
    @Expose
    private Double quantityOrg;
    @SerializedName("totalQuantityOrg")
    @Expose
    private Double totalQuantityOrg;
    @SerializedName("totalGoodsOrg")
    @Expose
    private Double totalGoodsOrg;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("packingVolume")
    @Expose
    private Double packingVolume;
    @SerializedName("quantityByPack")
    @Expose
    private Double quantityByPack;
    @SerializedName("quantityByItem")
    @Expose
    private Double quantityByItem;
    @SerializedName("packingQuantity")
    @Expose
    private Double packingQuantity;
    @SerializedName("mfgDate")
    @Expose
    private String mfgDate;
    @SerializedName("expDate")
    @Expose
    private String expDate;
    @SerializedName("inputDate")
    @Expose
    private String inputDate;
    @SerializedName("outputDate")
    @Expose
    private String outputDate;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("pONumber")
    @Expose
    private String pONumber;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("mold")
    @Expose
    private String mold;

    public WIDataGeneral(String widNumber, Integer ordinal, String idCode, String partNumber, String productFamily, String lotID, String partName, Double quantity, Double totalQuantity, Double totalGoods, Double quantityOrg, Double totalQuantityOrg, Double totalGoodsOrg, String status, Double packingVolume, Double quantityByPack, Double quantityByItem, Double packingQuantity, String palletID, String mfgDate, String expDate, String inputDate, String outputDate, String remark, String unit, String pONumber, String material, String mold) {
        this.widNumber = widNumber;
        this.ordinal = ordinal;
        this.idCode = idCode;
        this.partNumber = partNumber;
        this.productFamily = productFamily;
        this.lotID = lotID;
        this.partName = partName;
        this.quantity = quantity;
        this.totalQuantity = totalQuantity;
        this.totalGoods = totalGoods;
        this.quantityOrg = quantityOrg;
        this.totalQuantityOrg = totalQuantityOrg;
        this.totalGoodsOrg = totalGoodsOrg;
        this.status = status;
        this.packingVolume = packingVolume;
        this.quantityByPack = quantityByPack;
        this.quantityByItem = quantityByItem;
        this.packingQuantity = packingQuantity;
        this.palletID = palletID;
        this.mfgDate = mfgDate;
        this.expDate = expDate;
        this.inputDate = inputDate;
        this.outputDate = outputDate;
        this.remark = remark;
        this.unit = unit;
        this.pONumber = pONumber;
        this.material = material;
        this.mold = mold;
    }

    public WIDataGeneral() {
    }

    public String getWidNumber() {
        return widNumber;
    }

    public void setWidNumber(String widNumber) {
        this.widNumber = widNumber;
    }

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    public String getLotID() {
        return lotID;
    }

    public void setLotID(String lotID) {
        this.lotID = lotID;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalGoods() {
        return totalGoods;
    }

    public void setTotalGoods(Double totalGoods) {
        this.totalGoods = totalGoods;
    }

    public Double getQuantityOrg() {
        return quantityOrg;
    }

    public void setQuantityOrg(Double quantityOrg) {
        this.quantityOrg = quantityOrg;
    }

    public Double getTotalQuantityOrg() {
        return totalQuantityOrg;
    }

    public void setTotalQuantityOrg(Double totalQuantityOrg) {
        this.totalQuantityOrg = totalQuantityOrg;
    }

    public Double getTotalGoodsOrg() {
        return totalGoodsOrg;
    }

    public void setTotalGoodsOrg(Double totalGoodsOrg) {
        this.totalGoodsOrg = totalGoodsOrg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getPackingVolume() {
        return packingVolume;
    }

    public void setPackingVolume(Double packingVolume) {
        this.packingVolume = packingVolume;
    }

    public Double getQuantityByPack() {
        return quantityByPack;
    }

    public void setQuantityByPack(Double quantityByPack) {
        this.quantityByPack = quantityByPack;
    }

    public Double getQuantityByItem() {
        return quantityByItem;
    }

    public void setQuantityByItem(Double quantityByItem) {
        this.quantityByItem = quantityByItem;
    }

    public Double getPackingQuantity() {
        return packingQuantity;
    }

    public void setPackingQuantity(Double packingQuantity) {
        this.packingQuantity = packingQuantity;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    public String getMfgDate() {
        return mfgDate;
    }

    public void setMfgDate(String mfgDate) {
        this.mfgDate = mfgDate;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getOutputDate() {
        return outputDate;
    }

    public void setOutputDate(String outputDate) {
        this.outputDate = outputDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getpONumber() {
        return pONumber;
    }

    public void setpONumber(String pONumber) {
        this.pONumber = pONumber;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMold() {
        return mold;
    }

    public void setMold(String mold) {
        this.mold = mold;
    }
}
