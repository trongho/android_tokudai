package com.example.tokudai.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tokudai.R;
import com.example.tokudai.model.WRDataGeneral;
import com.example.tokudai.ui.receipt_data_scan.ReceiptDataScanActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WRDataGeneralScanAdapter extends RecyclerView.Adapter<WRDataGeneralScanAdapter.ViewHolder> {
    private ReceiptDataScanActivity receiptDataScanActivity;
    Context context;
    ArrayList<WRDataGeneral> list;
    WRDataGeneral wrDataGeneral;
    WRDataGeneralScanAdapter.ItemClickListener listener;
    WRDataGeneralScanAdapter.DetailButtonClickListener detailButtonClickListener;
    WRDataGeneralScanAdapter.ItemLongClickListener longClickListener;
    SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    boolean isEnable=false;
    boolean isSelectAll=false;

    public WRDataGeneralScanAdapter(Context context, ArrayList<WRDataGeneral> list, WRDataGeneralScanAdapter.ItemClickListener listener,WRDataGeneralScanAdapter.ItemLongClickListener longClickListener,DetailButtonClickListener detailButtonClickListener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
        this.longClickListener=longClickListener;
        this.detailButtonClickListener=detailButtonClickListener;
    }

    @NonNull
    @Override
    public WRDataGeneralScanAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wrdata_general_scan_item, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new WRDataGeneralScanAdapter.ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull WRDataGeneralScanAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        wrDataGeneral = list.get(i);
        viewHolder.tvPalletID.setText(wrDataGeneral.getPalletID());
        viewHolder.tvPartNumber.setText(wrDataGeneral.getPartNumber());
        viewHolder.tvLotID.setText(wrDataGeneral.getLotID());
        viewHolder.tvQuantity.setText(String.format("%.0f",wrDataGeneral.getQuantity()));

        for(int j=0;j<=i;j++){
            viewHolder.tvLineNo.setText(j+1+"");
        }
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        viewHolder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return false;
            }
        });

        viewHolder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailButtonClickListener != null) {
                    detailButtonClickListener.onClick(list.get(i));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPalletID,tvPartNumber,tvLotID, tvQuantity,tvLineNo;
        public ImageView chkBox;
        public CardView cardView;
        public Button btnDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPalletID = itemView.findViewById(R.id.tvPalletID);
            tvPartNumber = itemView.findViewById(R.id.tvPartNumber);
            tvLotID=itemView.findViewById(R.id.tvLotID);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            tvLineNo=itemView.findViewById(R.id.tvLineNo);
            cardView = itemView.findViewById(R.id.carView);
            btnDetail=itemView.findViewById(R.id.btnDetail);
        }
    }

    public void updateList(List<WRDataGeneral> lists) {
        this.list.clear();
        this.list.addAll(lists);
    }

    public interface ItemClickListener {
        void onClick(WRDataGeneral wrDataGeneral);
    }

    public interface DetailButtonClickListener {
        void onClick(WRDataGeneral wrDataGeneral);
    }

    public interface ItemLongClickListener {
        void onLongClick(WRDataGeneral wrDataGeneral);
    }

}
