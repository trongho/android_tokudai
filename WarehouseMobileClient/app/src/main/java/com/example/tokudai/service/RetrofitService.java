package com.example.tokudai.service;

import com.example.tokudai.helper.Common;

public class RetrofitService {
    private static final String BASE_URL = "http://192.168.1.182:6381/api/";
//private static final String BASE_URL = "http://wmservice.hp.com:6379/api/";
    public static IWarehouseApi getService(){
        return APIRetrofitClient.getClient(Common.IP_SERVER).create(IWarehouseApi.class);
    }

//    public static DataService Create(){
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.1.55:6379/api/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        return retrofit.create(DataService.class);
//    }
}
