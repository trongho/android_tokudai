package com.example.tokudai.ui.warehouse;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.example.tokudai.R;
import com.example.tokudai.databinding.FragmentWarehouseBinding;
import com.example.tokudai.helper.Common;
import com.example.tokudai.ui.got_goods_data.GotGoodsActivity;
import com.example.tokudai.ui.issue_data.IssueDataActivity;
import com.example.tokudai.ui.pallet_manager.CreatePalletActivity;
import com.example.tokudai.ui.pallet_manager.PalletListActivity;
import com.example.tokudai.ui.pallet_manager.PalletListActivity2;
import com.example.tokudai.ui.receipt_data.ReceiptDataActivity;
import com.example.tokudai.ui.receipt_data_scan.ReceiptDataScanActivity;
import com.example.tokudai.ui.setting.ServerConfigFragment;
import com.example.tokudai.ui.setting.SettingFragment;
import com.example.tokudai.ui.sort_goods_data.SortGoodsDataActivity;
import com.example.tokudai.ui.tally.TallyDataActivity;
import com.example.tokudai.ui.user.UserFragment;

public class WarehouseFragment extends Fragment {

    private WarehouseViewModel warehouseViewModel;
    private FragmentWarehouseBinding binding;
    public static Boolean isLogin=false;
    SharedPreferences storage;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        warehouseViewModel =
                new ViewModelProvider(this).get(WarehouseViewModel.class);

        binding = FragmentWarehouseBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

//        final LinearLayout llWarehouseReceipt = binding.llWarehouseReceipt;
        final LinearLayout llIssueOrder = binding.llIssueOrder;
//        final LinearLayout llWarehouseReceiptData = binding.llWarehouseReceiptData;
        final LinearLayout llIssueData = binding.llIssueData;
        final LinearLayout llCreatePalletID = binding.llCreatePalletID;
//        final LinearLayout llGotGoodsData = binding.llGotGoodsData;
//        final LinearLayout llSortGoodsData= binding.llSortGoodsData;
//        final LinearLayout llLogout = binding.llLogout;
//        final LinearLayout llReceiptDataScan=binding.llReceiptDataScan;
        final LinearLayout llTallySheet = binding.llTallySheet;
        final LinearLayout llTallySheet2 = binding.llTallySheet2;
        final LinearLayout llPalletNumberList=binding.llPalletNumberList;

        storage = getActivity().getSharedPreferences(ServerConfigFragment.PREFERENCES, Context.MODE_PRIVATE);
        String base = "http://%1$s:6381/api/";
        if (storage.getString(ServerConfigFragment.IPADRESS, null) != null) {
            Common.IP_SERVER =String.format(base,storage.getString(ServerConfigFragment.IPADRESS, null));
        }

        llIssueOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Navigation.findNavController(getActivity(),R.id.nav_host_fragment_activity_main).navigate(R.id.issueOrderdest, null,getNavOptions());
            }
        });

//        llWarehouseReceipt.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.warehouse_receipt_action, null));
        llIssueOrder.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.issue_order_action, null));
//        llWarehouseReceiptData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getContext(), ReceiptDataActivity.class));
//            }
//        });
        llIssueData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), IssueDataActivity.class));
            }
        });
//        llGotGoodsData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getContext(), GotGoodsActivity.class));
//            }
//        });
//        llSortGoodsData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getContext(), SortGoodsDataActivity.class));
//            }
//        });
        llCreatePalletID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), CreatePalletActivity.class));
            }
        });
        llPalletNumberList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), PalletListActivity2.class));
            }
        });

//        llLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getActivity().finishAndRemoveTask();
//                getActivity().finishAffinity();
//                System.exit(0);
//            }
//        });
//        llReceiptDataScan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getContext(), ReceiptDataScanActivity.class));
//            }
//        });

        llTallySheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(),TallyDataActivity.class));
            }
        });

        llTallySheet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        if(storage.getString(UserFragment.ISLOGIN, null)!=null&&storage.getString(UserFragment.ISLOGIN, null).equalsIgnoreCase("logined")){
//            binding.iv1.setImageResource(R.drawable.ic_baseline_receipt_24);
//            binding.iv2.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv3.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv4.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv5.setImageResource(R.drawable.ic_baseline_receipt_24);
            binding.iv6.setImageResource(R.drawable.ic_baseline_receipt_24);
//            binding.iv7.setImageResource(R.drawable.ic_baseline_receipt_24);
//            binding.iv8.setImageResource(R.drawable.ic_baseline_qr_code_scanner_24);
            binding.iv9.setImageResource(R.drawable.ic_baseline_calculate_24);
            binding.iv10.setImageResource(R.drawable.ic_baseline_calculate_24);
//            llWarehouseReceiptData.setEnabled(true);
//            llWarehouseReceipt.setEnabled(true);
            llIssueOrder.setEnabled(true);
            llIssueData.setEnabled(true);
//            llGotGoodsData.setEnabled(true);
//            llSortGoodsData.setEnabled(true);
            llCreatePalletID.setEnabled(true);
            llPalletNumberList.setEnabled(true);
//            llReceiptDataScan.setEnabled(true);
            llTallySheet.setEnabled(true);
            llTallySheet2.setEnabled(true);
        }else {
//            llWarehouseReceiptData.setEnabled(false);
//            llWarehouseReceipt.setEnabled(false);
            llIssueOrder.setEnabled(false);
            llIssueData.setEnabled(false);
//            llGotGoodsData.setEnabled(false);
//            llSortGoodsData.setEnabled(false);
            llCreatePalletID.setEnabled(false);
            llPalletNumberList.setEnabled(false);
//            llReceiptDataScan.setEnabled(false);
            llTallySheet.setEnabled(false);
            llTallySheet2.setEnabled(false);
        }
        
        return root;
    }



    protected NavOptions getNavOptions() {
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_right)
                .setExitAnim(R.anim.slide_out_left)
                .setPopEnterAnim(R.anim.slide_in_left)
                .setPopExitAnim(R.anim.slide_out_right)
                .build();
        return navOptions;
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}