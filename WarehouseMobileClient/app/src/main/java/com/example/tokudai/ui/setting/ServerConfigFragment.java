package com.example.tokudai.ui.setting;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.tokudai.R;
import com.example.tokudai.databinding.FragmentServerConfigBinding;
import com.example.tokudai.databinding.FragmentSettingBinding;
import com.example.tokudai.helper.Common;
import com.example.tokudai.model.User;
import com.example.tokudai.service.APIRetrofitClient;
import com.example.tokudai.service.IWarehouseApi;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServerConfigFragment extends Fragment {
    public static final String PREFERENCES = "preferences";
    public static final String IPADRESS = "ip adress";
    EditText edtIP = null;
    TextView tvStatus = null;
    Button btnAutoConnect = null;
    public SharedPreferences storage;
    String ipStr = "";
    IWarehouseApi iWarehouseApi = null;
    Boolean isIPAvaible = false;

    private FragmentServerConfigBinding binding;


    public static ServerConfigFragment newInstance(String text) {

        ServerConfigFragment  f = new ServerConfigFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding =  FragmentServerConfigBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnAutoConnect = view.findViewById(R.id.AutoConnect);
        tvStatus = view.findViewById(R.id.tvStatus);
        edtIP = view.findViewById(R.id.edtIP);

        edtIP.setText(getBaseUrl());

        if (getBaseUrl() != null) {
            callMethod(getBaseUrl());
        }

        btnAutoConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(edtIP.getText().toString())) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            callMethod(edtIP.getText().toString());
                            if (isIPAvaible == true) {
                                tvStatus.setText("Host server found!!");
                            } else {
                                tvStatus.setText("Không tìm thấy host server!!!");
                            }
                        }
                    });
                } else {
                    new DownloadFilesTask().execute();
//                    getActivity().runOnUiThread(new Runnable() {
//                        Boolean isConneted=false;
//                        @Override
//                        public void run() {
//                            for(int j=1;j<100;j++) {
//                                for (int i = 2; i < 300; i++) {
//                                    callAutoMethod("192.168." +j+"."+i);
//                                    if (isIPAvaible == true) {
//                                        tvStatus.setText("Host server found!!");
//                                        isConneted=true;
//                                        break;
//                                    } else {
//                                        tvStatus.setText("Không tìm thấy host server!!!");
//                                    }
//                                }
////                                if(isConneted=true){
////                                    break;
////                                }
//                            }
//                        }
//                    });
                }
            }
        });
    }

    private class DownloadFilesTask extends AsyncTask<Void, Integer, String> {
        protected String doInBackground(Void... voids) {
            String result = "";
            for (int j = 1; j < 100; j++) {
                for (int i = 2; i < 300; i++) {
                    try {
                        callAutoMethod("192.168." + j + "." + i);
                        if (isIPAvaible == true) {
                            result = "Host server found!!";
                            break;
                        } else {
                            result = "Không tìm thấy host server!!!";
                        }

                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (result.equalsIgnoreCase("Host server found")) {
                    break;
                }
            }
            return result;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(String result) {
            tvStatus.setText(result);
        }
    }

    private void callMethod(String ip) {
        String base = "http://%1$s:6381/api/";
        String address = String.format(base, ip);
        iWarehouseApi = APIRetrofitClient.getClient(address).create(IWarehouseApi.class);
        User user = new User();
        user.setUsername("admin");
        user.setPassword("123456");
        Call<User> callback = iWarehouseApi.login(user);
        callback.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    tvStatus.setText("Connected server " + ip);
                    setBaseUrl(ip);
                    Common.IP_SERVER = address;
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                tvStatus.setText("Fail to Connected server " + ip + " " + t.getMessage());
            }
        });
    }

    private void callAutoMethod(String ip) {
        String base = "http://%1$s:6381/api/";
        String address = String.format(base, ip);
        iWarehouseApi = APIRetrofitClient.getClient(address).create(IWarehouseApi.class);
        User user = new User();
        user.setUsername("admin");
        user.setPassword("123456");
        Call<User> callback = iWarehouseApi.login(user);
        callback.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    tvStatus.setText("Connected server " + ip);
                    setBaseUrl(ip);
                    Common.IP_SERVER = address;
                    isIPAvaible = true;
                    edtIP.setText(ip);
                } else {
                    isIPAvaible = false;
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                isIPAvaible = false;
            }
        });
    }

    private void setBaseUrl(String ipAdress) {
        storage = getActivity().getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = storage.edit();
        editor.putString(IPADRESS, ipAdress);
        editor.commit();
    }

    public String getBaseUrl() {
        storage = getActivity().getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if (String.valueOf(storage.getString(IPADRESS, null)) != null) {
            ipStr = storage.getString(IPADRESS, null);
        }
        return ipStr;
    }
}