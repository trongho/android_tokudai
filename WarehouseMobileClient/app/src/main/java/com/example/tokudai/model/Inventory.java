package com.example.tokudai.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Inventory {
    @SerializedName("year")
    @Expose
    private Integer year;
    @SerializedName("month")
    @Expose
    private Integer month;
    @SerializedName("warehouseID")
    @Expose
    private String warehouseID;
    @SerializedName("partNumber")
    @Expose
    private String partNumber;
    @SerializedName("openingStockQuantity")
    @Expose
    private Double openingStockQuantity;
    @SerializedName("receiptQuantity")
    @Expose
    private Double receiptQuantity;
    @SerializedName("issueQuantity")
    @Expose
    private Double issueQuantity;
    @SerializedName("closingStockQuantity")
    @Expose
    private Double closingStockQuantity;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("packingVolume")
    @Expose
    private Double packingVolume;
    @SerializedName("material")
    @Expose
    private String material;
    @SerializedName("mold")
    @Expose
    private String mold;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("idCode")
    @Expose
    private String idCode;
    @SerializedName("palletID")
    @Expose
    private String palletID;

    public Inventory(Integer year, Integer month, String warehouseID, String partNumber, Double openingStockQuantity, Double receiptQuantity, Double issueQuantity, Double closingStockQuantity, String status, Double packingVolume, String material, String mold, String unit, String idCode, String palletID) {
        this.year = year;
        this.month = month;
        this.warehouseID = warehouseID;
        this.partNumber = partNumber;
        this.openingStockQuantity = openingStockQuantity;
        this.receiptQuantity = receiptQuantity;
        this.issueQuantity = issueQuantity;
        this.closingStockQuantity = closingStockQuantity;
        this.status = status;
        this.packingVolume = packingVolume;
        this.material = material;
        this.mold = mold;
        this.unit = unit;
        this.idCode = idCode;
        this.palletID = palletID;
    }

    public Inventory() {
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getWarehouseID() {
        return warehouseID;
    }

    public void setWarehouseID(String warehouseID) {
        this.warehouseID = warehouseID;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public Double getOpeningStockQuantity() {
        return openingStockQuantity;
    }

    public void setOpeningStockQuantity(Double openingStockQuantity) {
        this.openingStockQuantity = openingStockQuantity;
    }

    public Double getReceiptQuantity() {
        return receiptQuantity;
    }

    public void setReceiptQuantity(Double receiptQuantity) {
        this.receiptQuantity = receiptQuantity;
    }

    public Double getIssueQuantity() {
        return issueQuantity;
    }

    public void setIssueQuantity(Double issueQuantity) {
        this.issueQuantity = issueQuantity;
    }

    public Double getClosingStockQuantity() {
        return closingStockQuantity;
    }

    public void setClosingStockQuantity(Double closingStockQuantity) {
        this.closingStockQuantity = closingStockQuantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getPackingVolume() {
        return packingVolume;
    }

    public void setPackingVolume(Double packingVolume) {
        this.packingVolume = packingVolume;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMold() {
        return mold;
    }

    public void setMold(String mold) {
        this.mold = mold;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getIdCode() {
        return idCode;
    }

    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getPalletID() {
        return palletID;
    }

    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }
}
