package com.example.tokudai.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public final static String DBNAME = "WMSANDROID";
    public final static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql;
        //creat wrdatageneral table
        sql = "CREATE TABLE WRDATAGENERAL(" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                "WRDNUMBER TEXT NOT NULL," +
                "PALLETID TEXT NOT NULL," +
                "IDCODE TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql;
        sql = "DROP TABLE IF EXISTS WRDATAGENERAL";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }
}
